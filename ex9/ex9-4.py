import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import norm

# Simulate data
np.random.seed(42)
n = 1000
X1 = np.random.binomial(1, 0.5, n)
X2 = np.random.normal(0, 1, n)
X = np.column_stack((np.ones(n), X1, X2))  # Add intercept term

# True coefficients
beta_true = np.array([-0.5, 1.0, -1.5])
p = 1 / (1 + np.exp(-X @ beta_true))
y = np.random.binomial(1, p)
print(f"p {p}")
print(f"y {y}")
sys.exit()


# Log-likelihood function
def log_likelihood(beta, X, y):
    linear_combination = X @ beta
    log_lik = np.sum(y * linear_combination - np.log(1 + np.exp(linear_combination)))
    return log_lik

# Log-prior function (assuming normal prior)
def log_prior(beta, mean=0, var=10):
    return -0.5 * np.sum((beta - mean)**2 / var)

# Log-posterior function
def log_posterior(beta, X, y):
    return log_likelihood(beta, X, y) + log_prior(beta)

# MH algorithm with tailored proposal
def metropolis_hastings(X, y, n_samples=10000, burnin=500, proposal_width=0.1):
    n_params = X.shape[1]
    beta_samples = np.zeros((n_samples, n_params))
    beta_current = np.zeros(n_params)
    current_log_post = log_posterior(beta_current, X, y)
    
    accept_count = 0
    for i in range(n_samples):
        beta_proposal = beta_current + np.random.normal(0, proposal_width, n_params)
        proposal_log_post = log_posterior(beta_proposal, X, y)
        
        acceptance_ratio = np.exp(proposal_log_post - current_log_post)
        
        if acceptance_ratio > np.random.rand():
            beta_current = beta_proposal
            current_log_post = proposal_log_post
            accept_count += 1
        
        beta_samples[i] = beta_current
    
    acceptance_rate = accept_count / n_samples
    return beta_samples[burnin:], acceptance_rate

# Run the MH sampler
n_samples = 10000
beta_samples, acceptance_rate = metropolis_hastings(X, y, n_samples=n_samples, proposal_width=0.1)

# Analyze the results
print(f"Acceptance rate: {acceptance_rate:.2f}")

# Plot the trace plots and posterior distributions
fig, axes = plt.subplots(3, 2, figsize=(12, 8))
param_names = ['Intercept', 'X1', 'X2']

for i in range(3):
    sns.lineplot(x=np.arange(n_samples - 500), y=beta_samples[:, i], ax=axes[i, 0])
    axes[i, 0].set_title(f'Trace plot for {param_names[i]}')
    sns.histplot(beta_samples[:, i], kde=True, ax=axes[i, 1])
    axes[i, 1].set_title(f'Posterior distribution for {param_names[i]}')

plt.tight_layout()
plt.show()
