import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import arviz as az
from scipy.stats import t, norm, cauchy, probplot

# Set random seed for reproducibility
np.random.seed(42)

# Target distribution: t-distribution with 3 degrees of freedom
def target_distribution(x):
    return t.pdf(x, df=3)

# Metropolis-Hastings algorithm
def metropolis_hastings(target, proposal_rvs, nsim, burnin):
    samples = []
    x_current = 0
    accept_count = 0
    for i in range(nsim + burnin):
        x_proposal = proposal_rvs(x_current)
        acceptance_ratio = target(x_proposal) / target(x_current)
        if np.random.rand() < acceptance_ratio:
            x_current = x_proposal
            accept_count += 1
        if i >= burnin:
            samples.append(x_current)
    acceptance_rate = accept_count / (nsim + burnin)
    return np.array(samples), acceptance_rate

# Normal proposal
def normal_proposal(x):
    return norm.rvs(loc=x, scale=1)

# Cauchy proposal
def cauchy_proposal(x):
    return cauchy.rvs(loc=x, scale=1)

# Parameters
nsim = 10000
burnin = 1000

# Run Metropolis-Hastings for both proposals
samples_normal, acceptance_rate_normal = metropolis_hastings(target_distribution,  normal_proposal, nsim, burnin)
samples_cauchy, acceptance_rate_cauchy = metropolis_hastings(target_distribution,  cauchy_proposal, nsim, burnin)

# Calculate effective sample sizes
ess_normal = az.ess(az.convert_to_inference_data(samples_normal))['x'].values
ess_cauchy = az.ess(az.convert_to_inference_data(samples_cauchy))['x'].values

# Plotting function
def plot_results(samples, proposal_name, acceptance_rate, ess):
    # MCMC plot
    plt.figure(figsize=(10, 6))
    plt.plot(samples, lw=0.5)
    plt.title(f'MCMC Trace Plot ({proposal_name} Proposal)\nAcceptance Rate: {acceptance_rate:.2f}, ESS: {ess:.2f}')
    plt.xlabel('Iteration')
    plt.ylabel('Sample Value')
    plt.show()

    # Kernel density estimate
    plt.figure(figsize=(10, 6))
    sns.kdeplot(samples, bw_adjust=0.5, label=f'KDE of samples ({proposal_name} Proposal)', color='blue')
    sns.kdeplot(t.rvs(df=3, size=nsim), bw_adjust=0.5, label='t(3) distribution', color='red', linestyle='--')
    plt.xlabel('Value')
    plt.ylabel('Density')
    plt.title(f'Kernel Density Estimate ({proposal_name} Proposal)')
    plt.legend()
    plt.show()

    # Q-Q plot
    plt.figure(figsize=(10, 6))
    probplot(samples, dist="t", sparams=(3,), plot=plt)
    plt.title(f'Q-Q Plot ({proposal_name} Proposal)')
    plt.show()


# Plot results for Normal proposal
plot_results(samples_normal, 'Normal', acceptance_rate_normal, ess_normal)

# Plot results for Cauchy proposal
plot_results(samples_cauchy, 'Cauchy', acceptance_rate_cauchy, ess_cauchy)


