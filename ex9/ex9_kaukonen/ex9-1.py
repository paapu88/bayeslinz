import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
import seaborn as sns

# Given data
y = np.array([1.64, 1.70, 1.72, 1.74, 1.82, 1.82, 1.82, 1.90, 2.08])
sigma2 = 0.132
m0 = 0
M0 = 10000

# Compute the posterior parameters
n = len(y)
y_bar = np.mean(y)

# page 62 in notes
posterior_mean = (m0 / M0 + n * y_bar / sigma2) / (1 / M0 + n / sigma2)
posterior_variance = 1 / (1 / M0 + n / sigma2)
posterior_std = np.sqrt(posterior_variance)

print(f"Posterior mean: {posterior_mean}")
print(f"Posterior variance: {posterior_variance}")

# Sample from the posterior distribution
nsim = 10000
mu_samples = np.random.normal(posterior_mean, posterior_std, nsim)

# Plot the kernel density estimate of the samples
plt.figure(figsize=(10, 6))
sns.kdeplot(mu_samples, bw_adjust=0.5, label='KDE of posterior samples', color='blue')

# Plot the exact posterior distribution
x = np.linspace(posterior_mean - 4 * posterior_std, posterior_mean + 4 * posterior_std, 1000)
plt.plot(x, norm.pdf(x, posterior_mean, posterior_std), label='Exact posterior distribution', color='red', linestyle='--')

plt.xlabel('µ')
plt.ylabel('Density')
plt.title('Kernel Density Estimate vs Exact Posterior Distribution')
plt.legend()
plt.show()
