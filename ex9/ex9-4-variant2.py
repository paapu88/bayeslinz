import numpy as np
import scipy.stats as stats
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf

def logistic_log_likelihood(beta, X, y):
    logits = X @ beta
    return np.sum(y * logits - np.log(1 + np.exp(logits)))

def proposal_distribution(beta):
    # Tailored proposal: Normal distribution centered at current beta
    return np.random.normal(beta, 0.5, size=beta.shape)

def mh_sampler(X, y, initial_beta, iterations):
    beta_samples = [initial_beta]
    current_beta = initial_beta
    current_log_like = logistic_log_likelihood(current_beta, X, y)
   
    
    for _ in range(iterations):
        proposed_beta = proposal_distribution(current_beta)
        proposed_log_like = logistic_log_likelihood(proposed_beta, X, y)
        
        acceptance_ratio = np.exp(proposed_log_like - current_log_like)
        #print(f"current_log_like {current_log_like}")
        #print(f"proposed_log_like {proposed_log_like}")

        if np.random.rand() < acceptance_ratio:
            current_beta = proposed_beta
            current_log_like = proposed_log_like
        
        beta_samples.append(current_beta)
    
    return np.array(beta_samples)

# Generate simulated data
np.random.seed(42)
n = 1000
X1 = np.random.binomial(1, 0.5, n)
X2 = np.random.normal(0, 1, n)
X = np.column_stack((np.ones(n), X1, X2))
true_beta = np.array([0.5, -1.0, 2.0])
logits = X @ true_beta
probabilities = 1 / (1 + np.exp(-logits))
y = np.random.binomial(1, probabilities)

# Run MH sampler
initial_beta = np.zeros(X.shape[1])
iterations = 10000
samples = mh_sampler(X, y, initial_beta, iterations)
# Display the samples
df_samples = pd.DataFrame(samples, columns=['beta_0', 'beta_1', 'beta_2'])
print(df_samples.tail())

# Trace plots
plt.figure(figsize=(12, 6))
for i in range(samples.shape[1]):
    plt.subplot(samples.shape[1], 1, i + 1)
    plt.plot(samples[:, i])
    plt.title(f'Trace plot for beta_{i}')
plt.tight_layout()
plt.show()

# Histograms
plt.figure(figsize=(12, 6))
for i in range(samples.shape[1]):
    plt.subplot(samples.shape[1], 1, i + 1)
    plt.hist(samples[:, i], bins=30, density=True)
    plt.title(f'Posterior of beta_{i}')
plt.tight_layout()
plt.show()

# Autocorrelation
plt.figure(figsize=(12, 6))
for i in range(samples.shape[1]):
    plt.subplot(samples.shape[1], 1, i + 1)
    plot_acf(samples[:, i], ax=plt.gca())
    plt.title(f'Autocorrelation of beta_{i}')
plt.tight_layout()
plt.show()
