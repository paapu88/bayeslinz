import numpy as np
import scipy.stats as stats
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf

def logistic_log_likelihood(beta, X, y):
    logits = X @ beta
    return np.sum(y * logits - np.log(1 + np.exp(logits)))

def log_prior(beta, mu, sigma):
    return np.sum(stats.norm.logpdf(beta, mu, sigma))

def log_posterior(beta, X, y, mu, sigma):
    return logistic_log_likelihood(beta, X, y) + log_prior(beta, mu, sigma)

def proposal_distribution(beta, scale):
    return np.random.normal(beta, scale, size=beta.shape)

def mh_sampler(X, y, initial_beta, iterations, mu=0, sigma=10, scale=0.1):
    beta_samples = [initial_beta]
    current_beta = initial_beta
    current_log_post = log_posterior(current_beta, X, y, mu, sigma)
    accept_count = 0
    for _ in range(iterations):
        proposed_beta = proposal_distribution(current_beta, scale=scale)
        proposed_log_post = log_posterior(proposed_beta, X, y, mu, sigma)
        
        acceptance_ratio = np.exp(proposed_log_post - current_log_post)
        
        if np.random.rand() < acceptance_ratio:
            current_beta = proposed_beta
            current_log_post = proposed_log_post
            accept_count += 1
        beta_samples.append(current_beta)
    print(f"acceptance ratio {accept_count / (iterations)}")
    return np.array(beta_samples)

# Generate simulated data
np.random.seed(42)
n = 1000
X1 = np.random.binomial(1, 0.5, n)
X2 = np.random.normal(0, 1, n)
X = np.column_stack((np.ones(n), X1, X2))
true_beta = np.array([0.5, -1.0, 2.0])
logits = X @ true_beta
probabilities = 1 / (1 + np.exp(-logits))
y = np.random.binomial(1, probabilities)

# Run MH sampler with priors
initial_beta = np.zeros(X.shape[1])
iterations = 1000
samples = mh_sampler(X, y, initial_beta, iterations)

# Display the samples
df_samples = pd.DataFrame(samples, columns=['beta_0', 'beta_1', 'beta_2'])
print(df_samples.tail())

# Trace plots
plt.figure(figsize=(12, 6))
for i in range(samples.shape[1]):
    plt.subplot(samples.shape[1], 1, i + 1)
    plt.plot(samples[:, i])
    plt.title(f'Trace plot for beta_{i}')
plt.tight_layout()
plt.show()

# Histograms
plt.figure(figsize=(12, 6))
for i in range(samples.shape[1]):
    plt.subplot(samples.shape[1], 1, i + 1)
    plt.hist(samples[:, i], bins=30, density=True)
    plt.title(f'Posterior of beta_{i}')
plt.tight_layout()
plt.show()

# Autocorrelation
plt.figure(figsize=(12, 6))
for i in range(samples.shape[1]):
    plt.subplot(samples.shape[1], 1, i + 1)
    plot_acf(samples[:, i], ax=plt.gca())
    plt.title(f'Autocorrelation of beta_{i}')
plt.tight_layout()
plt.show()

