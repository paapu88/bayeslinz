import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import norm
import arviz as az

# Given data
y = np.array([1.64, 1.70, 1.72, 1.74, 1.82, 1.82, 1.82, 1.90, 2.08])
sigma2 = 2
m0 = 0
M0 = 10000
nsim = 10000
burnin = 500

# Compute the posterior parameters
n = len(y)
y_bar = np.mean(y)
# page 62 in notes
posterior_mean = (m0 / M0 + n * y_bar / sigma2) / (1 / M0 + n / sigma2)
posterior_variance = 1 / (1 / M0 + n / sigma2)
posterior_std = np.sqrt(posterior_variance)

# Exact posterior distribution
x = np.linspace(posterior_mean - 4 * posterior_std, posterior_mean + 4 * posterior_std, 1000)
exact_posterior = norm.pdf(x, posterior_mean, posterior_std)

# Metropolis algorithm function
def metropolis_algorithm(delta2, nsim, burnin):
    mu_current = 0
    mu_samples = []
    for i in range(nsim + burnin):
        mu_proposal = np.random.normal(mu_current, np.sqrt(delta2))
        posterior_current = norm.logpdf(x=mu_current, loc=m0, scale=np.sqrt(M0)) + norm.logpdf(x=y, loc=mu_current, scale=np.sqrt(sigma2)).sum()
        posterior_proposal = norm.logpdf(x=mu_proposal, loc=m0, scale=np.sqrt(M0)) + norm.logpdf(x=y, loc=mu_proposal, scale=np.sqrt(sigma2)).sum()
        acceptance_ratio = np.exp(posterior_proposal - posterior_current)
        if np.random.rand() < acceptance_ratio:
            mu_current = mu_proposal
            #print(f"mu {mu_current}")
        if i >= burnin:
            mu_samples.append(mu_current)
    return np.array(mu_samples)

# Plotting function
def plot_density(samples, delta2):
    plt.figure(figsize=(10, 6))
    sns.kdeplot(samples, bw_adjust=0.5, label=f'KDE of samples (δ²={delta2})', color='blue')
    plt.plot(x, exact_posterior, label='Exact posterior distribution', color='red', linestyle='--')
    plt.xlabel('µ')
    plt.ylabel('Density')
    plt.title(f'Kernel Density Estimate vs Exact Posterior Distribution (δ²={delta2})')
    plt.legend()
    plt.show()

# Performance comparison function
def compare_performance(delta2_values):
    for delta2 in delta2_values:
        samples = metropolis_algorithm(delta2, nsim, burnin)
        plot_density(samples, delta2)
        # Calculate integrated autocorrelation time and effective sample size
        az_data = az.convert_to_inference_data(samples)
        iat = az.ess(az_data, method='mean', relative=False).x
        ess = az.ess(az_data, method='mean', relative=True).x
        print(f'δ²={delta2}: IAT={iat:.2f}, ESS={ess:.2f}')

# Values of δ² to test
delta2_values = [0.002, 0.02, 0.2, 2, 20]
compare_performance(delta2_values)
