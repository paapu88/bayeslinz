import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import beta


# Define the prior distributions
def jeffreys_prior(pi):
    return 1 / np.sqrt(pi * (1 - pi))


def flat_prior(pi):
    return np.ones_like(pi)


def beta_prior(pi, alpha, beta):
    return pi ** (alpha - 1) * (1 - pi) ** (beta - 1)


# Define the likelihood function
def likelihood(pi, n, y):
    return (1 - pi) ** (n - y) * pi**y


# Define the posterior distributions
def posterior_jeffreys(pi, n, y):
    return likelihood(pi, n, y) * jeffreys_prior(pi)


def posterior_flat(pi, n, y):
    return likelihood(pi, n, y) * flat_prior(pi)


def posterior_beta(pi, alpha, beta, n, y):
    return likelihood(pi, n, y) * beta_prior(pi, alpha, beta)


# Parameters
pi_values = np.linspace(0.001, 0.999, 1000)
n = 40
y = 0

# Calculate distributions
prior_jeffreys_values = jeffreys_prior(pi_values)
prior_flat_values = flat_prior(pi_values)
prior_beta_values = beta_prior(pi_values, 2, 1)

likelihood_values = likelihood(pi_values, n, y)

posterior_jeffreys_values = posterior_jeffreys(pi_values, n, y)
posterior_flat_values = posterior_flat(pi_values, n, y)
posterior_beta_values = posterior_beta(pi_values, 2, 1, n, y)

# Normalize posteriors
posterior_jeffreys_values /= np.trapz(posterior_jeffreys_values, pi_values)
posterior_flat_values /= np.trapz(posterior_flat_values, pi_values)
posterior_beta_values /= np.trapz(posterior_beta_values, pi_values)

# Plot
plt.figure(figsize=(10, 8))
plt.plot(pi_values, prior_jeffreys_values, label="Jeffreys Prior", linestyle="dashed")
plt.plot(pi_values, prior_flat_values, label="Flat Prior", linestyle="dashed")
plt.plot(pi_values, prior_beta_values, label="Beta(2, 1) Prior", linestyle="dashed")
plt.plot(pi_values, likelihood_values, label="Likelihood")
plt.plot(pi_values, posterior_jeffreys_values, label="Posterior (Jeffreys Prior)")
plt.plot(pi_values, posterior_flat_values, label="Posterior (Flat Prior)")
plt.plot(pi_values, posterior_beta_values, label="Posterior (Beta(2, 1) Prior)")
plt.xlabel("π")
plt.ylabel("Density")
plt.legend()
plt.title("Prior, Likelihood, and Posterior Distributions")
plt.show()
