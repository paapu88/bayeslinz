from scipy.stats import beta
from scipy.integrate import quad
import numpy as np

# Posterior parameters for flat prior
a_flat, b_flat = 1, 41
mean_flat = a_flat / (a_flat + b_flat)
ci_flat = beta.ppf([0.025, 0.975], a_flat, b_flat)
p_less_than_0_01_flat = beta.cdf(0.01, a_flat, b_flat)

# Posterior parameters for Beta(2, 1) prior
a_beta, b_beta = 2, 41
mean_beta = a_beta / (a_beta + b_beta)
ci_beta = beta.ppf([0.025, 0.975], a_beta, b_beta)
p_less_than_0_01_beta = beta.cdf(0.01, a_beta, b_beta)

print(f"Flat prior posterior mean: {mean_flat}")
print(f"Flat prior 95% equal-tailed interval: {ci_flat}")
print(f"Flat prior P(π < 0.01): {p_less_than_0_01_flat}")

print(f"Beta(2, 1) prior posterior mean: {mean_beta}")
print(f"Beta(2, 1) prior 95% equal-tailed interval: {ci_beta}")
print(f"Beta(2, 1) prior P(π < 0.01): {p_less_than_0_01_beta}")


# Posterior with Jeffreys prior
def posterior_jeffreys_normalized(pi):
    unnormalized = (1 - pi) ** 40 / np.sqrt(pi * (1 - pi))
    normalization_constant = quad(lambda x: (1 - x) ** 40 / np.sqrt(x * (1 - x)), 0, 1)[
        0
    ]
    return unnormalized / normalization_constant


posterior_mean_jeffreys = quad(lambda pi: pi * posterior_jeffreys_normalized(pi), 0, 1)[
    0
]
p_less_than_0_01_jeffreys = quad(posterior_jeffreys_normalized, 0, 0.01)[0]

print(f"Jeffreys prior posterior mean: {posterior_mean_jeffreys}")
print(f"Jeffreys prior P(π < 0.01): {p_less_than_0_01_jeffreys}")
