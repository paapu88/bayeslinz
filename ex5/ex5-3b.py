import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import beta, binom

# Parameters from posterior
alpha_post = 41
beta_post = 1011
nA = 1000  # Total number of surgeries for method A
nB = 50  # Total number of surgeries for method B

# Generate samples from the posterior distribution of theta
theta_samples = beta.rvs(alpha_post, beta_post, size=100000)

# Simulate new observations from the predictive distribution
new_infections_A = binom.rvs(nA, theta_samples, size=100000)
new_infections_B = binom.rvs(nB, theta_samples, size=100000)
print(f"new_infections_A {new_infections_A}")
print(f"new_infections_B {new_infections_B}")

# Plotting the results
plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.hist(new_infections_A, bins=30, alpha=0.75, label="Predictive - Method A")
plt.axvline(x=40, color="red", linestyle="--", label="Observed - Method A")
plt.xlabel("Number of Infections")
plt.ylabel("Frequency")
plt.title("Posterior Predictive Check - Method A")
plt.legend()

plt.subplot(1, 2, 2)
plt.hist(new_infections_B, bins=30, alpha=0.75, label="Predictive - Method B")
plt.axvline(x=0, color="red", linestyle="--", label="Observed - Method B")
plt.xlabel("Number of Infections")
plt.ylabel("Frequency")
plt.title("Posterior Predictive Check - Method B")
plt.legend()

plt.tight_layout()
plt.show()

# Calculate and compare infection prevalences
prevalence_A = np.mean(new_infections_A) / nA
prevalence_B = np.mean(new_infections_B) / nB
print(f"Predictive Infection Prevalence for Method A: {prevalence_A:.4f}")
print(f"Predictive Infection Prevalence for Method B: {prevalence_B:.4f}")
print(f"Observed Infection Prevalence for Method A: {40/nA:.4f}")
print(f"Observed Infection Prevalence for Method B: {0/nB:.4f}")

# ANSWER
# model is less plausible for trial B
# but still possible (in B about 13% of samples have zero cases)
