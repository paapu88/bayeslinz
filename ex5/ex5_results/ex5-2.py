import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import invgamma, norm

# Given data
nA = nB = 16
y_bar_A = 75.2
s_A = 7.3
y_bar_B = 77.5
s_B = 8.1
m0 = 75
M0 = 100
params = [1, 2, 4, 16, 32]  # Different values for ν0 = κ0

# Results list
pi_estimates = []

inv_g_x = np.linspace(0.1, 20, 100)
for param in params:
    nu0 = kappa0 = param
    # Posterior parameters for A (page 70 in slides)
    mA = (kappa0 * m0 + nA * y_bar_A) / (kappa0 + nA)
    kA = kappa0 + nA
    alpha_A = nu0 / 2 + nA / 2
    beta_A = (
        nu0 * M0 / 2
        + np.sum((s_A**2) * (nA - 1)) / 2
        + kappa0 * nA * (y_bar_A - m0) ** 2 / (2 * (kappa0 + nA))
    )

    # Posterior parameters for B (page 70 in slides)
    mB = (kappa0 * m0 + nB * y_bar_B) / (kappa0 + nB)
    kB = kappa0 + nB
    alpha_B = nu0 / 2 + nB / 2
    beta_B = (
        nu0 * M0 / 2
        + np.sum((s_B**2) * (nB - 1)) / 2
        + kappa0 * nB * (y_bar_B - m0) ** 2 / (2 * (kappa0 + nB))
    )

    # Monte Carlo simulation
    num_samples = 10000
    muA_samples = norm.rvs(mA, np.sqrt(beta_A / (alpha_A * kA)), size=num_samples)
    muB_samples = norm.rvs(mB, np.sqrt(beta_B / (alpha_B * kB)), size=num_samples)

    pi_estimate = np.mean(muA_samples < muB_samples)
    pi_estimates.append(pi_estimate)

    # plot invere gamma with current parameters
    a = kappa0 / 2  # page 68, shape parameter
    b = kappa0 / 2 * nu0**2  # scale parameter
    dist = invgamma(a, scale=b)
    inv_g_y = dist.pdf(inv_g_x)
    # Plotting the results
    plt.figure(figsize=(10, 6))
    plt.plot(inv_g_x, inv_g_y, marker="o")
    plt.xlabel(f"sigma")
    plt.ylabel(f"pdf of sigma")
    plt.title(f"pdf of sigma with a={a}, b={b}")
    plt.grid(True)
    plt.show()


# Plotting the results
plt.figure(figsize=(10, 6))
plt.plot(params, pi_estimates, marker="o")
plt.xlabel("ν0 = κ0")
plt.ylabel("Estimated π (P(µA < µB | yA, yB))")
plt.title("Effect of ν0 and κ0 on the Probability Estimate")
plt.grid(True)
plt.show()

# small kappa0 and nu0 mean that variance of variance is small
# so we know with more certaintity y,
# so probability for P(µA < µB | yA, yB) is more certain
