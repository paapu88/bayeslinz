import numpy as np
import matplotlib.pyplot as plt

# Parameters

sigma2 = 1
n = 10
y_bar = 0.5  # Example mean of observations
S = 10000  # Number of samples for Monte Carlo

for m0, M0 in [[-5, 3], [0, 1], [3, 5], [3, 0.2]]:

    # Posterior parameters
    Mn = 1 / (1 / M0 + n / sigma2)
    mn = (m0 / M0 + n * y_bar / sigma2) * Mn

    # Monte Carlo sample
    mu_samples = np.random.normal(mn, np.sqrt(Mn), S)
    y_samples = np.random.normal(mu_samples, np.sqrt(sigma2), S)
    # Plotting results
    plt.hist(y_samples, bins=50, alpha=0.75, label="Monte Carlo")
    plt.axvline(
        x=mn,
        color="r",
        linestyle="dashed",
        linewidth=2,
        label=f"Posterior Mean={round(mn,2)} given observed mean={y_bar}",
    )
    plt.title(f"MC Predictive Distribution with prior for $\mu$ m0={m0} M0={M0}")
    plt.legend()
    plt.show()
