import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import beta

# Posterior parameters
alpha_post = 41
beta_post = 1011

# Define theta values for plotting
theta_values = np.linspace(0, 0.1, 1000)

# Calculate the posterior density
posterior_density = beta.pdf(theta_values, alpha_post, beta_post)

# Plotting
plt.figure(figsize=(8, 4))
plt.plot(theta_values, posterior_density, label="Posterior Density", color="blue")
plt.title("Posterior Distribution of Infection Risk")
plt.xlabel("Risk of Infection (θ)")
plt.ylabel("Density")
plt.fill_between(theta_values, posterior_density, color="blue", alpha=0.3)
plt.axvline(
    x=alpha_post / (alpha_post + beta_post),
    color="red",
    linestyle="--",
    label="Posterior Mean",
)
plt.legend()
plt.show()
