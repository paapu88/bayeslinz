import pandas as pd
import torch
import pyro
import pyro.distributions as dist
from pyro.infer import MCMC, NUTS
from pathlib import Path
import matplotlib.pyplot as plt
from pyro.infer import Predictive
import numpy as np

# Load the data
data = pd.read_csv(
    Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/ex2/children.csv")
)
data["exposed"] = data["exposed"].to_numpy().astype(int)
# Inspect the data structure
print(data.head())
print("Summary stats:")
print(data.describe())

# Check the intervention point
print("Data around intervention point (id 93):")
print(data.iloc[92:95])


def model(data_killed, data_exposed):
    # Prior distributions for the probabilities of being killed before and after the intervention
    p1 = pyro.sample(
        "p1", dist.Beta(2.0, 10.0)
    )  # Reasonable beta prior, you might want to adjust this
    p2 = pyro.sample("p2", dist.Beta(2.0, 10.0))

    # Risk reductions
    delta1 = p1 - p2
    delta2 = (p1 - p2) / p1

    # Register these as Pyro deterministics for trace extraction
    pyro.deterministic("delta1", delta1)
    pyro.deterministic("delta2", delta2)

    # Observations pre-intervention
    with pyro.plate("pre_data", data_killed[:92].size(0)):
        pyro.sample(
            "obs_pre", dist.Binomial(data_exposed[:92], p1), obs=data_killed[:92]
        )

    # Observations post-intervention
    with pyro.plate("post_data", data_killed[93:].size(0)):
        pyro.sample(
            "obs_post", dist.Binomial(data_exposed[93:], p2), obs=data_killed[93:]
        )


# Data preparation
data_killed = torch.tensor(data["y"].values).float()
data_exposed = torch.tensor(data["exposed"].values).float()

# Run MCMC
nuts_kernel = NUTS(model)
mcmc = MCMC(nuts_kernel, num_samples=1000, warmup_steps=200)
mcmc.run(data_killed, data_exposed)

# Extract samples
posterior_samples = mcmc.get_samples()

# Extract the samples for p1 and p2
p1_samples = posterior_samples["p1"]
p2_samples = posterior_samples["p2"]

# Calculate delta1 and delta2
delta1_samples = p1_samples - p2_samples
delta2_samples = (p1_samples - p2_samples) / p1_samples

# Summary statistics
print("Summary of delta1:")
print(f"Mean: {delta1_samples.mean().item():.4f}")
print(f"Median: {delta1_samples.median().item():.4f}")
print(
    f"95% CI: [{delta1_samples.quantile(0.025).item():.4f}, {delta1_samples.quantile(0.975).item():.4f}]"
)

print("\nSummary of delta2:")
print(f"Mean: {delta2_samples.mean().item():.4f}")
print(f"Median: {delta2_samples.median().item():.4f}")
print(
    f"95% CI: [{delta2_samples.quantile(0.025).item():.4f}, {delta2_samples.quantile(0.975).item():.4f}]"
)


# Plotting the distributions
plt.figure(figsize=(12, 5))

plt.subplot(1, 2, 1)
plt.hist(delta1_samples.numpy(), bins=30, color="blue", alpha=0.7)
plt.title("Posterior Distribution of $\\delta_1$ (ARR)")
plt.xlabel("$\\delta_1$")
plt.ylabel("Frequency")

plt.subplot(1, 2, 2)
plt.hist(delta2_samples.numpy(), bins=30, color="green", alpha=0.7)
plt.title("Posterior Distribution of $\\delta_2$ (RRR)")
plt.xlabel("$\\delta_2$")
plt.ylabel("Frequency")

plt.tight_layout()
plt.show()


# Initialize the Predictive object with number of posterior samples to generate
predictive = Predictive(model, posterior_samples=posterior_samples, num_samples=1000)

# Generate posterior predictive samples
post_pred_samples = predictive(data_killed, data_exposed)


# distribution of killed children

# Convert predictive samples into numpy for easier handling
post_pred_pre = post_pred_samples["obs_pre"].numpy()
post_pred_post = post_pred_samples["obs_post"].numpy()
print(f"post_pred_pre {post_pred_pre}")
print(f"post_pred_pre shape {post_pred_pre.shape}")
print(f"data_killed[:92] {data_killed[:92]}")
print(f"post_pred_post {post_pred_post}")

# Plotting
plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.hist(
    post_pred_pre.flatten(),
    bins=30,
    alpha=0.75,
    color="blue",
    label="Predictive Pre-Intervention",
)
plt.hist(
    np.tile(
        data_killed[:92], (1000, 1)
    ).flatten(),  # Use np.tile to repeat the array 1000 times along a new axis
    bins=30,
    alpha=0.75,
    color="red",
    label="Observed Pre-Intervention",
    histtype="step",
    linewidth=4,
)
plt.title("Posterior Predictive Check: Pre-Intervention")
plt.legend()
# plt.show()

plt.subplot(1, 2, 2)
plt.hist(
    post_pred_post.flatten(),
    bins=30,
    alpha=0.75,
    color="green",
    label="Predictive Post-Intervention",
)
plt.hist(
    np.tile(data_killed[93:], (1000, 1)).flatten(),
    bins=30,
    alpha=0.75,
    color="red",
    label="Observed Post-Intervention",
    histtype="step",
    linewidth=2,
)
plt.title("Posterior Predictive Check: Post-Intervention")
plt.legend()

plt.show()


# Function to compute first-order autocorrelation
def autocorrelation(data):
    return np.corrcoef(data[:-1], data[1:])[0, 1]


# Calculate autocorrelation for each predictive sample
autocorr_pred_pre = np.array([autocorrelation(sample) for sample in post_pred_pre])
autocorr_pred_post = np.array([autocorrelation(sample) for sample in post_pred_post])

# Calculate autocorrelation for observed data
# Assuming 'data_killed' and 'data_exposed' are your observed datasets up to and after intervention
autocorr_obs_pre = autocorrelation(
    data_killed[:92].numpy()
)  # Make sure it's a numpy array
autocorr_obs_post = autocorrelation(data_killed[93:].numpy())

# Plotting
plt.hist(
    autocorr_pred_pre,
    bins=30,
    alpha=0.75,
    color="blue",
    label="Predictive Autocorrelation Pre",
)
plt.hist(
    autocorr_pred_post,
    bins=30,
    alpha=0.75,
    color="blue",
    label="Predictive Autocorrelation Post",
)
plt.axvline(
    x=autocorr_obs_pre,
    color="red",
    linestyle="--",
    label="Observed Autocorrelation Pre-Intervention",
)
plt.axvline(
    x=autocorr_obs_post,
    color="green",
    linestyle="--",
    label="Observed Autocorrelation Post-Intervention",
)
plt.title("Distribution of First-Order Autocorrelations")
plt.xlabel("Autocorrelation")
plt.ylabel("Frequency")
plt.legend()
plt.show()

# Report summary statistics
print("Mean Predictive Pre-Intervention:", np.mean(autocorr_pred_pre))
print("Median Predictive Pre-Intervention:", np.median(autocorr_pred_pre))
print("Mean Predictive Post-Intervention:", np.mean(autocorr_pred_post))
print("Median Predictive Post-Intervention:", np.median(autocorr_pred_post))
print("Observed Autocorrelation Pre-Intervention:", autocorr_obs_pre)
print("Observed Autocorrelation Post-Intervention:", autocorr_obs_post)

# zero inflation
# Count zeros in the posterior predictive samples
zero_count_pre_pred = (post_pred_pre == 0).astype(float).mean(axis=0).mean()
zero_count_post_pred = (post_pred_post == 0).astype(float).mean(axis=0).mean()

# Observed zero counts
zero_count_pre_obs = (data_killed[:92] == 0).float().mean()
zero_count_post_obs = (data_killed[93:] == 0).float().mean()

print(f"Predicted Zero Fraction Pre-Intervention: {zero_count_pre_pred:.2f}")
print(f"Predicted Zero Fraction Post-Intervention: {zero_count_post_pred:.2f}")
print(f"Observed Zero Fraction Pre-Intervention: {zero_count_pre_obs:.2f}")
print(f"Observed Zero Fraction Post-Intervention: {zero_count_post_obs:.2f}")
