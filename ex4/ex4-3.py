import torch
import pyro
import pyro.distributions as dist
from pyro.infer import MCMC, NUTS
import matplotlib.pyplot as plt


# Define the model using Pyro
def model(data):
    # Inverse Gamma Prior for σ^2
    alpha = 3.0  # Shape parameter
    beta = 2.0  # Rate parameter
    sigma2 = pyro.sample("sigma2", dist.InverseGamma(alpha, beta))
    sigma = torch.sqrt(sigma2)  # Convert variance σ^2 to standard deviation σ

    # Normal Prior for μ using σ derived from σ^2
    mu = pyro.sample("mu", dist.Normal(10, sigma))

    # Likelihood
    with pyro.plate("data", len(data)):
        pyro.sample("obs", dist.Normal(mu, sigma), obs=data)


# Generating synthetic data
torch.manual_seed(0)  # For reproducibility
data = torch.normal(12, torch.tensor(1.5), size=(100,))

# Setting up MCMC
nuts_kernel = NUTS(model)
mcmc = MCMC(nuts_kernel, num_samples=1000, warmup_steps=300)
mcmc.run(data)

# Extracting posterior samples
posterior_samples = mcmc.get_samples()
mu_samples = posterior_samples["mu"].numpy()

# Plotting the posterior distribution of μ
plt.hist(mu_samples, bins=30, density=True)
plt.title("Posterior Distribution of $\\mu$")
plt.xlabel("$\\mu$")
plt.ylabel("Density")
plt.show()
