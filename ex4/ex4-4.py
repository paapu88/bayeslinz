import numpy as np
import pandas as pd
import torch
import pyro
import pyro.distributions as dist
from pyro.infer import MCMC, NUTS
from pathlib import Path


def load_data(file):
    return pd.read_csv(file, header=None).values.squeeze()


mydir = Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/ex4")
data1 = load_data(mydir.joinpath("school1.dat"))
data2 = load_data(mydir.joinpath("school2.dat"))
data3 = load_data(mydir.joinpath("school3.dat"))


def model(data, mu0=5, sigma0=2, kappa0=1, nu0=2):
    # Prior hyperparameters, page 68, 72 in lecture notes
    # mu|sigma**2 ~N(mu0,1/kappa0*sigma**2)
    # sigma2 ~G-1(nu0/2,nu0/(2*sigma0**2))
    mu0 = 5.0
    lambda0 = 1.0  # Precision related to variance, kappa0 in some contexts
    alpha0 = 1.0  # Corresponds to nu0/2
    beta0 = 2.0  # Corresponds to nu0 * sigma20 / 2

    # Priors for mu and sigma^2
    sigma2 = pyro.sample("sigma2", dist.InverseGamma(nu0 / 2, nu0 / (2 * sigma0**2)))
    mu = pyro.sample("mu", dist.Normal(mu0, 1 / kappa0 * sigma2))

    # Get posteriori
    with pyro.plate("data", size=len(data)):
        pyro.sample("obs", dist.Normal(mu, sigma2), obs=torch.tensor(data))

    return mu, sigma2


def infer_and_summarize(data):
    nuts_kernel = NUTS(model)
    mcmc = MCMC(nuts_kernel, num_samples=1000, warmup_steps=200)
    mcmc.run(data)
    samples = mcmc.get_samples()
    mu_posterior = samples["mu"].numpy()
    sigma2_posterior = samples["sigma2"].numpy()

    # Compute posterior summaries
    mu_mean = np.mean(mu_posterior)
    mu_ci = np.percentile(mu_posterior, [2.5, 97.5])
    sigma2_mean = np.mean(sigma2_posterior)
    sigma2_ci = np.percentile(sigma2_posterior, [2.5, 97.5])

    return mu_mean, mu_ci, sigma2_mean, sigma2_ci, mu_posterior


# Analysis for each school
results1 = infer_and_summarize(data1)
results2 = infer_and_summarize(data2)
results3 = infer_and_summarize(data3)

print(
    f"School 1: Mu Mean = {results1[0]}, Mu 95% CI = {results1[1]}, Sigma^2 Mean = {results1[2]}, Sigma^2 95% CI = {results1[3]}"
)
print(
    f"School 2: Mu Mean = {results2[0]}, Mu 95% CI = {results2[1]}, Sigma^2 Mean = {results2[2]}, Sigma^2 95% CI = {results2[3]}"
)
print(
    f"School 3: Mu Mean = {results3[0]}, Mu 95% CI = {results3[1]}, Sigma^2 Mean = {results3[2]}, Sigma^2 95% CI = {results3[3]}"
)


def compare_posteriors(mu_post1, mu_post2, mu_post3):

    combinations = [
        (mu_post1, mu_post2, mu_post3),
        (mu_post1, mu_post3, mu_post2),
        (mu_post2, mu_post1, mu_post3),
        (mu_post2, mu_post3, mu_post1),
        (mu_post3, mu_post1, mu_post2),
        (mu_post3, mu_post2, mu_post1),
    ]
    results = []
    for combo in combinations:
        # print(combo)
        count = sum(1 for x, y, z in zip(*combo) if x < y < z)
        probability = count / len(combo[0])
        results.append(probability)
    return results


mu_posterior1 = results1[-1]
mu_posterior2 = results2[-1]
mu_posterior3 = results3[-1]

# Assuming `samples` dictionary from MCMC contains all mu_posterior arrays
probabilities = compare_posteriors(mu_posterior1, mu_posterior2, mu_posterior3)
print(f"1<2<3 {probabilities[0]}")
print(f"1<3<2 {probabilities[1]}")
print(f"2<1<3 {probabilities[2]}")
print(f"2<3<1 {probabilities[3]}")
print(f"3<1<2 {probabilities[4]}")
print(f"3<2<1 {probabilities[5]}")

""" 
results:
School 1: Mu Mean = 9.272611069304293, Mu 95% CI = [ 7.76198108 10.75174359], Sigma^2 Mean = 3.9848628622818696, Sigma^2 95% CI = [3.06760986 5.27638068]
School 2: Mu Mean = 6.9438321646440855, Mu 95% CI = [5.13072768 8.72145391], Sigma^2 Mean = 4.446893521209084, Sigma^2 95% CI = [3.33548177 6.06951666]
School 3: Mu Mean = 7.785723293111467, Mu 95% CI = [6.11286476 9.36199249], Sigma^2 Mean = 3.7884147002089326, Sigma^2 95% CI = [2.84331316 5.23026433]
1<2<3 0.008
1<3<2 0.003
2<1<3 0.088
2<3<1 0.669
3<1<2 0.013
3<2<1 0.219

"""
