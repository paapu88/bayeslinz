import numpy as np
import matplotlib.pyplot as plt

# Given constants
mu0 = 12
sigma = 1.5
m0 = 10


# Function to calculate MSEs
def calculate_mses(sigma, kappa0, n):
    # Calculate the MLE variance
    mse_mle = sigma**2 / n  # + (mu0 - m0) ** 2

    # Calculate the posterior mean MSE
    mn = (kappa0 * m0 + n * mu0) / (kappa0 + n)
    sigma_n2 = sigma**2 / (kappa0 + n)
    mse_posterior = sigma_n2 + (mn - mu0) ** 2

    return mse_posterior, mse_mle


# Sample x
sample_size = np.arange(1, 101, 1)

# Kappa values
kappa_values = [1, 2, 3]

# Store results
results = {kappa: [] for kappa in kappa_values}
for kappa0 in kappa_values:
    for n in sample_size:
        mse_posterior, mse_mle = calculate_mses(sigma=sigma, kappa0=kappa0, n=n)
        results[kappa0].append(mse_posterior / mse_mle)

# Plotting the MSE ratio
plt.figure(figsize=(10, 6))
for kappa0 in kappa_values:
    plt.plot(sample_size, results[kappa0], label=f"κ0 = {kappa0}")
plt.xlabel("Sample Size n")
plt.ylabel("Ratio of MSEs (Posterior/MLE)")
plt.title("Ratio of MSE of Posterior Mean to MLE for Different κ0")
plt.legend()
plt.grid(True)
plt.show()


def plot_sampling_distribution(kappa0, sample_sizes):
    plt.figure(figsize=(12, 6))
    for n in sample_sizes:
        samples = np.random.normal(mu0, sigma, (10000, n))
        mle_means = np.mean(samples, axis=1)
        posterior_means = (kappa0 * m0 + n * mle_means) / (kappa0 + n)

        plt.hist(mle_means, bins=30, alpha=0.5, label=f"MLE n={n}")
        plt.hist(posterior_means, bins=30, alpha=0.5, label=f"Posterior Mean n={n}")

    plt.xlabel("Estimates of µ")
    plt.ylabel("Frequency")
    plt.title(f"Sampling Distribution of MLE and Posterior Mean (κ0={kappa0})")
    plt.legend()
    plt.grid(True)
    plt.show()


# Plot for each kappa0 and sample size combinations
for kappa0 in kappa_values:
    plot_sampling_distribution(kappa0, [10, 100])
