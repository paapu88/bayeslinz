import numpyro
import numpyro.distributions as dist
from numpyro.infer import MCMC, NUTS
import jax.numpy as jnp
from jax import random
import numpy as np
import gc
import scipy.stats as stats
import matplotlib.pyplot as plt

# plot some gamma functions
x = np.linspace(0, 20, 100)
for n0 in [1, 10, 20, 30, 40, 50]:
    y = stats.gamma.pdf(x, a=12 * n0, scale=1 / n0)
    print(f"x {x}")
    print(f"y {y}")
    # plt.plot(x, y, "y-", label=f"$\alpha=$ {12*n0}, $\beta=${n0}")
    plt.plot(x, y, label=r"$\alpha={}$, $\beta={}$".format(12 * n0, n0))

    plt.ylim([0, 0.8])
    plt.xlim([0, 20])
    plt.title("PDF of the Gamma Distribution")
    plt.xlabel("x")
    plt.ylabel("Probability density")
    plt.legend()
    plt.show()

# Observed tumor counts
yA = jnp.array([12, 9, 12, 14, 13, 13, 15, 8, 15, 6])
yB = jnp.array([11, 11, 10, 9, 9, 8, 7, 10, 6, 8, 8, 9, 7])


# Model definition
def model(yA, yB, n0=1):
    thetaA = numpyro.sample("thetaA", dist.Gamma(120, 10))
    thetaB = numpyro.sample("thetaB", dist.Gamma(12 * n0, 1 * n0))

    with numpyro.plate("A", size=len(yA)):
        numpyro.sample("obs_A", dist.Poisson(thetaA), obs=yA)
    with numpyro.plate("B", size=len(yB)):
        numpyro.sample("obs_B", dist.Poisson(thetaB), obs=yB)


# Run the sampler
for n0 in range(1, 51):
    # MCMC inference
    nuts_kernel = NUTS(model)

    mcmc = MCMC(nuts_kernel, num_warmup=500, num_samples=2000)
    rng_key = random.PRNGKey(n0)
    mcmc.run(rng_key, yA=yA, yB=yB, n0=n0)

    # Get the posterior samples
    posterior_samples = mcmc.get_samples()

    # Calculate posterior means, variances, and 95% confidence intervals
    thetaA_mean = jnp.mean(posterior_samples["thetaA"])
    thetaB_mean = jnp.mean(posterior_samples["thetaB"])

    thetaA_variance = jnp.var(posterior_samples["thetaA"])
    thetaB_variance = jnp.var(posterior_samples["thetaB"])

    thetaA_CI = jnp.quantile(
        np.array(posterior_samples["thetaA"]), np.array([0.025, 0.975])
    )
    thetaB_CI = jnp.quantile(
        np.array(posterior_samples["thetaB"]), np.array([0.025, 0.975])
    )

    # Print results
    if n0 == 1:
        print(
            f"ThetaA Mean: {thetaA_mean}, Variance: {thetaA_variance}, 95% CI: {thetaA_CI}"
        )
    print(
        f"n0: {n0}, ThetaB Mean: {thetaB_mean}, Variance: {thetaB_variance}, 95% CI: {thetaB_CI}"
    )
    del mcmc
    del nuts_kernel
    gc.collect()  # Force garbage collection

# 1b)
""" 
       ThetaA Mean: 11.877936363220215, Variance: 0.6005240082740784, 95% CI: [10.421391 13.397272]

ThetaB:
n0: 1, ThetaB Mean: 8.91407299041748, Variance: 0.6077957153320312, 95% CI: [ 7.4189796 10.501826 ]
n0: 2, ThetaB Mean: 9.110490798950195, Variance: 0.6867775917053223, 95% CI: [ 7.5676336 10.758744 ]
n0: 3, ThetaB Mean: 9.311057090759277, Variance: 0.5554430484771729, 95% CI: [ 7.9358225 10.818633 ]
n0: 4, ThetaB Mean: 9.447067260742188, Variance: 0.5360912680625916, 95% CI: [ 8.021968 10.898472]
n0: 5, ThetaB Mean: 9.57656478881836, Variance: 0.5477946400642395, 95% CI: [ 8.128188 11.060367]
n0: 6, ThetaB Mean: 9.742545127868652, Variance: 0.5061847567558289, 95% CI: [ 8.404947 11.135838]
n0: 7, ThetaB Mean: 9.854422569274902, Variance: 0.47320467233657837, 95% CI: [ 8.513563 11.231485]
n0: 8, ThetaB Mean: 9.973910331726074, Variance: 0.4984336495399475, 95% CI: [ 8.635432 11.416642]
n0: 9, ThetaB Mean: 10.016932487487793, Variance: 0.474261611700058, 95% CI: [ 8.720787 11.411123]
n0: 10, ThetaB Mean: 10.12143611907959, Variance: 0.4442645013332367, 95% CI: [ 8.898554 11.47357 ]
n0: 11, ThetaB Mean: 10.204277992248535, Variance: 0.40194275975227356, 95% CI: [ 9.089207 11.504581]
n0: 12, ThetaB Mean: 10.298463821411133, Variance: 0.41301408410072327, 95% CI: [ 9.061525 11.585954]
n0: 13, ThetaB Mean: 10.346102714538574, Variance: 0.39869582653045654, 95% CI: [ 9.144056 11.588876]
n0: 14, ThetaB Mean: 10.413115501403809, Variance: 0.38811802864074707, 95% CI: [ 9.215722 11.558053]
n0: 15, ThetaB Mean: 10.463301658630371, Variance: 0.3539835512638092, 95% CI: [ 9.340181 11.621922]
n0: 16, ThetaB Mean: 10.500946998596191, Variance: 0.37684938311576843, 95% CI: [ 9.308376 11.715233]
n0: 17, ThetaB Mean: 10.56679916381836, Variance: 0.37723901867866516, 95% CI: [ 9.4036255 11.789311 ]
n0: 18, ThetaB Mean: 10.599955558776855, Variance: 0.3328596353530884, 95% CI: [ 9.450894 11.777737]
n0: 19, ThetaB Mean: 10.641974449157715, Variance: 0.34631678462028503, 95% CI: [ 9.569004 11.859378]
n0: 20, ThetaB Mean: 10.676639556884766, Variance: 0.3466830849647522, 95% CI: [ 9.555391 11.853471]
n0: 21, ThetaB Mean: 10.734131813049316, Variance: 0.3303866684436798, 95% CI: [ 9.665609 11.917868]
n0: 22, ThetaB Mean: 10.798308372497559, Variance: 0.3190554976463318, 95% CI: [ 9.711332 11.89169 ]
n0: 23, ThetaB Mean: 10.803248405456543, Variance: 0.28850287199020386, 95% CI: [ 9.798963 11.894546]
n0: 24, ThetaB Mean: 10.852330207824707, Variance: 0.29252538084983826, 95% CI: [ 9.819795 11.918384]
n0: 25, ThetaB Mean: 10.87019157409668, Variance: 0.2973760664463043, 95% CI: [ 9.827168 11.960111]
n0: 26, ThetaB Mean: 10.861114501953125, Variance: 0.2827610671520233, 95% CI: [ 9.810521 11.90368 ]
n0: 27, ThetaB Mean: 10.916648864746094, Variance: 0.252257764339447, 95% CI: [ 9.92219  11.904775]
n0: 28, ThetaB Mean: 10.947772026062012, Variance: 0.2921364903450012, 95% CI: [ 9.890443 11.984283]
n0: 29, ThetaB Mean: 10.983479499816895, Variance: 0.2855760157108307, 95% CI: [ 9.960561 12.017521]
n0: 30, ThetaB Mean: 10.996171951293945, Variance: 0.2525263726711273, 95% CI: [ 9.995178 11.979115]
n0: 31, ThetaB Mean: 11.024552345275879, Variance: 0.2471354752779007, 95% CI: [10.080693 12.025192]
n0: 32, ThetaB Mean: 11.063934326171875, Variance: 0.2585068345069885, 95% CI: [10.125953 12.150331]
n0: 33, ThetaB Mean: 11.066184043884277, Variance: 0.2413175106048584, 95% CI: [10.135598 12.045285]
n0: 34, ThetaB Mean: 11.081932067871094, Variance: 0.224879190325737, 95% CI: [10.193459 12.023248]
n0: 35, ThetaB Mean: 11.110986709594727, Variance: 0.21236920356750488, 95% CI: [10.220908 11.987799]
n0: 36, ThetaB Mean: 11.117722511291504, Variance: 0.2181437462568283, 95% CI: [10.208564 12.084356]
n0: 37, ThetaB Mean: 11.16163158416748, Variance: 0.22863081097602844, 95% CI: [10.259493 12.121243]
n0: 38, ThetaB Mean: 11.137994766235352, Variance: 0.21900354325771332, 95% CI: [10.229431 12.05819 ]
n0: 39, ThetaB Mean: 11.167530059814453, Variance: 0.2202254682779312, 95% CI: [10.282712 12.08107 ]
n0: 40, ThetaB Mean: 11.188859939575195, Variance: 0.22867092490196228, 95% CI: [10.327635 12.208654]
n0: 41, ThetaB Mean: 11.209073066711426, Variance: 0.21042224764823914, 95% CI: [10.313672 12.121278]
n0: 42, ThetaB Mean: 11.22357177734375, Variance: 0.20311127603054047, 95% CI: [10.363823 12.094032]
n0: 43, ThetaB Mean: 11.228507995605469, Variance: 0.1818089336156845, 95% CI: [10.384664 12.07297 ]
n0: 44, ThetaB Mean: 11.260927200317383, Variance: 0.2084122896194458, 95% CI: [10.328975 12.175216]
n0: 45, ThetaB Mean: 11.271180152893066, Variance: 0.19387468695640564, 95% CI: [10.427811 12.141804]
n0: 46, ThetaB Mean: 11.287555694580078, Variance: 0.18904297053813934, 95% CI: [10.483807 12.165992]
n0: 47, ThetaB Mean: 11.286527633666992, Variance: 0.20535102486610413, 95% CI: [10.439785 12.201081]
n0: 48, ThetaB Mean: 11.308095932006836, Variance: 0.1850898563861847, 95% CI: [10.46956  12.144195]
n0: 49, ThetaB Mean: 11.30960750579834, Variance: 0.18694938719272614, 95% CI: [10.459094 12.157238]
n0: 50, ThetaB Mean: 11.328109741210938, Variance: 0.18105174601078033, 95% CI: [10.537497 12.170794]
"""
"""
The prior belief for B to get its mean close to A should be such that n0 is big.
The prior peak pof pdf moves towards bigger values with increasing n0 and gets more concentrated. 
When n0=50 its peak is at about 12.0 
"""

# 1c)
# It is said in text " but type B mice are related to type A mice"
# so it is likely that knowing results of mice A can be used in predictions
# for mice B, so one cannot think A and B would be independend,
# Thus P(thetaA, thetaB)=P(thetaA)* P(thetaB)
# is not true.
