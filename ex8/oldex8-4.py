import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats

# Parameters
n = 200
pi = 0.5
mu1 = 0
sigma2 = 1
sigma = np.sqrt(sigma2)
mu2_values = [1, 3, 5]

# Generate data sets
data_sets = []
for mu2 in mu2_values:
    data = []
    for _ in range(n):
        if np.random.rand() < pi:
            data.append(np.random.normal(mu1, sigma))
        else:
            data.append(np.random.normal(mu2, sigma))
    data_sets.append(np.array(data))

# Plot the generated data
for i, data in enumerate(data_sets):
    plt.figure(figsize=(8, 4))
    plt.hist(data, bins=30, alpha=0.7, label=f"Data set with $\mu_2 = {mu2_values[i]}$")
    plt.legend()
    plt.show()


# Generate group indicators
def generate_group_indicators(n, pi):
    return np.random.binomial(1, pi, n)


# Bayesian analysis assuming group indicators are observed
def bayesian_analysis_known_indicators(data, indicators):
    n1 = np.sum(indicators)
    n2 = len(indicators) - n1
    mean1 = np.mean(data[indicators == 1])
    mean2 = np.mean(data[indicators == 0])
    return mean1, mean2


# Perform analysis on each data set
for i, data in enumerate(data_sets):
    indicators = generate_group_indicators(n, pi)
    mean1, mean2 = bayesian_analysis_known_indicators(data, indicators)
    print(
        f"For data set with $\mu_2 = {mu2_values[i]}$: Estimated means are $\mu_1$ = {mean1:.2f} and $\mu_2$ = {mean2:.2f}"
    )
