import numpy as np
import pandas as pd
from scipy.stats import multivariate_normal, invgamma
import pandas as pd
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri

# Activate the automatic conversion
pandas2ri.activate()

# Import the R package MASS
MASS = importr("MASS")

# Load the dataset
Pima_tr = robjects.r["Pima.tr"]

# Convert R dataframe to pandas dataframe
data = pandas2ri.rpy2py(Pima_tr)

# Display the first few rows of the dataframe
print(data.head())
print(data.describe())

# Split data into predictors and outcome
X = data.iloc[:, :-1].values
y = data.iloc[:, -1].values - 1
print(f"X shape with NO added unit vector for beta0 {X.shape}")
print(f"y shape {y.shape}")
print(y)
# sys.exit()

# Unit vector (column of ones) with the same number of rows as the matrix
unit_vector = np.ones((X.shape[0], 1))
# Add unit vector as the first column
# Standardize the predictors
# X = (X - X.mean(axis=0)) / X.std(axis=0)
X = np.hstack((unit_vector, X))
# Count the number of NaNs
num_nans = np.isnan(X).sum()
print(f"Number of NaNs in the array: {num_nans}")
print(f"X shape with added unit vector for beta0 {X.shape}")

# Prior parameters
nu0 = 1
sigma2 = 1

# Number of iterations
n_iter = 5000

# Initialize beta and Sigma
beta = np.linalg.inv(X.T @ X) @ (X.T @ y)  # OLS
# beta = np.insert(beta, 0, 1.0)  # beta0
print(f"beta ols: {beta}")
n = X.shape[0]
p = X.shape[1]
# beta = np.zeros(p)
Sigma = np.eye(p, p)

# Storage for samples
beta_samples = np.zeros((n_iter, p))
sigma2_samples = np.zeros(n_iter)

for i in range(n_iter):

    # Update beta
    inv_Sigma = np.linalg.inv(Sigma)
    Var_beta = np.linalg.inv(inv_Sigma + X.T @ X / sigma2)
    E_beta = Var_beta @ (inv_Sigma * beta + X.T @ y / sigma2)
    # print(f"E_beta.shape {E_beta.shape} eb {E_beta}")
    beta = multivariate_normal.rvs(np.diag(E_beta), Var_beta)
    # print(f"y {y.shape}")
    # print(f"X {X.shape}")
    # print(f"beta {beta.shape}")

    # Update sigma2
    SSR = np.sum((y - X @ beta) ** 2)
    alpha = (nu0 + n) / 2
    beta_param = (nu0 * sigma2 + SSR) / 2
    sigma2 = invgamma.rvs(alpha, 1 / beta_param)

    # Store samples
    beta_samples[i, :] = beta
    sigma2_samples[i] = sigma2

# Step 3: Analyze the results
beta_mean = beta_samples.mean(axis=0)
beta_std = beta_samples.std(axis=0)
sigma2_mean = sigma2_samples.mean()
sigma2_std = sigma2_samples.std()

print("Posterior mean of beta:", beta_mean)
print("Posterior std of beta:", beta_std)
print("Posterior mean of sigma2:", sigma2_mean)
print("Posterior std of sigma2:", sigma2_std)
