import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
from scipy.linalg import inv
from numpy.random import default_rng


# Step 1: Generate data sets
def generate_mixture_data(n, pi, mu1, mu2, sigma2):
    component = np.random.binomial(1, pi, n)
    y = np.where(
        component == 1,
        np.random.normal(mu1, np.sqrt(sigma2), n),
        np.random.normal(mu2, np.sqrt(sigma2), n),
    )
    return y, component


n = 200
pi = 0.5
mu1 = 0
sigma2 = 1
mu2_values = [1, 3, 5]

data_sets = [generate_mixture_data(n, pi, mu1, mu2, sigma2) for mu2 in mu2_values]


# Step 2: Bayesian analysis with observed group indicators
def gibbs_sampler_known_indicators(
    y, component, mu1_prior, mu2_prior, sigma2, n_iter=1000
):
    # Initialize variables
    rng = default_rng()
    n1 = np.sum(component == 1)
    n2 = np.sum(component == 0)
    m0_1, m0_2 = mu1_prior, mu2_prior
    M0_1, M0_2 = 1, 1  # Prior variances
    e0_1, e0_2 = 1, 1  # Parameters for Beta distribution
    nu0_1, nu0_2 = 2, 2  # Degrees of freedom for Wishart distribution
    S0_1, S0_2 = 1, 1  # Scale matrices for Wishart distribution
    mu1_samples = []
    mu2_samples = []

    # Starting values
    Sigma1 = sigma2
    Sigma2 = sigma2

    for _ in range(n_iter):
        # Sample eta
        eta = rng.beta(n1 + e0_1, n2 + e0_2)

        # Sample mu1
        y1 = y[component == 1]
        y1_mean = np.mean(y1)
        Mn_1 = 1 / (1 / M0_1 + n1 / Sigma1)
        mn_1 = Mn_1 * (m0_1 / M0_1 + n1 * y1_mean / Sigma1)
        mu1 = rng.normal(mn_1, np.sqrt(Mn_1))

        # Sample mu2
        y2 = y[component == 0]
        y2_mean = np.mean(y2)
        Mn_2 = 1 / (1 / M0_2 + n2 / Sigma2)
        mn_2 = Mn_2 * (m0_2 / M0_2 + n2 * y2_mean / Sigma2)
        mu2 = rng.normal(mn_2, np.sqrt(Mn_2))

        # Sample Sigma1
        Sn_1 = S0_1 + 0.5 * np.sum((y1 - mu1) ** 2)
        Sigma1 = stats.invwishart.rvs(df=nu0_1 + n1 / 2, scale=Sn_1)

        # Sample Sigma2
        Sn_2 = S0_2 + 0.5 * np.sum((y2 - mu2) ** 2)
        Sigma2 = stats.invwishart.rvs(df=nu0_2 + n2 / 2, scale=Sn_2)

        mu1_samples.append(mu1)
        mu2_samples.append(mu2)

    return np.array(mu1_samples), np.array(mu2_samples)


mu1_prior = 0
mu2_prior = 0
n_iter = 1000

for i, (y, component) in enumerate(data_sets):
    mu1_samples, mu2_samples = gibbs_sampler_known_indicators(
        y, component, mu1_prior, mu2_prior, sigma2, n_iter
    )
    plt.figure(figsize=(10, 4))
    plt.subplot(1, 2, 1)
    plt.plot(mu1_samples)
    plt.title(f"Trace plot of mu1 (Dataset {i+1})")
    plt.subplot(1, 2, 2)
    plt.plot(mu2_samples)
    plt.title(f"Trace plot of mu2 (Dataset {i+1})")
    plt.tight_layout()
    plt.show()
