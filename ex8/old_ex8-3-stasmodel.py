import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import pandas as pd
import statsmodels.api as smf

# np.random.seed(0)

# Simulate data
n = 100  # number of observations
p = 3  # number of predictors
X = np.random.randn(n, p)
beta_true = np.array([0.5, -1.0, 1.5])
Z = X @ beta_true + np.random.randn(n)
y = (Z > 0).astype(int)

probit_model = smf.Probit(y, X)
result = probit_model.fit()
print(result.summary2())
