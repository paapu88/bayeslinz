import numpy as np
import matplotlib.pyplot as plt

# Parameters
mu = [0, 0]
rho = 1.5
sigma = [[1, rho], [rho, 1]]

# Generate data
data = np.random.multivariate_normal(mu, sigma, 1000)

# Plot the data
plt.scatter(data[:, 0], data[:, 1], alpha=0.5)
plt.title("Bivariate Normal Distribution")
plt.xlabel("X1")
plt.ylabel("X2")
plt.grid(True)
plt.show()
