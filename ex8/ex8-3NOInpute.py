import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt

# np.random.seed(0)

# Simulate data
n = 1000  # Number of samples
X = np.random.randn(n, 2)  # Two covariates
beta_true = np.array([3, 0.5, -5.0])  # True coefficients
# Add intercept
X = np.hstack([np.ones((n, 1)), X])
z = X @ beta_true + np.random.randn(n)  # Latent variable
y = (z > 0).astype(int)  # Observed binary outcome

# Introduce missingness
missing_rate = 0.1
missing_mask = np.random.rand(n, 2) < missing_rate
X_missing = X.copy()
X_missing[:, 1:][missing_mask] = np.nan  # Apply missingness to the covariates only
# print(X_missing[0:10, :])
# sys.exit()


def sample_beta(X, z, beta_prior_mean, beta_prior_cov):
    """
    Sample beta from the posterior distribution.
    https://bookdown.org/aramir21/IntroductionBayesianEconometricsGuidedTour/probit-model.html
    Chap 6.3
    and page 77 in Helga notes
    """
    beta_post_cov = np.linalg.inv(np.linalg.inv(beta_prior_cov) + X.T @ X)

    beta_post_mean = beta_post_cov @ (
        np.linalg.inv(beta_prior_cov) @ beta_prior_mean + X.T @ z
    )

    return np.random.multivariate_normal(beta_post_mean, beta_post_cov)


def sample_z(X, y, beta):
    """
    Sample z from the truncated normal distribution.
    """
    mean = X @ beta
    z = np.zeros_like(mean)
    for i in range(len(y)):
        if y[i] == 1:
            z[i] = stats.truncnorm.rvs(
                a=(0 - mean[i]) / 1, b=np.inf, loc=mean[i], scale=1
            )
        else:
            z[i] = stats.truncnorm.rvs(
                a=-np.inf, b=(0 - mean[i]) / 1, loc=mean[i], scale=1
            )
    return z


def impute_missing_values(X, beta):
    """
    Impute missing values in X using the conditional distribution.
    """
    X_imputed = X.copy()
    for i in range(X.shape[0]):
        for j in range(
            1, X.shape[1]
        ):  # Start from 1 to avoid changing the intercept term
            if np.isnan(X[i, j]):
                # Impute missing value based on the observed part of X and the current beta
                observed_indices = ~np.isnan(X[i, :])
                mean = X[i, observed_indices] @ beta[observed_indices]
                X_imputed[i, j] = np.random.normal(mean, 1)
    return X_imputed


# Initialize parameters
n_iter = 5000
beta_prior_mean = np.zeros(X.shape[1])
beta_prior_cov = np.eye(X.shape[1])
beta_samples = np.zeros((n_iter, X.shape[1]))
z_samples = np.zeros((n_iter, n))

# Initial values
beta = np.zeros(X.shape[1])
z = np.random.randn(n)
X_imputed = impute_missing_values(X_missing, beta)

# Gibbs sampling
for i in range(n_iter):
    z = sample_z(X, y, beta)
    beta = sample_beta(X, z, beta_prior_mean, beta_prior_cov)
    # X_imputed = impute_missing_values(X_missing, beta)
    beta_samples[i, :] = beta
    print(f"i {i} beta {beta}")
    z_samples[i, :] = z

# Posterior mean of beta
beta_mean = beta_samples.mean(axis=0)
print("Posterior mean of beta:", beta_mean)

# Plot the trace and posterior distribution of beta
fig, axes = plt.subplots(2, 1, figsize=(10, 6))

for j in range(beta_samples.shape[1]):
    axes[0].plot(beta_samples[:, j], label=f"beta_{j}")
axes[0].set_title("Trace plot of beta")
axes[0].legend()

for j in range(beta_samples.shape[1]):
    axes[1].hist(
        beta_samples[:, j], bins=30, alpha=0.5, density=True, label=f"beta_{j}"
    )
axes[1].set_title("Posterior distribution of beta")
axes[1].legend()

plt.tight_layout()
plt.show()

# Evaluate the results
print("True beta:", beta_true)
print("Estimated beta:", beta_mean)

# Plot the original, missing, and imputed data
fig, axes = plt.subplots(3, 1, figsize=(10, 15))

axes[0].scatter(X[:, 1], X[:, 2], alpha=0.5, label="Original Data")
axes[0].set_title("Original Data")
axes[0].legend()

axes[1].scatter(
    X_missing[:, 1], X_missing[:, 2], alpha=0.5, label="Data with Missing Values"
)
axes[1].set_title("Data with Missing Values")
axes[1].legend()

axes[2].scatter(X_imputed[:, 1], X_imputed[:, 2], alpha=0.5, label="Imputed Data")
axes[2].scatter(X[:, 1], X[:, 2], alpha=0.5, label="Original Data", color="red")
axes[2].set_title("Imputed Data with Original Data Overlaid")
axes[2].set_xlabel("x1")
axes[2].set_ylabel("x2")
axes[2].legend()

plt.tight_layout()
plt.show()
