import numpy as np
import matplotlib.pyplot as plt

np.random.seed(0)  # For reproducibility

# Parameters for the bivariate normal distribution
mu = np.array([0, 0])
rho = 0.8
sigma = np.array([[1, rho], [rho, 1]])

# Simulate data
n = 1000
data = np.random.multivariate_normal(mu, sigma, n)


# Introduce missingness
def introduce_missingness(data, missing_rate=0.1):
    data_missing = data.copy()
    n, p = data.shape
    for i in range(n):
        if np.random.rand() < missing_rate:
            j = np.random.choice(p)
            data_missing[i, j] = np.nan
    return data_missing


data_missing = introduce_missingness(data)


# Gibbs sampling for missing data imputation
def gibbs_sampling(observed_data, num_samples=1000, burn_in=500):
    n, d = observed_data.shape
    mu = np.zeros(d)
    sigma = np.eye(d)
    samples = []

    for _ in range(num_samples):
        # Sample Y_mis
        for i in range(n):
            missing_indices = np.isnan(observed_data[i])
            observed_indices = ~missing_indices
            if np.any(missing_indices):
                observed_values = observed_data[i, observed_indices]
                mu_obs = mu[observed_indices]
                mu_mis = mu[missing_indices]
                sigma_obs = sigma[np.ix_(observed_indices, observed_indices)]
                sigma_mis = sigma[np.ix_(missing_indices, missing_indices)]
                sigma_mis_obs = sigma[np.ix_(missing_indices, observed_indices)]

                mu_cond = mu_mis + sigma_mis_obs @ np.linalg.inv(sigma_obs) @ (
                    observed_values - mu_obs
                )
                sigma_cond = (
                    sigma_mis
                    - sigma_mis_obs @ np.linalg.inv(sigma_obs) @ sigma_mis_obs.T
                )

                observed_data[i, missing_indices] = np.random.multivariate_normal(
                    mu_cond, sigma_cond
                )

        # Sample mu and sigma
        mu = np.mean(observed_data, axis=0)
        sigma = np.cov(observed_data, rowvar=False)

        if _ >= burn_in:
            samples.append((mu.copy(), sigma.copy()))

    return samples


# Run Gibbs sampling
observed_data = data_missing.copy()
samples = gibbs_sampling(observed_data)
imputed_data = observed_data.copy()

# Plot original, observed, and imputed data
plt.figure(figsize=(15, 5))

# Original Data
plt.subplot(1, 3, 1)
plt.scatter(data[:, 0], data[:, 1], alpha=0.5, label="Original Data", color="gray")
plt.title("Original Data")
plt.xlabel("X1")
plt.ylabel("X2")
plt.legend()
plt.grid(True)

# Observed Data (with missing values)
plt.subplot(1, 3, 2)
plt.scatter(
    data_missing[:, 0],
    data_missing[:, 1],
    alpha=0.5,
    label="Observed Data",
    color="blue",
)
plt.title("Observed Data with Missing Values")
plt.xlabel("X1")
plt.ylabel("X2")
plt.legend()
plt.grid(True)

# Imputed Data
plt.subplot(1, 3, 3)
plt.scatter(data[:, 0], data[:, 1], alpha=0.2, label="Original Data", color="gray")
fully_observed_mask = np.all(~np.isnan(data_missing), axis=1)
plt.scatter(
    imputed_data[fully_observed_mask, 0],
    imputed_data[fully_observed_mask, 1],
    alpha=0.5,
    label="Observed Data",
    color="blue",
)
partially_imputed_mask = np.any(np.isnan(data_missing), axis=1)
plt.scatter(
    imputed_data[partially_imputed_mask, 0],
    imputed_data[partially_imputed_mask, 1],
    label="Imputed Data",
    color="green",
    alpha=0.5,
)
plt.title("Imputed Data after Gibbs Sampling")
plt.xlabel("X1")
plt.ylabel("X2")
plt.legend()
plt.grid(True)

plt.tight_layout()
plt.show()
