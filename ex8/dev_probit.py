import numpy as np
import pandas as pd
import statsmodels.api as sm

# Set a random seed for reproducibility
np.random.seed(0)

# Generate synthetic data
n = 100  # number of students
study_hours = np.random.normal(5, 2, n)  # average study hours
grades = np.random.normal(70, 10, n)  # average grades

# Ensure that study_hours are non-negative
study_hours = np.clip(study_hours, 0, None)

# Adjust the coefficients and intercept to balance pass and fail
latent_variable = 0.5 * study_hours + 0.5 * grades - 50 + np.random.normal(0, 5, n)

# Pass/Fail based on the latent variable
pass_fail = (latent_variable > 0).astype(int)

# Create a DataFrame
data = pd.DataFrame({"StudyHours": study_hours, "Grades": grades, "Pass": pass_fail})

# Add a constant to the independent variables matrix
X = sm.add_constant(data[["StudyHours", "Grades"]])

# Dependent variable
y = data["Pass"]

# Fit the probit model
probit_model = sm.Probit(y, X).fit()

# Print the summary of the model
print(probit_model.summary())

# Predictions
data["PredictedProb"] = probit_model.predict(X)

# Print the first few rows of the DataFrame with predictions
print(data.head())

# Visualize the results
import matplotlib.pyplot as plt

plt.figure(figsize=(10, 6))
plt.scatter(data["StudyHours"], data["Grades"], c=data["Pass"], cmap="bwr", alpha=0.7)
plt.xlabel("Study Hours")
plt.ylabel("Grades")
plt.colorbar(label="Pass/Fail")
plt.title("Pass/Fail based on Study Hours and Grades")
plt.show()

plt.figure(figsize=(10, 6))
plt.hist(data["PredictedProb"], bins=20, alpha=0.7, color="blue")
plt.xlabel("Predicted Probability of Passing")
plt.ylabel("Frequency")
plt.title("Histogram of Predicted Probabilities")
plt.show()
