import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal, invwishart, norm

# Define the parameters and priors
np.random.seed(42)
n = 1000
mu_true = np.array([0, 0])
rho = 0.8
sigma_true = np.array([[1, rho], [rho, 1]])

# Simulate data
data = np.random.multivariate_normal(mu_true, sigma_true, n)


def introduce_missingness(data, missing_rate=0.1):
    """add missing values, 0.1=10%"""
    data_missing = data.copy()
    n, p = data.shape
    for i in range(n):
        if np.random.rand() < missing_rate:
            j = np.random.choice(p)
            data_missing[i, j] = np.nan
    return data_missing


data_missing = introduce_missingness(data)

# Initial values
mu = np.mean(data, axis=0)
Sigma = np.cov(data.T)
Ymis = np.copy(data_missing)

# Initial priors
m0 = np.zeros(2)
M0 = np.eye(2)
nu0 = 3
S0 = np.eye(2)


# Gibbs Sampling
num_iterations = 1000
mu_samples = []
Sigma_samples = []

# helpers
observed_mask = ~np.isnan(data_missing)
y_bar = np.nanmean(data_missing, axis=0)
data_filled = np.where(
    np.isnan(data_missing), np.nanmean(data_missing, axis=0), data_missing
)
for iteration in range(num_iterations):

    # Update priors based on observed data p 186 in slides
    M_n = np.linalg.inv(np.linalg.inv(M0) + n * np.linalg.inv(S0))
    m_n = M_n @ (
        np.linalg.inv(M0) @ m0
        + n * np.linalg.inv(S0) @ np.nanmean(data_missing, axis=0)
    )
    mu = np.random.multivariate_normal(m_n, M_n)
    centered_data = data_filled - np.nanmean(data_missing, axis=0)
    center = centered_data.T @ centered_data
    nu_n = nu0 + n / 2
    S_n = S0 + 0.5 * center
    Sigma = invwishart.rvs(df=nu_n, scale=S_n)

    print(f"mu {mu}, sigma {Sigma}")

    # Sample Ymis from p(Ymis | mu, Sigma, Yobs)
    for i in range(n):
        if np.isnan(data_missing[i, 0]):
            cond_mu = mu[0] + (Sigma[0, 1] / Sigma[1, 1]) * (data_missing[i, 1] - mu[1])
            cond_sigma = Sigma[0, 0] - (
                Sigma[0, 1] / Sigma[1, 1] * Sigma[1, 1]
            )  # ?? from hat
            Ymis[i, 0] = np.random.normal(cond_mu, np.sqrt(cond_sigma))
        if np.isnan(data_missing[i, 1]):
            cond_mu = mu[1] + (Sigma[1, 0] / Sigma[0, 0]) * (data_missing[i, 0] - mu[0])
            cond_sigma = Sigma[1, 1] - (
                Sigma[1, 0] / Sigma[0, 0] * Sigma[0, 0]
            )  # ?? seems to work
            Ymis[i, 1] = np.random.normal(cond_mu, np.sqrt(cond_sigma))

    # Store samples
    mu_samples.append(mu)
    Sigma_samples.append(Sigma)

    # Update for the next iteration
    m0 = m_n
    M0 = M_n
    nu0 = nu_n
    S0 = S_n

# Convert lists to numpy arrays
mu_samples = np.array(mu_samples)
Sigma_samples = np.array(Sigma_samples)

# Impute missing data with the last iteration of Gibbs sampling
data_imputed = data_missing.copy()
data_imputed[np.isnan(data_imputed)] = Ymis[np.isnan(data_imputed)]

# Identify imputed data points, for plots
imputed_mask = np.isnan(data_missing)

# Plot the comparison of original data, data with missing values, and imputed data
plt.figure(figsize=(18, 6))

plt.subplot(1, 3, 1)
plt.scatter(data[:, 0], data[:, 1], alpha=0.5, label="Original Data", color="green")
plt.xlabel("y1")
plt.ylabel("y2")
plt.title("Original Data")
plt.legend()

plt.subplot(1, 3, 2)
plt.scatter(
    data_missing[:, 0],
    data_missing[:, 1],
    alpha=0.5,
    label="Missing Data",
    color="orange",
)
plt.xlabel("y1")
plt.ylabel("y2")
plt.title("Data with Missing Values")
plt.legend()

plt.subplot(1, 3, 3)
plt.scatter(data[:, 0], data[:, 1], alpha=0.2, label="Original Data", color="green")
plt.scatter(
    data_imputed[imputed_mask[:, 0], 0],
    data_imputed[imputed_mask[:, 0], 1],
    alpha=0.2,
    label="Imputed Data (y0 missing)",
    color="red",
)
plt.scatter(
    data_imputed[imputed_mask[:, 1], 0],
    data_imputed[imputed_mask[:, 1], 1],
    alpha=0.2,
    label="Imputed Data (y1 missing)",
    color="blue",
)
plt.xlabel("y1")
plt.ylabel("y2")
plt.title("Original and Imputed Data")
plt.legend()

plt.tight_layout()
plt.show()
