import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal, invgamma
import math


def introduce_missingness(data, missing_rate=0.1):
    """add missing values, 0.1=10%"""
    data_missing = data.copy()
    n, p = data.shape
    for i in range(n):
        if np.random.rand() < missing_rate:
            j = np.random.choice(p)
            data_missing[i, j] = np.nan
    return data_missing


def replace_missing_with_column_means(X):
    # Calculate column means, ignoring NaNs
    col_means = np.nanmean(X, axis=0)
    # Find indices where NaNs are present
    inds = np.where(np.isnan(X))
    # Replace NaNs with the corresponding column means
    X[inds] = np.take(col_means, inds[1])
    return X


# Step 1: Simulate data for the probit model
np.random.seed(0)
n = 1000
p = 2

# True beta coefficients
beta_true = np.array([0.5, -1.0])

# Simulate predictor variables
X = np.random.normal(size=(n, p))

# Simulate latent variables
y = X @ beta_true + np.random.normal(size=n)

X_missing = introduce_missingness(X)
nan_mask = np.isnan(X_missing)

X_replaced = replace_missing_with_column_means(X_missing)
# Step 2: Gibbs Sampler

# Prior parameters
nu0 = 1
sigma2 = 1

# Number of iterations
n_iter = 5000

# Initialize beta and Sigma
beta = np.linalg.inv(X_replaced.T @ X_replaced) @ (X_replaced.T @ y)  # OLS
print(f"beta ols: {beta}")
# beta = np.zeros(p)
Sigma = np.eye(p, p)

# Storage for samples
beta_samples = np.zeros((n_iter, p))
sigma2_samples = np.zeros(n_iter)

for i in range(n_iter):

    # Update beta
    inv_Sigma = np.linalg.inv(Sigma)
    Var_beta = np.linalg.inv(inv_Sigma + X_replaced.T @ X_replaced / sigma2)
    E_beta = Var_beta @ (inv_Sigma * beta + X_replaced.T @ y / sigma2)
    print(f"E_beta.shape {E_beta.shape} eb {E_beta}")
    beta = multivariate_normal.rvs(np.diag(E_beta), Var_beta)
    print(f"y {y.shape}")
    print(f"X {X.shape}")
    print(f"beta {beta.shape}")

    # Update sigma2
    SSR = np.sum((y - X_replaced @ beta) ** 2)
    alpha = (nu0 + n) / 2
    beta_param = (nu0 * sigma2 + SSR) / 2
    sigma2 = invgamma.rvs(alpha, 1 / beta_param)

    # update missing values in X
    # Calculate the pseudo-inverse of b
    beta_pseudo_inv = np.linalg.pinv(beta.reshape(p, 1))
    print(f"y {y.reshape(-1, 1).shape}")
    print(f"beta_pseudo_inv.T {beta_pseudo_inv.shape}")

    # Solve for X
    X_estimate = y.reshape(-1, 1) @ beta_pseudo_inv
    # update missing values
    X_replaced = np.where(nan_mask, X_estimate, X_missing)

    # Store samples
    beta_samples[i, :] = beta
    sigma2_samples[i] = sigma2

# Step 3: Analyze the results
beta_mean = beta_samples.mean(axis=0)
beta_std = beta_samples.std(axis=0)
sigma2_mean = sigma2_samples.mean()
sigma2_std = sigma2_samples.std()

print("Posterior mean of beta:", beta_mean)
print("Posterior std of beta:", beta_std)
print("Posterior mean of sigma2:", sigma2_mean)
print("Posterior std of sigma2:", sigma2_std)

plt.scatter(X[:, 0], X[:, 1], alpha=0.2, label="Original Data", color="green")
plt.scatter(
    X_replaced[nan_mask[:, 0], 0],
    X_replaced[nan_mask[:, 0], 1],
    alpha=0.2,
    label="Imputed Data (X1issing)",
    color="red",
)
plt.scatter(
    X_replaced[nan_mask[:, 1], 0],
    X_replaced[nan_mask[:, 1], 1],
    alpha=0.2,
    label="Imputed Data (X2 missing)",
    color="blue",
)
plt.xlabel("y1")
plt.ylabel("y2")
plt.title("Original and Imputed Data")
plt.legend()

plt.tight_layout()
plt.show()


# Plot the results for beta
plt.figure(figsize=(10, 6))
for j in range(p):
    plt.subplot(p, 1, j + 1)
    plt.plot(beta_samples[:, j])
    plt.title(f"Trace plot for beta_{j}")
plt.tight_layout()
plt.show()

plt.figure(figsize=(10, 6))
for j in range(p):
    plt.subplot(p, 1, j + 1)
    plt.hist(beta_samples[:, j], bins=30, density=True)
    plt.axvline(beta_true[j], color="r", linestyle="--")
    plt.title(f"Posterior distribution for beta_{j}")
plt.tight_layout()
plt.show()

# Plot the results for sigma2
plt.figure(figsize=(10, 6))
plt.plot(sigma2_samples)
plt.title("Trace plot for sigma2")
plt.show()

plt.figure(figsize=(10, 6))
plt.hist(sigma2_samples, bins=30, density=True)
plt.title("Posterior distribution for sigma2")
plt.show()
