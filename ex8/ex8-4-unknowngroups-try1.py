import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt


# Step 1: Generate data sets
def generate_mixture_data(n, pi, mu1, mu2, sigma2):
    component = np.random.binomial(1, pi, n)
    y = np.where(
        component == 1,
        np.random.normal(mu1, np.sqrt(sigma2), n),
        np.random.normal(mu2, np.sqrt(sigma2), n),
    )
    return y, component


n = 200
pi = 0.5
mu1 = 0
sigma2 = 1
mu2_values = [1, 3, 5]

data_sets = [generate_mixture_data(n, pi, mu1, mu2, sigma2) for mu2 in mu2_values]


# Step 2: Bayesian analysis with unknown group indicators
def gibbs_sampler_unknown_indicators(
    y, mu1_prior, mu2_prior, sigma2, e0_1, e0_2, n_iter=1000
):
    # Initialize variables
    rng = np.random.default_rng()
    n = len(y)
    m0_1, m0_2 = mu1_prior, mu2_prior
    M0_1, M0_2 = 1, 1  # Prior variances
    nu0_1, nu0_2 = 2, 2  # Degrees of freedom for Wishart distribution
    S0_1, S0_2 = 1, 1  # Scale matrices for Wishart distribution
    mu1_samples = []
    mu2_samples = []
    eta_samples = []
    component = rng.binomial(1, 0.5, n)  # Initial group indicators

    # Starting values
    Sigma1 = sigma2
    Sigma2 = sigma2
    mu1 = np.mean(y[component == 1]) if np.sum(component == 1) > 0 else np.mean(y)
    mu2 = np.mean(y[component == 0]) if np.sum(component == 0) > 0 else np.mean(y)
    eta = 0.5

    for _ in range(n_iter):
        # Sample group indicators
        p1 = eta * stats.norm.pdf(y, mu1, np.sqrt(Sigma1))
        p2 = (1 - eta) * stats.norm.pdf(y, mu2, np.sqrt(Sigma2))
        prob = p1 / (p1 + p2)
        component = rng.binomial(1, prob)

        n1 = np.sum(component == 1)
        n2 = np.sum(component == 0)

        # Sample eta
        eta = rng.beta(n1 + e0_1, n2 + e0_2)

        # Sample mu1
        y1 = y[component == 1]
        y1_mean = np.mean(y1) if n1 > 0 else mu1
        Mn_1 = 1 / (1 / M0_1 + n1 / Sigma1)
        mn_1 = Mn_1 * (m0_1 / M0_1 + n1 * y1_mean / Sigma1)
        mu1 = rng.normal(mn_1, np.sqrt(Mn_1))

        # Sample mu2
        y2 = y[component == 0]
        y2_mean = np.mean(y2) if n2 > 0 else mu2
        Mn_2 = 1 / (1 / M0_2 + n2 / Sigma2)
        mn_2 = Mn_2 * (m0_2 / M0_2 + n2 * y2_mean / Sigma2)
        mu2 = rng.normal(mn_2, np.sqrt(Mn_2))

        # Sample Sigma1
        Sn_1 = S0_1 + 0.5 * np.sum((y1 - mu1) ** 2) if n1 > 0 else S0_1
        Sigma1 = stats.invwishart.rvs(df=nu0_1 + n1 / 2, scale=Sn_1)

        # Sample Sigma2
        Sn_2 = S0_2 + 0.5 * np.sum((y2 - mu2) ** 2) if n2 > 0 else S0_2
        Sigma2 = stats.invwishart.rvs(df=nu0_2 + n2 / 2, scale=Sn_2)

        mu1_samples.append(mu1)
        mu2_samples.append(mu2)
        eta_samples.append(eta)

    return np.array(mu1_samples), np.array(mu2_samples), np.array(eta_samples)


# Parameters for the priors
mu1_prior = 0
mu2_prior = 0
e0_1, e0_2 = 1, 1
n_iter = 1000

# Run the Gibbs sampler for each dataset
for i, (y, _) in enumerate(data_sets):
    mu1_samples, mu2_samples, eta_samples = gibbs_sampler_unknown_indicators(
        y, mu1_prior, mu2_prior, sigma2, e0_1, e0_2, n_iter
    )

    # Plot the trace and posterior distribution of mu1, mu2, and eta
    fig, axes = plt.subplots(3, 2, figsize=(10, 12))

    axes[0, 0].plot(mu1_samples)
    axes[0, 0].set_title(f"Trace plot of mu1 (Dataset {i+1})")
    axes[0, 1].hist(mu1_samples, bins=30, density=True)
    axes[0, 1].set_title(f"Posterior distribution of mu1 (Dataset {i+1})")

    axes[1, 0].plot(mu2_samples)
    axes[1, 0].set_title(f"Trace plot of mu2 (Dataset {i+1})")
    axes[1, 1].hist(mu2_samples, bins=30, density=True)
    axes[1, 1].set_title(f"Posterior distribution of mu2 (Dataset {i+1})")

    axes[2, 0].plot(eta_samples)
    axes[2, 0].set_title(f"Trace plot of eta (Dataset {i+1})")
    axes[2, 1].hist(eta_samples, bins=30, density=True)
    axes[2, 1].set_title(f"Posterior distribution of eta (Dataset {i+1})")

    plt.tight_layout()
    plt.show()
