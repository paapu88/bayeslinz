import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt


# Step 1: Generate data sets
def generate_mixture_data(n, pi, mu1, mu2, sigma2):
    component = np.random.binomial(1, pi, n)
    y = np.where(
        component == 1,
        np.random.normal(mu1, np.sqrt(sigma2), n),
        np.random.normal(mu2, np.sqrt(sigma2), n),
    )
    return y, component


n = 200
pi = 0.5
mu1 = 0
sigma2 = 1
mu2_values = [1, 3, 5]

data_sets = [generate_mixture_data(n, pi, mu1, mu2, sigma2) for mu2 in mu2_values]


# Step 2: Bayesian analysis with observed group indicators
def gibbs_sampler_known_indicators(
    y, component, mu1_prior, mu2_prior, sigma2, n_iter=1000
):
    mu1_samples = []
    mu2_samples = []
    for _ in range(n_iter):
        y1 = y[component == 1]
        y2 = y[component == 0]

        # Sample mu1
        mu1_post_mean = (mu1_prior + np.sum(y1)) / (1 + len(y1))
        mu1_post_var = sigma2 / (1 + len(y1))
        mu1 = np.random.normal(mu1_post_mean, np.sqrt(mu1_post_var))

        # Sample mu2
        mu2_post_mean = (mu2_prior + np.sum(y2)) / (1 + len(y2))
        mu2_post_var = sigma2 / (1 + len(y2))
        mu2 = np.random.normal(mu2_post_mean, np.sqrt(mu2_post_var))

        mu1_samples.append(mu1)
        mu2_samples.append(mu2)

    return np.array(mu1_samples), np.array(mu2_samples)


mu1_prior = 0
mu2_prior = 0
n_iter = 1000

for i, (y, component) in enumerate(data_sets):
    mu1_samples, mu2_samples = gibbs_sampler_known_indicators(
        y, component, mu1_prior, mu2_prior, sigma2, n_iter
    )
    plt.figure(figsize=(10, 4))
    plt.title("KNOWN indicators")
    plt.subplot(1, 2, 1)
    plt.plot(mu1_samples)
    plt.title(f"Trace plot of mu1 (Dataset {i+1})")
    plt.subplot(1, 2, 2)
    plt.plot(mu2_samples)
    plt.title(f"Trace plot of mu2 (Dataset {i+1})")
    plt.tight_layout()
    plt.show()


# Step 3: Bayesian analysis with unknown group indicators
def gibbs_sampler_unknown_indicators(
    y, pi_prior, mu1_prior, mu2_prior, sigma2, n_iter=1000
):
    n = len(y)
    pi_samples = []
    mu1_samples = []
    mu2_samples = []
    component_samples = []

    # Initialize parameters
    pi = 0.5
    mu1 = np.random.normal()
    mu2 = np.random.normal()
    component = np.random.binomial(1, pi, n)

    for _ in range(n_iter):
        # Sample component indicators
        p1 = pi * stats.norm.pdf(y, mu1, np.sqrt(sigma2))
        p2 = (1 - pi) * stats.norm.pdf(y, mu2, np.sqrt(sigma2))
        p = p1 / (p1 + p2)
        component = np.random.binomial(1, p)

        # Sample pi
        alpha_post = pi_prior + np.sum(component)
        beta_post = pi_prior + n - np.sum(component)
        pi = np.random.beta(alpha_post, beta_post)

        # Sample mu1
        y1 = y[component == 1]
        mu1_post_mean = (mu1_prior + np.sum(y1)) / (1 + len(y1))
        mu1_post_var = sigma2 / (1 + len(y1))
        mu1 = np.random.normal(mu1_post_mean, np.sqrt(mu1_post_var))

        # Sample mu2
        y2 = y[component == 0]
        mu2_post_mean = (mu2_prior + np.sum(y2)) / (1 + len(y2))
        mu2_post_var = sigma2 / (1 + len(y2))
        mu2 = np.random.normal(mu2_post_mean, np.sqrt(mu2_post_var))

        pi_samples.append(pi)
        mu1_samples.append(mu1)
        mu2_samples.append(mu2)
        component_samples.append(component)

    return (
        np.array(pi_samples),
        np.array(mu1_samples),
        np.array(mu2_samples),
        np.array(component_samples),
    )


pi_prior = 1

for i, (y, _) in enumerate(data_sets):
    pi_samples, mu1_samples, mu2_samples, component_samples = (
        gibbs_sampler_unknown_indicators(
            y, pi_prior, mu1_prior, mu2_prior, sigma2, n_iter
        )
    )
    plt.figure(figsize=(15, 4))
    plt.title("UNKNOWN indicators")

    plt.subplot(1, 3, 1)
    plt.plot(pi_samples)
    plt.title(f"Trace plot of pi (Dataset {i+1})")
    plt.subplot(1, 3, 2)
    plt.plot(mu1_samples)
    plt.title(f"Trace plot of mu1 (Dataset {i+1})")
    plt.subplot(1, 3, 3)
    plt.plot(mu2_samples)
    plt.title(f"Trace plot of mu2 (Dataset {i+1})")
    plt.tight_layout()
    plt.show()
