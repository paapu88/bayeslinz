import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import multivariate_normal, invgamma
import pandas as pd
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri

# get data to X and y
pandas2ri.activate()
MASS = importr("MASS")
Pima_tr = robjects.r["Pima.tr"]
data = pandas2ri.rpy2py(Pima_tr)
print(data.head())
print(data.describe())
headers = ["constant"] + list(data.columns)
X = data.iloc[:, :-1].values
n = X.shape[0]
# ones for constant coefficiennt beta0
X = np.hstack([np.ones((n, 1)), X])
y = data.iloc[:, -1].values - 1
p = X.shape[1]


def sample_beta(X, z, beta_prior_mean, beta_prior_cov):
    """
    Sample beta from the posterior distribution.
    https://bookdown.org/aramir21/IntroductionBayesianEconometricsGuidedTour/probit-model.html
    Chap 6.3
    and page 77 in Helga notes
    """
    beta_post_cov = np.linalg.inv(np.linalg.inv(beta_prior_cov) + X.T @ X)

    beta_post_mean = beta_post_cov @ (
        np.linalg.inv(beta_prior_cov) @ beta_prior_mean + X.T @ z
    )

    return np.random.multivariate_normal(beta_post_mean, beta_post_cov)


def sample_z(X, y, beta):
    """
    Sample z from the truncated normal distribution.
    """
    mean = X @ beta
    z = np.zeros_like(mean)
    for i in range(len(y)):
        if y[i] == 1:
            z[i] = stats.truncnorm.rvs(
                a=(0 - mean[i]) / 1, b=np.inf, loc=mean[i], scale=1
            )
        else:
            z[i] = stats.truncnorm.rvs(
                a=-np.inf, b=(0 - mean[i]) / 1, loc=mean[i], scale=1
            )
    return z


def impute_missing_values(X, beta):
    """
    Impute missing values in X using the conditional distribution.
    """
    X_imputed = X.copy()
    for i in range(X.shape[0]):
        for j in range(
            1, X.shape[1]
        ):  # Start from 1 to avoid changing the intercept term
            if np.isnan(X[i, j]):
                # Impute missing value based on the observed part of X and the current beta
                observed_indices = ~np.isnan(X[i, :])
                mean = X[i, observed_indices] @ beta[observed_indices]
                X_imputed[i, j] = np.random.normal(mean, 1)
    return X_imputed


# Initialize parameters
n_iter = 1000
burn = 500
beta_prior_mean = np.zeros(X.shape[1])
beta_prior_cov = np.eye(X.shape[1])
beta_samples = np.zeros((n_iter, X.shape[1]))
z_samples = np.zeros((n_iter, n))

# Initial values
beta = np.zeros(X.shape[1])
z = np.random.randn(n)

# Gibbs sampling
for i in range(n_iter):
    z = sample_z(X, y, beta)
    beta = sample_beta(X, z, beta_prior_mean, beta_prior_cov)
    beta_samples[i, :] = beta
    print(f"i {i} beta {beta}")
    z_samples[i, :] = z

# Posterior mean of beta
beta_mean = beta_samples[burn:, :].mean(axis=0)
print("Posterior mean of beta:", beta_mean)

# Plot the trace of beta
fig, ax = plt.subplots(figsize=(10, 6))
for j in range(beta_samples.shape[1]):
    ax.plot(beta_samples[:, j], label=f"beta_{j}")
ax.set_title("Trace plot of beta")
ax.legend()
plt.tight_layout()
plt.show()

# Plot the posterior distribution of beta
fig, ax = plt.subplots(figsize=(10, 6))
for j in range(beta_samples.shape[1]):
    print(f"j {j} mean(beta_j) {beta_mean[j]}")
    ax.hist(beta_samples[burn:, j], bins=30, alpha=0.5, density=True, label=f"beta_{j}")
ax.set_title("Posterior distribution of beta")
ax.legend()
plt.tight_layout()
plt.show()
