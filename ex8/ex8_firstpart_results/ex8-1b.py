import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal, invwishart, norm

# Step 1: Generate the Simulated Data
np.random.seed(0)  # For reproducibility

# Parameters for the bivariate normal distribution
mu_true = np.array([0, 0])
rho = 0.8
sigma_true = np.array([[1, rho], [rho, 1]])

# Simulate data
n = 1000
data = np.random.multivariate_normal(mu_true, sigma_true, n)


# Introduce missingness
def introduce_missingness(data, missing_rate=0.1):
    data_missing = data.copy()
    n, p = data.shape
    for i in range(n):
        if np.random.rand() < missing_rate:
            j = np.random.choice(p)
            data_missing[i, j] = np.nan
    return data_missing


data_missing = introduce_missingness(data)

# Step 2: Set Up the Priors
m0 = np.zeros(2)
M0 = np.eye(2)
nu0 = 3
S0 = np.eye(2)


# Step 3: Implement the Gibbs Sampling Scheme
def gibbs_sampling(data_missing, m0, M0, nu0, S0, n_iter=1000):
    n, p = data_missing.shape

    # Initialize missing data with the observed data means
    data_filled = np.where(
        np.isnan(data_missing), np.nanmean(data_missing, axis=0), data_missing
    )

    mu_samples = []
    sigma_samples = []

    for _ in range(n_iter):
        # Sample mu  p 186 in slides
        M_n = np.linalg.inv(np.linalg.inv(M0) + n * np.linalg.inv(S0))
        m_n = M_n @ (
            np.linalg.inv(M0) @ m0
            + n * np.linalg.inv(S0) @ np.nanmean(data_filled, axis=0)
        )
        mu = np.random.multivariate_normal(m_n, M_n)
        mu_samples.append(mu)

        # Sample Sigma  p 186 in slides
        centered_data = data_filled - np.nanmean(data_filled, axis=0)
        S_n = centered_data.T @ centered_data
        nu_n = nu0 + n / 2
        S_n = S0 + 0.5 * S_n
        sigma = invwishart.rvs(df=nu_n, scale=S_n)
        sigma_samples.append(sigma)

        # Sample missing data
        for i in range(n):
            for j in range(p):
                if np.isnan(data_missing[i, j]):
                    if np.isnan(data_missing[i, 0]):
                        cond_mu = mu[0] + (sigma[0, 1] / sigma[1, 1]) * (
                            data_missing[i, 1] - mu[1]
                        )
                        cond_sigma = sigma[0, 0] - (
                            sigma[0, 1] / sigma[1, 1] * sigma[1, 1]
                        )
                        data_filled[i, 0] = np.random.normal(
                            cond_mu, np.sqrt(cond_sigma)
                        )
                    if np.isnan(data_missing[i, 1]):
                        cond_mu = mu[1] + (sigma[1, 0] / sigma[0, 0]) * (
                            data_missing[i, 0] - mu[0]
                        )
                        cond_sigma = sigma[1, 1] - (
                            sigma[1, 0] / sigma[0, 0] * sigma[0, 0]
                        )
                        data_filled[i, 1] = np.random.normal(
                            cond_mu, np.sqrt(cond_sigma)
                        )

    return np.array(mu_samples), np.array(sigma_samples), data_filled


# Run the Gibbs Sampler
mu_samples, sigma_samples, data_imputed = gibbs_sampling(
    data_missing, m0, M0, nu0, S0, n_iter=1000
)
# Identify imputed data points
imputed_mask = np.isnan(data_missing)

# Step 4: Plot the Results
plt.figure(figsize=(18, 5))

# Original Data
plt.subplot(1, 3, 1)
plt.scatter(data[:, 0], data[:, 1], alpha=0.5, label="Original Data", color="green")
plt.title("Original Data")
plt.xlabel("Y0")
plt.ylabel("Y1")
plt.legend()

# Data with Missing Values
plt.subplot(1, 3, 2)
plt.scatter(
    data_missing[:, 0], data_missing[:, 1], alpha=0.5, label="Missing Data", color="red"
)
plt.title("Data with Missing Values")
plt.xlabel("Y0")
plt.ylabel("Y1")
plt.legend()

plt.subplot(1, 3, 3)
plt.scatter(data[:, 0], data[:, 1], alpha=0.2, label="Original Data", color="green")
plt.scatter(
    data_imputed[imputed_mask[:, 0], 0],
    data_imputed[imputed_mask[:, 0], 1],
    alpha=0.2,
    label="Imputed Data (y0 missing)",
    color="red",
)
plt.scatter(
    data_imputed[imputed_mask[:, 1], 0],
    data_imputed[imputed_mask[:, 1], 1],
    alpha=0.2,
    label="Imputed Data (y1 missing)",
    color="blue",
)
plt.xlabel("y1")
plt.ylabel("y2")
plt.title("Original and Imputed Data")
plt.legend()

plt.tight_layout()
plt.show()
