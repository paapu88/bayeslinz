import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import invgamma, norm, t


def sample_t_distribution(df, size=1000):
    W = invgamma.rvs(df / 2, scale=df / 2, size=size)
    Y = norm.rvs(scale=np.sqrt(W))
    return Y


df = 5
size = 1000

# Sample  t-distribution
samples = sample_t_distribution(df, size)
plt.hist(
    samples, bins=30, density=True, alpha=0.6, color="g", label="Sampled Data (N, G-1)"
)

# true t-dist
x = np.linspace(min(samples), max(samples), 100)
plt.plot(x, t.pdf(x, df), "r-", lw=2, label="True t-dist (df=5)")

plt.title("t-distribution using N, G-1")
plt.xlabel("Value")
plt.ylabel("Density")
plt.legend()
plt.show()
