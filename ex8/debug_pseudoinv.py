import numpy as np

# Assuming y is (1000, 1)
y = np.random.rand(1000, 1)

# Assuming b is (3, 1)
b = np.random.rand(3, 1)

# Compute the pseudo-inverse of b
b_pseudo_inv = np.linalg.pinv(b)

# Ensure y is shaped (1000, 1)
if y.ndim == 1:
    y = y.reshape(-1, 1)

print
# Compute the estimated X
X_estimate = y @ b_pseudo_inv

print(X_estimate)
