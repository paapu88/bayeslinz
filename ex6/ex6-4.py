import numpy as np
import pandas as pd
from scipy.stats import invgamma, multivariate_normal
from pathlib import Path
import matplotlib.pyplot as plt
from numpy.linalg import inv


def calculate_ols_estimate(X, y):
    """Calculate the Ordinary Least Squares estimate of regression coefficients."""
    beta_ols = np.linalg.inv(X.T @ X) @ (X.T @ y)
    return beta_ols


def initial_sigma2(X, y):
    """Calculate the initial sigma squared as the variance of OLS residuals."""
    # OLS solution for beta
    beta_hat = inv(X.T @ X) @ (X.T @ y)
    # Calculate residuals
    residuals = y - X @ beta_hat
    # Variance of residuals
    sigma2_initial = np.var(residuals, ddof=1)  # using n-1 degrees of freedom
    return sigma2_initial


def gibbs_sampler(X, y, iterations, b0, B0, s0, S0, sigma2_initial):
    n, p = X.shape

    # Initial values for parameters
    sigma2 = sigma2_initial  # Initialize sigma2 to the variance of y

    # Storage for samples
    beta_samples = np.zeros((iterations, p))
    sigma2_samples = np.zeros(iterations)

    # Gibbs sampling iterations
    for i in range(iterations):
        # Update beta given sigma2 and y, page 119
        B0_inv = np.linalg.inv(B0)
        Bn = np.linalg.inv(X.T @ X / sigma2 + B0_inv)
        bn = Bn @ (X.T @ y / sigma2 + B0_inv @ b0)
        beta = multivariate_normal.rvs(mean=bn, cov=Bn)

        # Update sigma2 given beta and y
        residuals = y - X @ beta
        Sn = S0 + 0.5 * residuals.T @ residuals
        sn = s0 + n / 2
        sigma2 = invgamma.rvs(a=sn, scale=Sn)

        # Store samples
        beta_samples[i, :] = beta
        sigma2_samples[i] = sigma2

    return beta_samples, sigma2_samples


# Load data
data = pd.read_csv(
    Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/ex6/oxygen_mok.csv")
)

# Assuming data columns are appropriately named
data["xi1"] = 1  # Intercept
data["xi2"] = (data["program"] == 1).astype(int)  # 1 if aerobic, 0 otherwise
data["xi3"] = data["age"]  # Age of the subject
data["xi4"] = data["xi2"] * data["xi3"]  # Interaction term

# Define the model matrix X and response vector y
X = data[["xi1", "xi2", "xi3", "xi4"]].values
y = data["change"].values

# Prior parameters
# Define prior parameters
b0 = calculate_ols_estimate(X, y)
B0 = 1000 * np.eye(4)
s0 = 1
S0 = 12
sigma2_initial = initial_sigma2(X, y)
print("Initial sigma^2 from OLS residuals:", sigma2_initial)

# Assuming 'X' and 'y' are already defined and loaded as shown previously
print("Initial beta estimates from OLS:", b0)

# Run Gibbs sampling
beta_samples, sigma2_samples = gibbs_sampler(
    X, y, 10000, b0=b0, B0=B0, s0=s0, S0=S0, sigma2_initial=sigma2_initial
)

# Display results
print("Sampled beta values (first 5):", beta_samples[:5, :])
print("Sampled sigma2 values (first 5):", sigma2_samples[:5])

# Assuming `beta_samples` holds the sampled beta values from the Gibbs sampling
# Let's calculate the means of each beta parameter from the last 500 samples

last_500_beta_samples = beta_samples[-500:]  # extract the last 500 samples

# Calculate the expected values (means) for each beta coefficient
expected_betas = np.mean(last_500_beta_samples, axis=0)

# Print the expected values
for i, beta_mean in enumerate(expected_betas, start=1):
    print(f"Expected value of beta_{i}: {beta_mean:.4f}")


# Number of parameters in beta (includes intercept and other features)
num_beta_params = beta_samples.shape[1]

plt.figure(figsize=(10, 8))

# Plotting histograms for each beta parameter
for i in range(num_beta_params):
    plt.subplot(
        2, 3, i + 1
    )  # Adjust the grid size based on the number of beta parameters
    plt.hist(beta_samples[500:, i], bins=50, color="blue", alpha=0.7)
    plt.title(f"Posterior Distribution of $\\beta_{i+1}$")
    plt.xlabel(f"$\\beta_{i+1}$")
    plt.ylabel("Frequency")

# Plot for sigma^2
plt.subplot(2, 3, 6)  # Position sigma^2 in the last subplot
plt.hist(sigma2_samples[500:], bins=50, color="green", alpha=0.7)
plt.title("Posterior Distribution of $\\sigma^2$")
plt.xlabel("$\\sigma^2$")
plt.ylabel("Frequency")

plt.tight_layout()
plt.show()

# results are close to results of 3)
