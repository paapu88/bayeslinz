import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import beta
from scipy.integrate import quad

# Parameters
pi = 0.0009
n_max = 10**8
n_values = [10**exp for exp in range(1, 9)]

# Generate the sample
np.random.seed(0)  # for reproducibility
sample = np.random.binomial(1, pi, n_max)

# Define the theta values
theta_values_a = np.arange(0.0005, 0.0016, 0.0001)


def compute_posterior(sample, n, theta_values):
    """not used, caused overflow"""
    y_sum = np.sum(sample[:n])
    likelihood = theta_values**y_sum * (1 - theta_values) ** (n - y_sum)
    prior = np.ones_like(theta_values) / len(theta_values)
    posterior = likelihood * prior
    posterior /= np.sum(posterior)
    return posterior


def compute_posterior_with_logs(sample, n, theta_values):
    y_sum = np.sum(sample[:n])
    log_likelihood = y_sum * np.log(theta_values) + (n - y_sum) * np.log(
        1 - theta_values
    )
    log_prior = np.log(np.ones_like(theta_values) / len(theta_values))  # Uniform prior
    log_posterior = log_likelihood + log_prior
    # Normalize log_posterior by subtracting the logsumexp to prevent underflow
    max_log_posterior = np.max(log_posterior)
    log_posterior -= (
        np.log(np.sum(np.exp(log_posterior - max_log_posterior))) + max_log_posterior
    )
    return np.exp(log_posterior)


# Calculate posterior for different n values
posteriors_a = {
    n: compute_posterior_with_logs(sample, n, theta_values_a) for n in n_values
}

# Plotting all posteriors
plt.figure(figsize=(12, 8))
for n in n_values:
    plt.plot(theta_values_a, posteriors_a[n], label=f"n={n}")

plt.title("a) Posterior Distributions for Different Sample Sizes")
plt.xlabel("Theta")
plt.ylabel("Posterior Probability")
plt.legend()
plt.grid(True)
plt.show()

# b)
theta_values_b = np.array([0.0005, 0.00075, 0.001, 0.00125, 0.0015])
posteriors_b = {
    n: compute_posterior_with_logs(sample, n, theta_values_b) for n in n_values
}

for n in n_values:
    plt.plot(theta_values_b, posteriors_b[n], label=f"n={n}")

plt.title("b) Posterior Distributions for Different Sample Sizes")
plt.xlabel("Theta")
plt.ylabel("Posterior Probability")
plt.legend()
plt.grid(True)
plt.show()


# c) page 32 in slides
def beta_posterior_c(sample, n):
    y_sum = np.sum(sample[:n])
    return beta(y_sum + 1, n - y_sum + 1)


beta_posteriors_c = {n: beta_posterior_c(sample, n) for n in n_values}

x = np.linspace(0, 0.002, 1000)

for n in n_values:
    plt.plot(x, beta_posteriors_c[n].pdf(x), label=f"n={n}")

plt.title(
    "c) Posterior Distributions for different sample size n, with Beta(1,1) prior"
)
plt.xlabel("Theta")
plt.ylabel("Posterior Probability")
plt.legend()
plt.grid(True)
plt.show()


# d) page 32 in slides
def beta_posterior_d(sample, n):
    y_sum = np.sum(sample[:n])
    return beta(y_sum + 0.01, n - y_sum + 1)


beta_posteriors_d = {n: beta_posterior_d(sample, n) for n in n_values}

for n in n_values:
    plt.plot(x, beta_posteriors_d[n].pdf(x), label=f"n={n}")

plt.title(
    "d) Posterior Distributions for different sample size n, with Beta(0.01,1) prior"
)
plt.xlabel("Theta")
plt.ylabel("Posterior Probability")
plt.legend()
plt.grid(True)
plt.show()


# Integrate the PDF of the beta distribution from 0 to 1
area, _ = quad(beta_posteriors_d[100000].pdf, 0, 1)
print("The area under the PDF (total probability):", area)
