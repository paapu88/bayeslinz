import pandas as pd
import numpy as np
from pathlib import Path
from scipy.stats import invgamma, multivariate_normal
import matplotlib.pyplot as plt

# Load data
data = pd.read_csv(
    Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/ex6/oxygen_mok.csv")
)

# Assuming data columns are appropriately named
data["xi1"] = 1  # Intercept
data["xi2"] = (data["program"] == 1).astype(int)  # 1 if aerobic, 0 otherwise
data["xi3"] = data["age"]  # Age of the subject
data["xi4"] = data["xi2"] * data["xi3"]  # Interaction term

# Define the model matrix X and response vector y
X = data[["xi1", "xi2", "xi3", "xi4"]].values
y = data["change"].values

print(data)

# Dimensions of X
n, p = X.shape

# Prior parameters
b0 = np.zeros(p)
B0 = 1000 * np.eye(p)  # B0 is 1000 times the identity matrix
s0 = 1
S0 = 12

# Initial guesses for mu and sigma^2
mu_initial = np.zeros(p)

# Inverting M0 for computational stability
B0_inv = np.linalg.inv(B0)

# Updating rules, page 78
B_n_inv = X.T @ X + B0_inv
B_n = np.linalg.inv(B_n_inv)
b_n = B_n @ (X.T @ y + B0_inv @ b0)

# Sum of squared residuals for sigma^2 posterior
S_y = y.T @ y + b0.T @ B0_inv @ b0 - b_n.T @ B_n_inv @ b_n

# Posterior parameters for sigma^2, page 119
s_n = s0 + n / 2
S_n = S0 + 0.5 * S_y

print(f"Posterior mean of beta: {b_n}")
print(f"Posterior covariance matrix of beta: {B_n}")
print(f"Posterior sn for sigma^2: {s_n}")
print(f"Posterior Sn for sigma^2: {S_n}")


# Monte Carlo Sampling
num_samples = 10000
sigma2_samples = invgamma.rvs(a=s_n, scale=S_n, size=num_samples)
beta_samples = np.array(
    [multivariate_normal.rvs(mean=b_n, cov=sigma2 * B_n) for sigma2 in sigma2_samples]
)

print("Sampled beta values (first 5):", beta_samples[:5])


# Number of parameters in beta (includes intercept and other features)
num_beta_params = beta_samples.shape[1]

plt.figure(figsize=(12, 8))

# Plotting histograms for each beta parameter
for i in range(num_beta_params):
    plt.subplot(
        2, 2, i + 1
    )  # Adjust the grid size based on the number of beta parameters
    plt.hist(beta_samples[:, i], bins=50, color="blue", alpha=0.7)
    plt.title(f"Posterior Distribution of $\\beta_{i+1}$")
    plt.xlabel(f"$\\beta_{i+1}$")
    plt.ylabel("Frequency")

# Plot for sigma^2
plt.figure(figsize=(6, 4))
plt.hist(sigma2_samples, bins=50, color="green", alpha=0.7)
plt.title("Posterior Distribution of $\\sigma^2$")
plt.xlabel("$\\sigma^2$")
plt.ylabel("Frequency")

plt.tight_layout()
plt.show()

# look ok in comparison (scalars near peak values)
