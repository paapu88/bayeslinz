import numpy as np
from scipy.stats import multivariate_normal, invwishart

# Number of samples
n = 1000

# Dimensions of the multivariate normal
p = 3

# Synthetic Data Generation
np.random.seed(42)
true_mu = np.array([1.0, 2.0, 3.0])
true_sigma = np.array([[1.0, 0.5, 0.3], [0.5, 1.5, 0.4], [0.3, 0.4, 1.0]])
data = multivariate_normal.rvs(mean=true_mu, cov=true_sigma, size=n)
print(data.shape)
# Prior parameters for mu (mean vector)
m0 = np.zeros(p)
M0 = np.eye(p)

# Prior parameters for Sigma (covariance matrix)
v0 = p + 2  # degrees of freedom
S0 = np.eye(p)


def make_positive_definite(matrix):
    """Attempt to make a matrix positive definite by adjusting eigenvalues"""
    eigenvalues, eigenvectors = np.linalg.eigh(matrix)
    eigenvalues[eigenvalues < 0] = 0.0001  # Replace negative eigenvalues
    return eigenvectors @ np.diag(eigenvalues) @ eigenvectors.T


def gibbs_sampling(data, m0, M0, v0, S0, iters=1000):
    n, p = data.shape

    # Initial values
    mu = np.mean(data, axis=0)
    Sigma = np.cov(data, rowvar=False)

    print(Sigma)
    # sys.exit()

    # Store samples
    samples_mu = np.zeros((iters, p))
    samples_Sigma = np.zeros((iters, p, p))

    # Gibbs sampling iterations
    for i in range(iters):
        
        mu = multivariate_normal.rvs(mean=mu, cov=Sigma)
        Sigma = invwishart.rvs(df=v, scale=S_n)
        except:
            print(f"problem...:")
            print(f"SN {S_n}")

            S_n = make_positive_definite(S_n)
            Sigma = invwishart.rvs(df=v_n, scale=S_n)
        # Store samples
        samples_mu[i] = mu
        samples_Sigma[i] = Sigma

    return samples_mu, samples_Sigma


# Run Gibbs sampling
samples_mu, samples_Sigma = gibbs_sampling(data, m0, M0, v0, S0, iters=1000)

# Example outputs
print("Sampled means (last iteration):", samples_mu[-1])
print("Sampled covariance matrices (last iteration):", samples_Sigma[-1])
