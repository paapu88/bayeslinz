import pyreadr
import pandas as pd
from statsmodels.stats.proportion import proportion_confint

# Load the RData file
result = pyreadr.read_r("children.Rdata")

# `result` is a dictionary where keys are the names of the objects in the RData file
# and values are pandas DataFrames
dfs = []
for name, df in result.items():
    print(f"Dataset name: {name}")
    # print(df.head())  # Print the first few rows of each dataset

df = result["children"]

print(df.head(20))
print(df.describe())

""" 
Output:
                y     exposed
count  192.000000   192.00000
mean     1.838542  8856.30315
std      1.298398   293.09500
min      0.000000  8171.40000
25%      1.000000  8678.02080
50%      2.000000  8900.03610
75%      3.000000  9103.20000
max      5.000000  9203.70000

CAN EXPOSED CHILDREN BE A FLOAT????
"""
print(f"total number of deaths: {df['y'].sum()}")

# monthly data
start_date = "1987-01"
periods = 192
freq = "ME"  # Monthly frequency

date_index = pd.date_range(start=start_date, periods=periods, freq=freq)
df = pd.DataFrame(df.values, columns=["y", "exposed"], index=date_index)

df["Year"] = df.index.year
df["Month"] = df.index.month_name().str[:3]

# Initialize an empty DataFrame to hold the reshaped data
reshaped_df = pd.DataFrame()

# Loop through each variable and reshape data
for var in ["y", "exposed"]:
    # Pivot each variable separately
    pivot_var = df.pivot_table(index="Year", columns="Month", values=var)

    # Rename columns to include the variable name
    pivot_var.columns = [f"{var}_{col}" for col in pivot_var.columns]

    # If reshaped_df is empty, initialize it with pivot_var
    if reshaped_df.empty:
        reshaped_df = pivot_var
    else:
        # If reshaped_df is not empty, merge the new pivoted data
        reshaped_df = pd.merge(
            reshaped_df, pivot_var, left_index=True, right_index=True
        )

print(reshaped_df.head())
pd.set_option("display.max_columns", None)  # None means no limit

print(reshaped_df.describe())
""" 
monthly stats:
           y_Apr      y_Aug      y_Dec      y_Feb      y_Jan      y_Jul  \
count  16.000000  16.000000  16.000000  16.000000  16.000000  16.000000   
mean    2.437500   0.875000   2.062500   1.250000   2.312500   1.000000   
std     1.152895   0.718795   1.181454   1.064581   1.078193   0.966092   
min     1.000000   0.000000   0.000000   0.000000   1.000000   0.000000   
25%     2.000000   0.000000   1.000000   0.750000   2.000000   0.000000   
50%     2.000000   1.000000   2.000000   1.000000   2.000000   1.000000   
75%     3.000000   1.000000   3.000000   2.000000   3.000000   2.000000   
max     5.000000   2.000000   4.000000   3.000000   5.000000   3.000000   

           y_Jun     y_Mar      y_May      y_Nov      y_Oct      y_Sep  \
count  16.000000  16.00000  16.000000  16.000000  16.000000  16.000000   
mean    2.625000   1.93750   1.375000   1.812500   2.625000   1.750000   
std     1.668333   1.12361   1.408309   1.327592   1.204159   1.183216   
min     0.000000   0.00000   0.000000   0.000000   1.000000   0.000000   
25%     1.000000   1.00000   0.000000   1.000000   2.000000   1.000000   
50%     3.000000   2.00000   1.000000   2.000000   2.500000   2.000000   
75%     4.000000   3.00000   3.000000   2.250000   3.250000   2.000000   
max     5.000000   4.00000   4.000000   5.000000   5.000000   4.000000   


"""

# point estimate and 95% confidence interval
# Example data: total number of child deaths observed and total observations
total_deaths = 353
total_observations = 192

# Calculate the 95% confidence interval for the proportion of child deaths
# Using the Wilson score method
ci_lower, ci_upper = proportion_confint(
    count=total_deaths, nobs=total_observations, alpha=0.05, method="wilson"
)

# The point estimate is simply the observed proportion
point_estimate = total_deaths / total_observations

# Print the point estimate and the confidence interval
print(f"Point Estimate of Child Mortality Rate: {point_estimate:.4f}")
print(f"95% Confidence Interval (Wilson score): [{ci_lower:.4f}, {ci_upper:.4f}]")
"""
Point Estimate of Child Mortality Rate: 1.8385
95% Confidence Interval (Wilson score): [nan, nan]
(success rate cannot be more than 100% in binomial)
"""

# lets estimate fraction, that should work
total_deaths = 353
total_observations = 192 * 8856.30315
# Calculate the 95% confidence interval for the proportion of child deaths
# Using the Wilson score method
ci_lower, ci_upper = proportion_confint(
    count=total_deaths, nobs=total_observations, alpha=0.05, method="wilson"
)

# The point estimate is simply the observed proportion
point_estimate = total_deaths / total_observations

# Print the point estimate and the confidence interval
print(f"Point Estimate of Child Mortality Rate: {point_estimate:.4f}")
print(f"95% Confidence Interval (Wilson score): [{ci_lower:.4f}, {ci_upper:.4f}]")
""" 
Point Estimate of Child Mortality Rate: 0.0002
95% Confidence Interval (Wilson score): [0.0002, 0.0002]

Not very illumanitive either, probably one should use Bayesian statistics...

"""
