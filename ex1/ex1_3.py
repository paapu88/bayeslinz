from statsmodels.stats.proportion import proportion_confint, proportions_ztest

# Given data
failures_a, total_a = 40, 1000
failures_b, total_b = 0, 50

# Point estimates
pA = failures_a / total_a
pB = failures_b / total_b

# Confidence intervals
ci_clopper_pearson_a = proportion_confint(failures_a, total_a, method="beta")
ci_approx_a = proportion_confint(failures_a, total_a, method="normal")

ci_clopper_pearson_b = proportion_confint(failures_b, total_b, method="beta")
ci_approx_b = proportion_confint(failures_b, total_b, method="normal")

# Output
print(
    f"Case A - Point Estimate: {pA}, Approximate CI: {ci_approx_a}, Clopper-Pearson CI: {ci_clopper_pearson_a}"
)
print(
    f"Case B - Point Estimate: {pB}, Approximate CI: {ci_approx_b}, Clopper-Pearson CI: {ci_clopper_pearson_b}"
)

# Perform the Z-test for the difference in proportions
stat, pval = proportions_ztest([failures_a, failures_b], [total_a, total_b])

# Output the results
print(f"Z statistic: {stat}, P-value: {pval}")


# output
# Case A - Point Estimate: 0.04, Approximate CI: (0.027854547405936063, 0.05214545259406394),
# Clopper-Pearson CI: (0.0287276252858908, 0.054072696819368486)

# Case B - Point Estimate: 0.0, Approximate CI: (0.0, 0.0),
# Clopper-Pearson CI: (0.0, 0.07112173646419766)

# Z statistic: 1.4419458799802714, P-value: 0.14931764177529055
# So we cannot reject null hypothesis that the risks are equal (P value >> 0.05)
