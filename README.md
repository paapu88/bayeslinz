# to run coursework

- requirements 

```
pip install numpy pandas scipy scikit-learn seaborn imblearn statsmodels
```

- to run 12 variable training and prediction (with random 90%/10% split)

```
cd coursework
python mh_try2.py
```

- to run 42 variable version, in the end of mh_try2.py, uncomment the latest call to main and comment the second last one, then:


```
cd coursework
python mh_try2.py
```

- result for 12 variable dataset is ok, for the bigger one rather bad.
Probably inputation problem when 40% of data is missing.

for 12 variables:

    #F1 Score: 0.34
    #Confusion Matrix:
    #[[557 114]
    #[ 52  42]]
    #ROC AUC Score: 0.64

for 42 variables:
    #F1 Score: 0.21
    #Confusion Matrix:
    #[[ 41 631]
    #[  8  85]]
    #ROC AUC Score: 0.48

# median imputation version
```
mh.py
# comment in the end to run 12 or 42 variable version
```

for 12 variables:

    #F1 Score: 0.28
    
    #ROC AUC Score: 0.59
    #Classification Report:
    #            precision    recall  f1-score   support
    #
    #        0       0.90      0.86      0.88      1339
    #        1       0.25      0.31      0.28       191
    #    accuracy                           0.80      1530
    #macro avg       0.57      0.59      0.58      1530
    #weighted avg       0.82      0.80      0.81      1530
    #Confusion Matrix:
    #[[1157  182]
    #[ 131   60]]

for 42 variables:

    #F1 Score: 0.28
    #ROC AUC Score: 0.61
    #Classification Report:
    #            precision    recall  f1-score   support
    #        0       0.92      0.61      0.73      1339
    #        1       0.18      0.62      0.28       191
    #    accuracy                           0.61      1530
    #macro avg       0.55      0.61      0.51      1530
    #weighted avg       0.83      0.61      0.68      1530
    #Confusion Matrix:
    #[[818 521]
    #[ 73 118]]

# X inputation (no info from y)

```
python mh_try2inputation_onlyX.py
python report_from_confusionmatrix.py
```

(bayes) /home/mka/venvs/bayes/bin/python /home/mka/git/Omat/bayeslinz/coursework/report_from_confusionmatrix.py
12 variables, bayesian inputation for X (no info from y)
confusion matrix:
[1114, 228]
[104, 84]
              precision    recall  f1-score   support

     Class 0       0.91      0.83      0.87       671
     Class 1       0.27      0.45      0.34        94

    accuracy                           0.78       765
   macro avg       0.59      0.64      0.60       765
weighted avg       0.84      0.78      0.80       765

42 variables, bayesian inputation for X (no info from y)
confusion matrix:
[82, 1262]
[16, 170]
              precision    recall  f1-score   support

     Class 0       0.84      0.06      0.11       672
     Class 1       0.12      0.91      0.21        93

    accuracy                           0.16       765
   macro avg       0.48      0.49      0.16       765
weighted avg       0.75      0.16      0.13       765



