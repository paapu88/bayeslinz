import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pandas as pd
from pathlib import Path
from utils.mynumpy import get_train_test
from utils.mypredictor import predict_probit
from utils.mysamplers import sample_beta, sample_z, impute_missing_values
from utils.myreports import report
from imblearn.over_sampling import SMOTE
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix, roc_auc_score, classification_report

data = pd.read_csv(
    Path.home().joinpath(
        "Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.1_method_noInpute_withBB_aoplus.csv"
    ),
    index_col=0,
)
print(data.head())
print(data.describe())
cols = data.columns[2:].to_list()
X = data.iloc[:, 2:].values
# ones for constant coefficiennt beta0
X = np.hstack([np.ones((X.shape[0], 1)), X])
y = data.iloc[:, 1].values
print(X[:3, :])
print(f"y: {y[:3]}")
print(f"cols {cols}")
mypreds=[]
mytrues=[]
for repeat in range(10):
    X_train, y_train, X_test, y_test = get_train_test(X=X, y=y)
    smote = SMOTE(random_state=42)
    X_train = np.where(np.isnan(X_train), -1234567, X_train)
    X_train, y_train = smote.fit_resample(X_train, y_train)
    # oversample because there is 1/10 bankruptcies
    X_train = np.where(X_train == -1234567, np.nan, X_train)

    print(f"X_train {X_train.shape}")
    print(f"y_train {y_train.shape}")
    print(f"X_test {X_test.shape}")
    print(f"y_test {y_test.shape}")
    print(f"X {X.shape}")
    print(f"y {y.shape}")
    #X = None
    #y = None

    # Initialize parameters
    n_iter = 100  # 1000
    burn = 50  # 500X = None
    beta_prior_mean = np.zeros(X_train.shape[1])
    beta_prior_cov = np.eye(X_train.shape[1])
    beta_samples = np.zeros((n_iter, X_train.shape[1]))
    z_samples = np.zeros((n_iter, X_train.shape[0]))

    # Initial values
    beta = np.zeros(X_train.shape[1])
    z = np.random.randn(X_train.shape[0])
    X_imputed = impute_missing_values(X_train, beta)


    # Gibbs sampling for training
    for i in range(n_iter):
        z = sample_z(X_imputed, y_train, beta)
        #beta_prior_mean, beta_prior_cov, 
        beta_prior_mean, beta_prior_cov, beta = sample_beta(
            X_imputed, z, beta_prior_mean, beta_prior_cov
        )
        X_imputed = impute_missing_values(X_train, beta)
        beta_samples[i, :] = beta
        #print(f"i {i} beta {beta}")
        z_samples[i, :] = z

    # Posterior mean of beta
    beta_mean = beta_samples[burn:, :].mean(axis=0)
    print("Posterior mean of beta:", beta_mean)

    # predictions
    last_goods = n_iter - burn
    X_imputed = impute_missing_values(X_test, beta_mean)
    # Gibbs sampling
    preds = predict_probit(beta_samples=beta_samples[-last_goods:, :],
                        X_test=X_imputed)
    preds = [1 if value >= 0.5 else 0 for value in preds]

    #for true, pred in zip(y_test.tolist(), preds):
    #    print(f"true {true} pred {pred}")
    mytrues = mytrues + y_test.tolist()
    mypreds = mypreds + preds
    report(true=mytrues, pred=mypreds)

sys.exit()

for i in range(n_iter - burn):

    beta = beta_samples[-i, :]
    X_imputed = impute_missing_values(X_test, beta)


# Plot the trace of beta
fig, ax = plt.subplots(figsize=(10, 6))
for j in range(beta_samples.shape[1]):
    ax.plot(beta_samples[:, j], label=f"beta_{j}")
ax.set_title("Trace plot of beta")
ax.legend()
plt.tight_layout()
plt.show()

# Plot the posterior distribution of beta
fig, ax = plt.subplots(figsize=(10, 6))
for j in range(beta_samples.shape[1]):
    print(f"j {j} mean(beta_j) {beta_mean[j]}")
    ax.hist(beta_samples[burn:, j], bins=30, alpha=0.5, density=True, label=f"beta_{j}")
ax.set_title("Posterior distribution of beta")
ax.legend()
plt.tight_layout()
plt.show()



