import numpy as np
import pandas as pd
import scipy.stats as stats
from sklearn.impute import SimpleImputer
import pandas as pd
import numpy as np
from scipy.stats import norm
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import norm, truncnorm, bernoulli
from utils.myreports import report
from imblearn.over_sampling import SMOTE
from utils.mydata import load_data, data_to_numpy
from utils.betaplots import histograms_X, histograms, traces, autocorrelations
from utils.mynumpy import nearestPD, standard_normal_with_nans, sample_from_sigmoid,solve_for_x_i
from utils.mynumpy import standard_normal_with_nans_not_col0,largest_values,has_nans_or_infs
from utils.mynumpy import least_squares_estimate
from utils.mysamplers import compute_multivariate_normal_params, impute_nans_using_mvn
from utils.myimputers import knn_impute_with_masks
import gc
from sklearn.utils import shuffle


# Define logistic log-likelihood
def logistic_log_likelihood(beta, X, y):
    logits = X @ beta
    return np.sum(y * logits - np.log(1 + np.exp(logits)))

# Define the prior for beta (normal prior)
def log_prior_beta(beta, mu=0, sigma=10):
    return np.sum(stats.norm.logpdf(beta, mu, sigma))

# Define the prior for X (multivariate normal prior)
def log_prior_X(X, mu, Sigma):
    #print(f"2: Sigma {Sigma}")
    return np.sum(stats.multivariate_normal.logpdf(X, mean=mu, cov=Sigma, allow_singular=True))

# Proposal distributions for beta
def proposal_beta(beta, scale):
    return np.random.normal(beta, scale, size=beta.shape)

def proposal_X(X, mask, y, beta, mu, Sigma, scale_last_nan:float):
    """ uses y info mildly, on ly for one nan in X row at time
        scaling it by scale_last_nan (otherwise jump in X is too big)"""
    X_proposal = X.copy()
    n_rows, n_cols = X.shape
    

    for i in range(n_rows):
        # Indices of NaN values in the current row
        nan_indices = np.where(mask[i])[0]
        #print(f"nan indices 0: {np.where(mask[i])[0]}")
        if len(nan_indices) > 0:
            # Randomly select one NaN index to use y information
            selected_index = np.random.choice(nan_indices)
            
            # Impute all NaN values without using y information
            # Impute all NaN values without using y information
            for j in nan_indices:
                if j != selected_index:
                    std_factor = Sigma[j, j] - Sigma[j, :] @ np.linalg.inv(Sigma) @ Sigma[:, j]
                    std_j = np.sqrt(np.clip(std_factor, a_min=0, a_max=None))
                    X_proposal[i, j] = X_proposal[i, j] + np.random.normal(0, std_j)
            
            # Sample result from the sigmoid distribution given y
            if y[i] == 1:
                sampled_value_x = sample_from_sigmoid(constraint="x > 0")
            else:
                sampled_value_x = sample_from_sigmoid(constraint="x < 0")
            # then solve the missing x_i
            #print(f"nan old: {X_proposal[i, selected_index]}")
            xtrue = solve_for_x_i(y=sampled_value_x, X=X_proposal[i, 1:], beta=beta, j=selected_index)
            dx = xtrue - X_proposal[i, selected_index]
            X_proposal[i, selected_index] = X_proposal[i, selected_index] + dx*scale_last_nan
            #print(f"nan new: {X_proposal[i, selected_index]}")

    #dX = X- X_proposal
    #largest_values(dX)
    #sys.exit()
    # print(f"proposla: {X[7,0:10]-X_proposal[7,0:10]}")
    return X_proposal



# Proposal distribution for X, no y observed
def proposal_X_no_y(X, mask, mu, Sigma):
    X_proposal = X.copy()
    # Only propose new values for missing entries
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            if mask[i, j]:
                mean_j = mu[j] + Sigma[j, :] @ np.linalg.inv(Sigma) @ (X_proposal[i, :] - mu)
                std_factor = Sigma[j, j] - Sigma[j, :] @ np.linalg.inv(Sigma) @ Sigma[:, j]
                std_j = np.sqrt(np.clip(std_factor, a_min=0, a_max=None))
                X_proposal[i, j] = np.random.normal(mean_j, std_j)
    return X_proposal

# Metropolis-Hastings sampler
def mh_sampler(X, y, burn:int, iter_tot:int, 
               scale_beta:float, scale_x:float, scale_last_nan:float,
               burn_nans:int):
    # Get a mask of NaN values
    #print(f"col means {np.nanmean(X, axis=0)}" )
    mask = np.isnan(X)
    # Impute initial values for missing data
    imputer = SimpleImputer(strategy='constant', fill_value=0)
    X_imputed = imputer.fit_transform(X)
    # Calculate initial values for mu and Sigma from filtered data
    mu_X = np.mean(X_imputed, axis=0)
    Sigma_X = np.cov(X_imputed, rowvar=False)
    #print(f"mu_X {mu_X.shape}")
    #print(f"mu_X {mu_X}")
    #print(f"sigma_X {Sigma_X.shape}")
    #print(f"sigma_X {Sigma_X}")

    #matrix[0, 0] = 1
    Sigma_X = nearestPD(A=Sigma_X)
    #print(f"1 Sigma_X {Sigma_X}")

    # Run MH sampler with priors and missing value imputation
    initial_beta = least_squares_estimate(X=X, y=y) #np.zeros(X.shape[1])
    beta_samples = []
    X_samples = []
    
    current_beta = initial_beta
    current_X = X_imputed
    current_log_post = logistic_log_likelihood(current_beta, current_X, y) + log_prior_beta(current_beta) + log_prior_X(current_X, mu_X, Sigma_X*scale_x)
    accept_count = 0
    last_iter = 0
    beta_check=100
    ar=None
    for iter in range(iter_tot):
        # Propose new beta
        last_iter +=1
        proposed_beta = proposal_beta(current_beta, scale=scale_beta)
        # Propose new X for missing values
        # use y only after we have some reasonable values in beta
        if iter <= burn_nans:
            proposed_X = proposal_X_no_y(X=current_X, mask=mask, 
                                         mu=mu_X, Sigma=Sigma_X*scale_x)
        else:
            proposed_X = proposal_X(X=current_X, 
                                  mask=mask, 
                                  y=y, 
                                  beta=current_beta, 
                                  mu=mu_X, 
                                  Sigma=Sigma_X*scale_x,
                                  scale_last_nan=scale_last_nan)
        #proposed_X=current_X
        proposed_log_post = logistic_log_likelihood(proposed_beta, proposed_X, y) + log_prior_beta(proposed_beta) + log_prior_X(proposed_X, mu_X, Sigma_X*scale_x)        
        acceptance_ratio = np.exp(proposed_log_post - current_log_post)
        
        if np.random.rand() < acceptance_ratio:
            current_beta = proposed_beta
            current_X = proposed_X
            current_log_post = proposed_log_post
            accept_count += 1
            if has_nans_or_infs(current_X):
                raise RuntimeError("X has nans after proposal...")


        if iter > burn:
            beta_samples.append(current_beta)
            if iter % 100 == 0:  # Reduce the frequency of saving X_samples to reduce memory usage
                if has_nans_or_infs(current_X):
                    raise RuntimeError("100: X has nans after proposal...")
                X_samples.append(current_X.copy())
                mu_X = np.mean(current_X, axis=0)
                Sigma_X = np.cov(current_X, rowvar=False)
                Sigma_X = nearestPD(A=Sigma_X)
                gc.collect()  # Explicitly free memory

        if last_iter > beta_check:
            ar =  accept_count / beta_check
            accept_count = 0
            last_iter=0 
            if scale_beta < 1e-3:
                scale_last_nan /= 10.
                scale_x /=10.
            if ar > 0.35:
                scale_beta = scale_beta*1.5
                scale_last_nan=scale_last_nan*1.01
                scale_x=scale_x*1.01
            if ar < 0.1:
                scale_beta = scale_beta/1.5
                scale_last_nan=scale_last_nan/1.01
                scale_x=scale_x/1.01

            

        print(f"iter {iter} beta {current_beta} scale_beta {scale_beta} scale_last_nan {scale_last_nan} scale_x {scale_x} accept ratio {ar}")
        
    return np.array(beta_samples), np.array(X_samples)

def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def test_model(X_test, samples):
    #X_imputed = impute_missing_values_test(X_test, samples)
    beta_mean = samples.mean(axis=0)
    y_pred_prob = sigmoid(X_test @ beta_mean)
    y_pred = (y_pred_prob >= 0.5).astype(int)
    return y_pred, y_pred_prob

# Main Function
def main(file_path, burn:int, iter_tot:int, burn_nans:int,
         scale_beta:float, scale_x:float, scale_last_nan:float):
    data = load_data(file_path)
    X, y, col_names = data_to_numpy(data)
    print(f"before {X.shape}")
    histograms_X(X=X, col_names=col_names)
    X = standard_normal_with_nans(X)
    
    ones_column = np.ones((X.shape[0], 1))
    X = np.hstack((ones_column, X))
    col_names = ["Intercept"]+ col_names
    print(col_names)
    
    histograms_X(X=X, col_names=col_names)

    print(f"after    {X.shape}")

    # train and test split, oversampling for train   
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    smote = SMOTE(random_state=42)
    X_train = np.where(np.isnan(X_train), -1234567, X_train)
    X_train, y_train = smote.fit_resample(X_train, y_train)
    # oversample because there is 1/10 bankruptcies
    X_train = np.where(X_train == -1234567, np.nan, X_train)
    X_train, y_train = shuffle(X_train, y_train, random_state=42)
    X_train=standard_normal_with_nans_not_col0(X=X_train)
    print(f"col means X_train {np.nanmean(X_train, axis=0)}" )
    # sample beta and X
    mask_train = np.isnan(X_train)
    mask_test = np.isnan(X_test)
    
    samples_beta, samples_X = mh_sampler(X=X_train, y=y_train, burn=burn, burn_nans=burn_nans,
                                        iter_tot=iter_tot, 
                                        scale_beta=scale_beta,
                                        scale_x=scale_x,
                                        scale_last_nan=scale_last_nan)
    print(f"NANS in X samples after training")
    print(f"samples_X, NANS: {has_nans_or_infs(samples_X)}")
    print(f"samples_beta, : {samples_beta.shape}")

    traces(samples_beta=samples_beta, col_names=col_names)
    histograms(samples_beta=samples_beta, col_names=col_names)
    autocorrelations(samples_beta=samples_beta, col_names=col_names)
    #y_pred, y_pred_prob = test_model(X_test, samples)

    # Print results
    #print(f"Acceptance Rate: {acceptance_rate}")
    print(f"after {samples_X.shape}")
    X_imputed_train = samples_X.reshape(-1,samples_X.shape[-1])
    histograms_X(X=X_imputed_train, col_names=col_names)

    # run prediction for the test set
    # assume distribution in train and test to be the same, so use mu and sigma from learned imputed X_train
    X_test=standard_normal_with_nans_not_col0(X=X_test)
    #mu, cov = compute_multivariate_normal_params(X=X_imputed_train)
    #X_test = impute_nans_using_mvn(X_test, mu, cov)
    X_test=knn_impute_with_masks(X_train=np.mean(samples_X, axis=0), 
                                 X_test=X_test, 
                                 mask_train=mask_train, 
                                 mask_test=mask_test,
    )
    
    y_pred, y_pred_prob = test_model(X_test, samples_beta)
    accuracy = np.mean(y_pred == y_test)
    print(f"Accuracy: {accuracy}")

    report(true=y_test, pred=y_pred)




if __name__ == "__main__":
    from pathlib import Path
    # 12 variables, with inputation
    main(burn=10000, iter_tot=15000, burn_nans=100, 
         scale_beta=1e-2, scale_x=1e0, scale_last_nan=1e-4,
    file_path=Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.9_method_noInpute_withBB.csv"))
    #NOT GOOD:
    #F1 Score: 0.18
    #onfusion Matrix:
    #[[ 297 1042]
    #[  66  125]]
    #ROC AUC Score: 0.44
    # 42 variables, with inputation
    #main(burn=5000, iter_tot=10000, scale_beta=1e-2, scale_x=1e-1, scale_last_nan=1e-3,
    # file_path=Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.1_method_noInpute_withBB.csv"))
    