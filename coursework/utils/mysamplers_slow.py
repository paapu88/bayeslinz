import numpy as np
import scipy.stats as stats


def sample_beta(X, z, beta_prior_mean, beta_prior_cov, size=None):
    """
    Sample beta from the posterior distribution.
    https://bookdown.org/aramir21/IntroductionBayesianEconometricsGuidedTour/probit-model.html
    Chap 6.3
    and page 77 in Helga notes
    """
    beta_post_cov = np.linalg.inv(np.linalg.inv(beta_prior_cov) + X.T @ X)
    beta_post_mean = beta_post_cov @ (
        np.linalg.inv(beta_prior_cov) @ beta_prior_mean + X.T @ z
    )
    if size is None:
        return (
            beta_post_mean,
            beta_post_cov,
            np.random.multivariate_normal(beta_post_mean, beta_post_cov),
        )
    return np.random.multivariate_normal(
        mean=beta_post_mean, cov=beta_post_cov, size=size
    )


def sample_z(X, y, beta):
    """
    Sample z from the truncated normal distribution.
    """
    mean = X @ beta
    z = np.zeros_like(mean)
    for i in range(len(y)):
        if y[i] == 1:
            z[i] = stats.truncnorm.rvs(
                a=(0 - mean[i]) / 1, b=np.inf, loc=mean[i], scale=1
            )
        else:
            z[i] = stats.truncnorm.rvs(
                a=-np.inf, b=(0 - mean[i]) / 1, loc=mean[i], scale=1
            )
    return z


def impute_missing_values(X, beta):
    """
    Impute missing values in X using the conditional distribution.
    """
    X_imputed = X.copy()
    for i in range(X.shape[0]):
        for j in range(
            1, X.shape[1]
        ):  # Start from 1 to avoid changing the intercept term
            if np.isnan(X[i, j]):
                # Impute missing value based on the observed part of X and the current beta
                observed_indices = ~np.isnan(X[i, :])

                #print(f"observed_indices {observed_indices}")
                #print(f"beta {beta.shape}")
                mean = X[i, observed_indices] @ beta[observed_indices]
                X_imputed[i, j] = np.random.normal(mean, 1)
    return X_imputed