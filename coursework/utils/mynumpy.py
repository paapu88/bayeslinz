import numpy as np
from typing import Tuple
from numpy import linalg as la
from sklearn.impute import SimpleImputer

def least_squares_estimate(X, y):
    # Ensure X and y are numpy arrays
    X = np.asarray(X)
    y = np.asarray(y)
    
    # Perform mean imputation
    imputer = SimpleImputer(strategy='median')
    X_imputed = imputer.fit_transform(X)
    
    # Compute the least squares estimate using the normal equation
    XTX_inv = np.linalg.inv(X_imputed.T @ X_imputed)
    XTy = X_imputed.T @ y
    beta_hat = XTX_inv @ XTy
    
    return beta_hat


def sample_from_sigmoid(constraint, size=1, random_state=None):
    if random_state is not None:
        np.random.seed(random_state)
    
    if constraint == 'x > 0': # x>0
        # Sample from uniform distribution in (0.5, 1)
        p = np.random.uniform(0.501, 0.75, size)
    elif constraint == 'x < 0':
        # Sample from uniform distribution in (0, 0.5)
        p = np.random.uniform(0.25, 0.499, size)
    else:
        raise ValueError("Invalid constraint. Use 'x > 0' or 'x < 0'")
    
    # Transform p using the logit function
    x = np.log(p / (1 - p))
    
    return x

def solve_for_x_i(y, X, beta, j):
    # Extract beta0 from beta
    beta0 = beta[0]
    
    # Extract the coefficients for the linear combination
    beta_rest = beta[1:]
    
    # Ensure X and beta_rest are numpy arrays
    X = np.asarray(X)
    beta_rest = np.asarray(beta_rest)
    
    # Compute the dot product excluding x_j * beta_j
    dot_product_excluding_xj = np.dot(X, beta_rest) - X[j-1] * beta_rest[j-1]
    
    # Solve for x_j
    x_j = (y - beta0 - dot_product_excluding_xj) / beta_rest[j-1]
    
    return x_j

def has_nans_or_infs(arr):
    return np.isnan(arr).any() or np.isinf(arr).any()


def standard_normal_with_nans(X:np.ndarray)-> np.ndarray:
    means = np.nanmean(X, axis=0)   
    stds = np.nanstd(X, axis=0)
    X = (X - means) / stds
    return X

def standard_normal_with_nans_not_col0(X:np.ndarray)-> np.ndarray:
    first_column = X[:, 0]
    remaining_columns = X[:, 1:]

    means = np.nanmean(remaining_columns, axis=0)   
    stds = np.nanstd(remaining_columns, axis=0)
    remaining_columns = (remaining_columns - means) / stds
    return np.column_stack((first_column, remaining_columns))

def largest_values(X:np.ndarray)->None:
    # Compute the absolute values
    abs_X = np.abs(X)

    # Flatten the array to make it one-dimensional
    flat_abs_X = abs_X.flatten()

    # Find the indices of the ten largest values
    indices = np.argpartition(flat_abs_X, -10)[-10:]

    # Retrieve the values using these indices and sort them
    largest_values = flat_abs_X[indices]
    sorted_largest_values = np.sort(largest_values)[::-1]

    # Optionally, find the original values and their positions
    original_values = X.flatten()[indices]

    print("Ten largest values in absolute terms:")
    print(sorted_largest_values)
    print("\nOriginal values and their positions:")
    for val, idx in zip(original_values, indices):
        print(f"Value: {val}, Position: {np.unravel_index(idx, X.shape)}")


def get_train_test(
    X: np.ndarray, y: np.ndarray, ratio=0.9
) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    # Generate random permutation of indices
    n = X.shape[0]
    indices = np.random.permutation(n)
    # Define split point (90% of 100)
    split_point = int(ratio * n)
    # Split indices into training and test sets
    train_indices = indices[:split_point]
    test_indices = indices[split_point:]
    # Create training and test sets using the indices
    X_train = X[train_indices]
    y_train = y[train_indices]
    X_test = X[test_indices]
    y_test = y[test_indices]
    return X_train, y_train, X_test, y_test


# from https://stackoverflow.com/questions/43238173/python-convert-matrix-to-positive-semi-definite
def isPD(B):
    """Returns true when input is positive-definite, via Cholesky"""
    try:
        _ = la.cholesky(B)
        return True
    except la.LinAlgError:
        return False

# from https://stackoverflow.com/questions/43238173/python-convert-matrix-to-positive-semi-definite
def nearestPD(A):
    """Find the nearest positive-definite matrix to input

    A Python/Numpy port of John D'Errico's `nearestSPD` MATLAB code [1], which
    credits [2].

    [1] https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd

    [2] N.J. Higham, "Computing a nearest symmetric positive semidefinite
    matrix" (1988): https://doi.org/10.1016/0024-3795(88)90223-6
    """

    B = (A + A.T) / 2
    _, s, V = la.svd(B)

    H = np.dot(V.T, np.dot(np.diag(s), V))

    A2 = (B + H) / 2

    A3 = (A2 + A2.T) / 2

    if isPD(A3):
        return A3

    spacing = np.spacing(la.norm(A))
    # The above is different from [1]. It appears that MATLAB's `chol` Cholesky
    # decomposition will accept matrixes with exactly 0-eigenvalue, whereas
    # Numpy's will not. So where [1] uses `eps(mineig)` (where `eps` is Matlab
    # for `np.spacing`), we use the above definition. CAVEAT: our `spacing`
    # will be much larger than [1]'s `eps(mineig)`, since `mineig` is usually on
    # the order of 1e-16, and `eps(1e-16)` is on the order of 1e-34, whereas
    # `spacing` will, for Gaussian random matrixes of small dimension, be on
    # othe order of 1e-16. In practice, both ways converge, as the unit test
    # below suggests.
    I = np.eye(A.shape[0])
    k = 1
    while not isPD(A3):
        mineig = np.min(np.real(la.eigvals(A3)))
        A3 += I * (-mineig * k**2 + spacing)
        k += 1

    return A3

