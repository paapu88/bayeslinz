from sklearn.impute import KNNImputer
import numpy as np

def knn_impute_with_masks(X_train, X_test, mask_train, mask_test, n_neighbors=5):
    """
    Impute NaNs in X_test using KNN imputation based on X_train and mask features.
    """
    # Create mask features
    mask_train_features = mask_train.astype(float)
    mask_test_features = mask_test.astype(float)
    
    # Combine X and mask features
    X_train_with_mask = np.hstack((X_train, mask_train_features))
    X_test_with_mask = np.hstack((X_test, mask_test_features))
    
    # Combine X_train and X_test to fit the imputer
    X_combined = np.vstack((X_train_with_mask, X_test_with_mask))
    
    # Create the KNN imputer
    imputer = KNNImputer(n_neighbors=n_neighbors)
    
    # Fit the imputer on the combined data and transform
    X_combined_imputed = imputer.fit_transform(X_combined)
    
    # Extract the imputed X_test from the combined data
    X_test_imputed_with_mask = X_combined_imputed[X_train.shape[0]:]
    
    # Separate imputed X_test from its mask features
    X_test_imputed = X_test_imputed_with_mask[:, :X_test.shape[1]]
    
    return X_test_imputed