import numpy as np
from scipy.stats import norm


def predict_probit(beta_samples: np.ndarray, X_test: np.ndarray) -> np.ndarray:
    """
    Make predictions for a test set using Bayesian estimates for the beta coefficients
    from a probit model.

    Parameters:
    beta_samples (np.ndarray): Posterior samples of beta, shape (num_samples, num_features).
    X_test (np.ndarray): Test set feature matrix, shape (num_test_samples, num_features).

    Returns:
    np.ndarray: Predicted probabilities for the test set, shape (num_test_samples,).
    """
    num_samples = beta_samples.shape[0]
    num_test_samples = X_test.shape[0]

    # Initialize array to store predicted probabilities
    predicted_probabilities = np.zeros(num_test_samples)

    # Loop through each posterior sample and compute the probabilities
    for beta in beta_samples:
        linear_predictor = np.dot(X_test, beta)
        probabilities = norm.cdf(
            linear_predictor
        )  # Probit model uses the CDF of the standard normal distribution
        predicted_probabilities += probabilities

    # Average the probabilities over all posterior samples
    predicted_probabilities /= num_samples

    return predicted_probabilities
