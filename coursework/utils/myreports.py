from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix, roc_auc_score, classification_report
import yaml

def report(true, pred, outfile=None):
    results = {}
    # Accuracy
    accuracy = accuracy_score(true, pred)
    print(f"Accuracy: {accuracy:.2f}")
    results["accuracy"]=f"{accuracy:.2f}"
    # Precision
    precision = precision_score(true, pred)
    print(f"Precision: {precision:.2f}")
    results["precision"]=f"{precision:.2f}"
    # Recall
    recall = recall_score(true, pred)
    print(f"Recall: {recall:.2f}")
    results["recall"]=f"{recall:.2f}"
    # F1 Score
    f1 = f1_score(true, pred)
    print(f"F1 Score: {f1:.2f}")
    results["f1"]=f"{f1:.2f}"
    # Confusion Matrix
    conf_matrix = confusion_matrix(true, pred)
    print("Confusion Matrix:")
    print(conf_matrix)
    results["ConfusionMatrix"]=str(conf_matrix)
    # ROC AUC Score
    roc_auc = roc_auc_score(true, pred)
    print(f"ROC AUC Score: {roc_auc:.2f}")
    results["roc_auc"]=f"{roc_auc:.2f}"

    # Classification Report
    class_report = classification_report(true, pred)
    print("Classification Report:")
    print(class_report)
    results["ClassificationReport"]=str(class_report)
    if outfile is not None:
        with open(outfile, 'w') as file:
            yaml.dump(results, file, default_flow_style=False)