import numpy as np
import scipy.stats as stats
from scipy.linalg import inv
from scipy.stats import multivariate_normal

def sample_beta(X, z, beta_prior_mean, beta_prior_cov, size=None):
    """
    Sample beta from the posterior distribution.
    """
    XtX = X.T @ X
    beta_prior_cov_inv = inv(beta_prior_cov)
    beta_post_cov = inv(beta_prior_cov_inv + XtX)
    beta_post_mean = beta_post_cov @ (beta_prior_cov_inv @ beta_prior_mean + X.T @ z)

    if size is None:
        return (
            beta_post_mean,
            beta_post_cov,
            np.random.multivariate_normal(beta_post_mean, beta_post_cov),
        )
    return np.random.multivariate_normal(mean=beta_post_mean, cov=beta_post_cov, size=size)

def sample_z(X, y, beta):
    """
    Sample z from the truncated normal distribution.
    """
    mean = X @ beta
    z = np.where(y == 1,
                 stats.truncnorm.rvs(a=(0 - mean) / 1, b=np.inf, loc=mean, scale=1),
                 stats.truncnorm.rvs(a=-np.inf, b=(0 - mean) / 1, loc=mean, scale=1))
    return z


def impute_missing_values_train(X, y, beta):
    """
    Impute missing values in X using the conditional distribution during training.
    Uses the known response variable y.
    """
    X_imputed = X.copy()
    for j in range(1, X.shape[1]):  # Start from 1 to avoid changing the intercept term
        missing_indices = np.isnan(X[:, j])
        if np.any(missing_indices):
            for i in np.where(missing_indices)[0]:
                observed_indices = ~np.isnan(X[i, :])
                mean = X[i, observed_indices] @ beta[observed_indices]
                if y[i] == 1:
                    X_imputed[i, j] = stats.truncnorm.rvs(
                        a=(0 - mean) / 1, b=np.inf, loc=mean, scale=1
                    )
                else:
                    X_imputed[i, j] = stats.truncnorm.rvs(
                        a=-np.inf, b=(0 - mean) / 1, loc=mean, scale=1
                    )
    return X_imputed

def impute_missing_values_test(X, beta):
    """
    Impute missing values in X using the conditional distribution during testing.
    Does not use the response variable y.
    """
    X_imputed = X.copy()
    for j in range(1, X.shape[1]):  # Start from 1 to avoid changing the intercept term
        missing_indices = np.isnan(X[:, j])
        if np.any(missing_indices):
            for i in np.where(missing_indices)[0]:
                observed_indices = ~np.isnan(X[i, :])
                mean = X[i, observed_indices] @ beta[observed_indices]
                X_imputed[i, j] = np.random.normal(mean, 1)
    return X_imputed

def compute_multivariate_normal_params(X):
    """
    Compute the mean vector and covariance matrix of X, excluding the first column.
    """
    # Exclude the first column (intercept)
    X_reduced = X[:, 1:]
    
    # Mean vector
    mu = np.mean(X_reduced, axis=0)
    
    # Covariance matrix
    cov = np.cov(X_reduced, rowvar=False)
    
    return mu, cov

def impute_nans_using_mvn(X_test, mu, cov, epsilon=1e-6):
    """
    Impute NaNs in X_test using the provided mean vector and covariance matrix, excluding the first column.
    """
    X_test_imputed = X_test.copy()
    for i in range(X_test.shape[0]):
        nan_indices = np.isnan(X_test[i, 1:])  # Ignore the first column
        if np.any(nan_indices):
            observed_indices = ~nan_indices
            observed_values = X_test[i, 1:][observed_indices]
            mu_obs = mu[observed_indices]
            mu_mis = mu[nan_indices]
            cov_oo = cov[np.ix_(observed_indices, observed_indices)]
            cov_mo = cov[np.ix_(nan_indices, observed_indices)]
            
            # Add regularization to the covariance matrix to ensure it's invertible
            cov_oo += np.eye(cov_oo.shape[0]) * epsilon
            cov_oo_inv = np.linalg.inv(cov_oo)
            
            mu_cond = mu_mis + cov_mo @ cov_oo_inv @ (observed_values - mu_obs)
            cov_cond = cov[np.ix_(nan_indices, nan_indices)] - cov_mo @ cov_oo_inv @ cov_mo.T
            
            X_test_imputed[i, 1:][nan_indices] = multivariate_normal.rvs(mean=mu_cond, cov=cov_cond)
    
    return X_test_imputed


def compute_empirical_distributions(X):
    """
    Compute empirical histograms and correlations from X.
    """
    distributions = []
    for col in range(X.shape[1]):
        hist, bin_edges = np.histogram(X[:, col], bins='auto', density=True)
        distributions.append((hist, bin_edges))
    correlations = np.corrcoef(X, rowvar=False)
    return distributions, correlations

def sample_from_histogram(hist, bin_edges):
    """
    Sample a value from the given histogram.
    """
    cdf = np.cumsum(hist)
    cdf = cdf / cdf[-1]  # Normalize to make it a proper CDF
    random_sample = np.random.rand()
    bin_index = np.searchsorted(cdf, random_sample)
    bin_min = bin_edges[bin_index]
    bin_max = bin_edges[bin_index + 1]
    return np.random.uniform(bin_min, bin_max)

def impute_using_empirical(X_test, distributions, correlations):
    """
    Impute NaNs in X_test using empirical distributions and correlations from X_train.
    """
    X_test_imputed = X_test.copy()
    for i in range(X_test.shape[0]):
        nan_indices = np.isnan(X_test[i])
        if np.any(nan_indices):
            observed_indices = ~nan_indices
            observed_values = X_test[i, observed_indices]
            
            for nan_idx in np.where(nan_indices)[0]:
                # Sample from the empirical distribution of the feature
                hist, bin_edges = distributions[nan_idx]
                imputed_value = sample_from_histogram(hist, bin_edges)
                X_test_imputed[i, nan_idx] = imputed_value
                
                # Adjust the imputed value based on observed correlations
                for obs_idx in np.where(observed_indices)[0]:
                    corr = correlations[nan_idx, obs_idx]
                    if not np.isnan(corr):
                        X_test_imputed[i, nan_idx] += corr * (observed_values[obs_idx] - X_test_imputed[i, obs_idx])
    
    return X_test_imputed