import argparse
import yaml

def parse_arguments():
    parser = argparse.ArgumentParser(description='Read a YAML file.')
    parser.add_argument('--filename', type=str, help='The path to the YAML file.')
    return parser.parse_args()

def read_yaml_file(filename):
    with open(filename, 'r') as file:
        return yaml.safe_load(file)
    