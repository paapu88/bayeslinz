import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
import numpy as np
from typing import List

def traces(samples_beta, col_names: List[str]):
    # Trace plots for beta
    plt.figure(figsize=(12, 24))
    for col_name, i in zip(col_names, range(samples_beta.shape[1])):
        plt.subplot(samples_beta.shape[1], 1, i + 1)
        plt.plot(samples_beta[:, i])
        plt.title(f'beta_{col_name}')
    plt.tight_layout()
    plt.show()

def histograms(samples_beta, col_names: List[str]):
    # Histograms for beta
    plt.figure(figsize=(12, 24))
    for col_name, i in zip(col_names, range(samples_beta.shape[1])):
        plt.subplot(samples_beta.shape[1], 1, i + 1)
        plt.hist(samples_beta[:, i], bins=30, density=True)
        plt.title(f'beta_{col_name}')
    plt.tight_layout()
    plt.show()

def autocorrelations(samples_beta, col_names: List[str]):
    # Autocorrelation for beta
    plt.figure(figsize=(12, 24))
    for col_name, i in zip(col_names, range(samples_beta.shape[1])):
        plt.subplot(samples_beta.shape[1], 1, i + 1)
        plot_acf(samples_beta[:, i], ax=plt.gca())
        plt.title(f'beta_{col_name}')
    plt.tight_layout()
    plt.show()

def histograms_X_normal(X: np.ndarray, col_names: List[str]) -> None:
    """
    Plots histograms for each column in the standardized X array.

    Assumes that each variable in X is standardized (mean=0, std=1).
    """
    n_cols = X.shape[1]
    plt.figure(figsize=(12, 3 * n_cols))
    
    for i, col_name in enumerate(col_names):
        plt.subplot(n_cols, 1, i + 1)
        
        # Plot the histogram
        plt.hist(X[:, i], bins=32, density=True, alpha=0.6, color='g', range=(-4, 4))
        
        # Set the x-axis limits
        plt.xlim(-4, 4)
        
        plt.title(f'{col_name}')
        plt.xlabel('Value')
        plt.ylabel('Density')
    
    plt.tight_layout()
    plt.show()

def histograms_X(X: np.ndarray, col_names: List[str]) -> None:
    # Histograms for X with bins set to the range between the 10% and 90% quantiles
    plt.figure(figsize=(12, 24))
    for col_name, i in zip(col_names, range(X.shape[1])):
        plt.subplot(X.shape[1], 1, i + 1)
        # Filter out NaN and Inf values
        valid_values = X[:, i][~np.isnan(X[:, i]) & ~np.isinf(X[:, i])]
        # Calculate 10% and 90% quantiles
        q10, q90 = np.percentile(valid_values, [5, 95])
        
        # Create bins between q10 and q90
        bins = np.linspace(q10, q90, 30)
        
        # Clip the values to be within q10 and q90 for the histogram
        clipped_values = np.clip(valid_values, q10, q90)
        
        # Plot histogram
        plt.hist(clipped_values, bins=bins, density=True, edgecolor='black')
        plt.title(f'{col_name}')
        
        # Set the x-axis limits to be between q10 and q90
        plt.xlim(q10, q90)
    plt.tight_layout()
    plt.show()

def old_histograms_X(X:np.ndarray, col_names:List[str])->None:
    # Histograms for beta
    plt.figure(figsize=(12, 6))
    for col_name, i in zip(col_names, range(X.shape[1])):
        plt.subplot(X.shape[1], 1, i + 1)
        plt.hist(X[:, i], bins=30, density=True)
        plt.title(f'{col_name}')
    plt.tight_layout()
    plt.show()