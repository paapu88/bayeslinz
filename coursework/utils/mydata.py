import pandas as pd

def load_data(file_path):
    data = pd.read_csv(file_path, index_col=0)
    return data

def data_to_numpy(data):    
    # Separate features and target
    X = data.drop(columns=['FONUM','bankruptcy'])
    y = data['bankruptcy']
    # Add intercept term
    #X.insert(0, 'intercept', 1)
    return X.to_numpy(), y.to_numpy(), list(X.columns)