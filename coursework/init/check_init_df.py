from pathlib import Path
import pandas as pd

df = pd.read_csv(
    Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.1_method_noInpute_withBB_aoplus.csv"),
    index_col=0,
)

df = df[["SIZE"]]
print(df.describe())
        