import pandas as pd
import numpy as np
from pathlib import Path

df = pd.read_csv(
    Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.9_method_noInpute_withBB.csv"),
    index_col=0,
)


# Calculate the percentage of NaN values
total_elements = df.size
nan_elements = df.isna().sum().sum()
percent_nan = (nan_elements / total_elements) * 100

print(f"Percentage of NaN values: {percent_nan:.2f}%")


df = pd.read_csv(
    Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.1_method_noInpute_withBB.csv"),
    index_col=0,
)
# Calculate the percentage of NaN values
total_elements = df.size
nan_elements = df.isna().sum().sum()
percent_nan = (nan_elements / total_elements) * 100

print(f"Percentage of NaN values: {percent_nan:.2f}%")
