import numpy as np
import pandas as pd
import scipy.stats as stats
from sklearn.impute import SimpleImputer

# Define logistic log-likelihood
def logistic_log_likelihood(beta, X, y):
    logits = X @ beta
    return np.sum(y * logits - np.log(1 + np.exp(logits)))

# Define the prior for beta (normal prior)
def log_prior_beta(beta, mu=0, sigma=10):
    return np.sum(stats.norm.logpdf(beta, mu, sigma))

# Define the prior for X (multivariate normal prior)
def log_prior_X(X, mu, Sigma):
    return np.sum(stats.multivariate_normal.logpdf(X, mean=mu, cov=Sigma))

# Proposal distributions for beta
def proposal_beta(beta):
    return np.random.normal(beta, 0.5, size=beta.shape)

# Proposal distribution for X
def proposal_X(X, mask, y, beta, mu, Sigma):
    X_proposal = X.copy()
    # Only propose new values for missing entries
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            if mask[i, j]:
                mean_j = mu[j] + Sigma[j, :] @ np.linalg.inv(Sigma) @ (X_proposal[i, :] - mu)
                std_j = np.sqrt(Sigma[j, j] - Sigma[j, :] @ np.linalg.inv(Sigma) @ Sigma[:, j])
                X_proposal[i, j] = np.random.normal(mean_j, std_j)
                if y[i] == 1:
                    X_proposal[i, j] += np.random.normal(0, 1 / np.exp(X_proposal[i, :] @ beta))
                else:
                    X_proposal[i, j] -= np.random.normal(0, 1 / (1 + np.exp(X_proposal[i, :] @ beta)))
    return X_proposal

# Metropolis-Hastings sampler
def mh_sampler(X, y, initial_beta, iterations, mu, Sigma):
    beta_samples = [initial_beta]
    X_samples = [X.copy()]
    
    current_beta = initial_beta
    current_X = X.copy()
    current_log_post = logistic_log_likelihood(current_beta, current_X, y) + log_prior_beta(current_beta) + log_prior_X(current_X, mu, Sigma)
    
    mask = np.isnan(X)  # Mask of missing values
    
    for _ in range(iterations):
        # Propose new beta
        proposed_beta = proposal_beta(current_beta)
        # Propose new X for missing values
        proposed_X = proposal_X(current_X, mask, y, current_beta, mu, Sigma)
        
        proposed_log_post = logistic_log_likelihood(proposed_beta, proposed_X, y) + log_prior_beta(proposed_beta) + log_prior_X(proposed_X, mu, Sigma)
        
        acceptance_ratio = np.exp(proposed_log_post - current_log_post)
        
        if np.random.rand() < acceptance_ratio:
            current_beta = proposed_beta
            current_X = proposed_X
            current_log_post = proposed_log_post
        
        beta_samples.append(current_beta)
        X_samples.append(current_X.copy())
    
    return np.array(beta_samples), np.array(X_samples)

# Generate simulated data with missing values
np.random.seed(42)
n = 1000
p = 2
X = np.column_stack((np.ones(n), np.random.binomial(1, 0.5, n), np.random.normal(0, 1, n)))
true_beta = np.array([0.5, -1.0, 2.0])
logits = X @ true_beta
probabilities = 1 / (1 + np.exp(-logits))
y = np.random.binomial(1, probabilities)

# Introduce missing values in X
missing_rate = 0.5
mask = np.random.rand(*X.shape) < missing_rate
X[mask] = np.nan

# Impute initial values for missing data
imputer = SimpleImputer(strategy='mean')
X_imputed = imputer.fit_transform(X)

# Calculate initial values for mu and Sigma from imputed data
mu_X = np.mean(X_imputed, axis=0)
Sigma_X = np.cov(X_imputed, rowvar=False)

# Ensure Sigma is positive definite by adding jitter
jitter = 1e-6
Sigma_X += np.eye(Sigma_X.shape[0]) * jitter

# Run MH sampler with priors and missing value imputation
initial_beta = np.zeros(X.shape[1])
iterations = 10000
samples_beta, samples_X = mh_sampler(X_imputed, y, initial_beta, iterations, mu_X, Sigma_X)

# Display the samples for beta
df_samples_beta = pd.DataFrame(samples_beta, columns=['beta_0', 'beta_1', 'beta_2'])
print(df_samples_beta.tail())

import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf

# Trace plots for beta
plt.figure(figsize=(12, 6))
for i in range(samples_beta.shape[1]):
    plt.subplot(samples_beta.shape[1], 1, i + 1)
    plt.plot(samples_beta[:, i])
    plt.title(f'Trace plot for beta_{i}')
plt.tight_layout()
plt.show()

# Histograms for beta
plt.figure(figsize=(12, 6))
for i in range(samples_beta.shape[1]):
    plt.subplot(samples_beta.shape[1], 1, i + 1)
    plt.hist(samples_beta[:, i], bins=30, density=True)
    plt.title(f'Posterior of beta_{i}')
plt.tight_layout()
plt.show()

# Autocorrelation for beta
plt.figure(figsize=(12, 6))
for i in range(samples_beta.shape[1]):
    plt.subplot(samples_beta.shape[1], 1, i + 1)
    plot_acf(samples_beta[:, i], ax=plt.gca())
    plt.title(f'Autocorrelation of beta_{i}')
plt.tight_layout()
plt.show()


