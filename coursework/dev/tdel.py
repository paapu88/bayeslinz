import numpy as np

def proposal_X(X, mask, y, beta, mu, Sigma):
    X_proposal = X.copy()
    n_rows, n_cols = X.shape
    
    # Cholesky decomposition of Sigma
    L = np.linalg.cholesky(Sigma)
    
    for i in range(n_rows):
        # Indices of NaN values in the current row
        nan_indices = np.where(mask[i])[0]
        
        if len(nan_indices) > 0:
            # Randomly select one NaN index to use y information
            selected_index = np.random.choice(nan_indices)
            
            # Impute all NaN values without using y information
            for j in nan_indices:
                if j != selected_index:
                    mean_j = mu[j] + Sigma[j, :] @ np.linalg.inv(Sigma) @ (X_proposal[i, :] - mu)
                    # Use Cholesky decomposition for a more stable standard deviation computation
                    L_j = L[j, :j+1]
                    std_j = np.sqrt(Sigma[j, j] - np.sum(L_j**2))
                    X_proposal[i, j] = np.random.normal(mean_j, std_j)
            
            # Sample from the sigmoid distribution given y
            if y[i] == 1:
                sampled_value = np.random.uniform(0, 1)
            else:
                sampled_value = np.random.uniform(-1, 0)

            # Calculate the mean and std deviation for the selected index
            mean_selected = mu[selected_index] + Sigma[selected_index, :] @ np.linalg.inv(Sigma) @ (X_proposal[i, :] - mu)
            # Use Cholesky decomposition for a more stable standard deviation computation
            L_selected = L[selected_index, :selected_index+1]
            std_selected = np.sqrt(Sigma[selected_index, selected_index] - np.sum(L_selected**2))
            
            # Solve deterministically for X[i, selected_index] using the sampled value
            if y[i] == 1:
                X_proposal[i, selected_index] = mean_selected + np.log(sampled_value / (1 - sampled_value)) / np.exp(X_proposal[i, :] @ beta)
            else:
                X_proposal[i, selected_index] = mean_selected - np.log(-sampled_value / (1 + sampled_value)) / (1 + np.exp(X_proposal[i, :] @ beta))
    
    return X_proposal

# Example usage
np.random.seed(42)
X = np.array([
    [1.0, 2.0, np.nan],
    [4.0, np.nan, 6.0],
    [np.nan, 8.0, 9.0]
])
mask = np.isnan(X)
y = np.array([1, 0, 1])
beta = np.array([0.1, 0.2, 0.3])
mu = np.array([0, 0, 0])
Sigma = np.eye(3)

X_proposal = proposal_X(X, mask, y, beta, mu, Sigma)
print(X_proposal)
