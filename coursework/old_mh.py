import pandas as pd
import numpy as np
from scipy.stats import norm
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import norm, truncnorm, bernoulli
from utils.myreports import report
from imblearn.over_sampling import SMOTE

# Step 1: Load the Data
def load_data(file_path):
    data = pd.read_csv(file_path, index_col=0)
    return data

# Step 2: Preprocess the Data
def preprocess_data(data):
    # Fill missing values with column means for initial processing
    #data = data.fillna(data.mean())
    
    # Separate features and target
    X = data.drop(columns=['FONUM','bankruptcy'])
    y = data['bankruptcy']
    # Add intercept term
    X.insert(0, 'intercept', 1)
    
    return X.to_numpy(), y.to_numpy()

# Step 3: Define the Logistic Regression Model and Metropolis-Hastings Algorithm
def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def log_likelihood(beta, X, y):
    linear_combination = X @ beta
    log_likelihood_value = np.sum(y * linear_combination - np.log(1 + np.exp(linear_combination)))
    return log_likelihood_value

def log_prior(beta, mean=0, variance=10):
    return np.sum(norm.logpdf(beta, mean, np.sqrt(variance)))

def log_posterior(beta, X, y):
    return log_likelihood(beta, X, y) + log_prior(beta)

def metropolis_hastings(log_posterior, initial_state, proposal_scale, n_iterations, burn_in, X, y):
    current_state = initial_state
    samples = []
    accepted = 0

    for i in range(n_iterations):
        proposed_state = current_state + np.random.normal(0, proposal_scale, size=current_state.shape)
        
        # Impute missing values
        #X_imputed = impute_missing_values(X, y, current_state)
        
        acceptance_ratio = np.exp(log_posterior(proposed_state, X, y) - log_posterior(current_state, X, y))

        if np.random.rand() < acceptance_ratio:
            current_state = proposed_state
            accepted += 1

        if i >= burn_in:
            samples.append(current_state)
        print(f"i {i} current_state {current_state}")
    acceptance_rate = accepted / n_iterations
    return np.array(samples), acceptance_rate

# Step 4: Train the Model
def train_model(X_train, y_train, n_iterations=100000, burn_in=50000, proposal_scale=2e-6):
    initial_state = np.zeros(X_train.shape[1])
    samples, acceptance_rate = metropolis_hastings(
        log_posterior, initial_state, proposal_scale, n_iterations, burn_in, X_train, y_train
    )
    return samples, acceptance_rate

# Step 6: Test the Model
def test_model(X_test, samples):
    #X_imputed = impute_missing_values_test(X_test, samples)
    beta_mean = samples.mean(axis=0)
    y_pred_prob = sigmoid(X_test @ beta_mean)
    y_pred = (y_pred_prob >= 0.5).astype(int)
    return y_pred, y_pred_prob

# Main Function
def main(file_path):
    data = load_data(file_path)
    X, y = preprocess_data(data)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    smote = SMOTE(random_state=42)
    X_train = np.where(np.isnan(X_train), -1234567, X_train)
    X_train, y_train = smote.fit_resample(X_train, y_train)
    # oversample because there is 1/10 bankruptcies
    X_train = np.where(X_train == -1234567, np.nan, X_train)
    samples, acceptance_rate = train_model(X_train, y_train)
    y_pred, y_pred_prob = test_model(X_test, samples)

    # Print results
    print(f"Acceptance Rate: {acceptance_rate}")
    accuracy = np.mean(y_pred == y_test)
    print(f"Accuracy: {accuracy}")

    report(true=y_test, pred=y_pred)


    # Plot results
    for ibeta in range(X_train.shape[1]):
        sns.kdeplot(samples[:, ibeta], label=f'beta_{ibeta}')
        #sns.kdeplot(samples[:, 1], label='beta_1')
    plt.title(f'Metropolis-Hastings Sampling\nAcceptance Rate: {acceptance_rate:.2f}')
    plt.xlabel('Parameter value')
    plt.ylabel('Density')
    plt.legend()
    plt.show()


if __name__ == "__main__":
    from pathlib import Path
    #main(file_path=Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.1_method_noInpute_withBB_altman.csv"))
    
    # proposal_scale=0.001
    #main(file_path=Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.1_method_median_withBB_altman.csv"))
    
    
    main(file_path=Path.home().joinpath("Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.9_method_median_withBB.csv"))