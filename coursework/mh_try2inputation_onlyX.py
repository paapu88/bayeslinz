import numpy as np
import pandas as pd
import scipy.stats as stats
from sklearn.impute import SimpleImputer
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from imblearn.over_sampling import SMOTE
from sklearn.utils import shuffle

from utils.myreports import report
from utils.mydata import load_data, data_to_numpy
from utils.betaplots import histograms_X, histograms_X_normal, histograms, traces, autocorrelations
from utils.mynumpy import nearestPD, standard_normal_with_nans, sample_from_sigmoid, solve_for_x_i
from utils.mynumpy import standard_normal_with_nans_not_col0, has_nans_or_infs, least_squares_estimate
from utils.mysamplers import compute_multivariate_normal_params, impute_nans_using_mvn
from utils.myimputers import knn_impute_with_masks

import gc

# Define logistic log-likelihood
def logistic_log_likelihood(beta, X, y):
    logits = X @ beta
    return np.sum(y * logits - np.log(1 + np.exp(logits)))

# Define the prior for beta (normal prior)
def log_prior_beta(beta, mu=0, sigma=10):
    return np.sum(stats.norm.logpdf(beta, mu, sigma))

# Define the prior for X (multivariate normal prior)
def log_prior_X(X, mu, Sigma):
    return np.sum(stats.multivariate_normal.logpdf(X, mean=mu, cov=Sigma, allow_singular=True))

# Proposal distributions for beta
def proposal_beta(beta, scale):
    return np.random.normal(beta, scale, size=beta.shape)

# Proposal distribution for X
def proposal_X(X, mask, y, beta, mu, Sigma, scale_last_nan):
    X_proposal = X.copy()
    n_rows, n_cols = X.shape
    inv_Sigma = np.linalg.inv(Sigma)

    for i in range(n_rows):
        nan_indices = np.where(mask[i])[0]
        if len(nan_indices) > 0:
            selected_index = np.random.choice(nan_indices)
            for j in nan_indices:
                if j != selected_index:
                    std_factor = Sigma[j, j] - Sigma[j, :] @ inv_Sigma @ Sigma[:, j]
                    std_j = np.sqrt(np.clip(std_factor, a_min=0, a_max=None))
                    X_proposal[i, j] += np.random.normal(0, std_j)

            if y[i] == 1:
                sampled_value_x = sample_from_sigmoid(constraint="x > 0")
            else:
                sampled_value_x = sample_from_sigmoid(constraint="x < 0")

            xtrue = solve_for_x_i(y=sampled_value_x, X=X_proposal[i, 1:], beta=beta, j=selected_index)
            dx = xtrue - X_proposal[i, selected_index]
            X_proposal[i, selected_index] += dx * scale_last_nan

    return X_proposal

def proposal_X_no_y_from_X(X, mask, Sigma):
    inv_Sigma = np.linalg.inv(Sigma)

    X_proposal = X.copy()
    n_rows, n_cols = X.shape

    for i in range(n_rows):
        nan_indices = np.where(mask[i])[0]
        if len(nan_indices) > 0:
            for j in nan_indices:
                std_factor = Sigma[j, j] - Sigma[j, :] @ inv_Sigma @ Sigma[:, j]
                std_j = np.sqrt(np.clip(std_factor, a_min=0, a_max=None))
                X_proposal[i, j] += np.random.normal(0, std_j)
    return X_proposal

# Proposal distribution for X without y
def proposal_X_no_y(X, mask, mu, Sigma):
    inv_Sigma = np.linalg.inv(Sigma)
    X_proposal = X.copy()
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            if mask[i, j]:
                mean_j = mu[j] + Sigma[j, :] @ inv_Sigma @ (X_proposal[i, :] - mu)
                std_factor = Sigma[j, j] - Sigma[j, :] @ inv_Sigma @ Sigma[:, j]
                std_j = np.sqrt(np.clip(std_factor, a_min=0, a_max=None))
                X_proposal[i, j] = np.random.normal(mean_j, std_j)
    return X_proposal

# Metropolis-Hastings sampler
def mh_sampler(X, y, burn, iter_tot, scale_beta, scale_x, scale_last_nan, burn_nans):
    scale_beta0, scale_x0, scale_last_nan0 = scale_beta, scale_x, scale_last_nan
    mask = np.isnan(X)
    imputer = SimpleImputer(strategy='constant', fill_value=0)
    X_imputed = imputer.fit_transform(X)

    mu_X = np.mean(X_imputed, axis=0)
    Sigma_X = np.cov(X_imputed, rowvar=False)
    Sigma_X = nearestPD(Sigma_X)

    initial_beta = least_squares_estimate(X, y)
    beta_samples = []
    X_samples = []

    current_beta = initial_beta
    current_X = X_imputed
    current_log_post = logistic_log_likelihood(current_beta, current_X, y) + log_prior_beta(current_beta) + log_prior_X(current_X, mu_X, Sigma_X * scale_x)
    accept_count = 0
    last_iter = 0
    beta_check = 100
    ar = None
    bad_set_count = 0
    for iter in range(iter_tot):
        last_iter += 1
        proposed_beta = proposal_beta(current_beta, scale=scale_beta)

        proposed_X = proposal_X_no_y(current_X, mask, mu_X, Sigma_X * scale_x)
        #proposed_X=proposal_X_no_y_from_X(X=current_X, mask=mask, Sigma=Sigma_X * scale_x)
        proposed_log_post = logistic_log_likelihood(proposed_beta, proposed_X, y) + log_prior_beta(proposed_beta) + log_prior_X(proposed_X, mu_X, Sigma_X * scale_x)
        acceptance_ratio = np.exp(proposed_log_post - current_log_post)
        #print(f"acceptance_ratio {acceptance_ratio} prop {proposed_log_post} cur {current_log_post}")
        if np.random.rand() <= acceptance_ratio:
            current_beta = proposed_beta
            current_X = proposed_X
            current_log_post = proposed_log_post
            accept_count += 1
            if has_nans_or_infs(current_X):
                raise RuntimeError("X has nans after proposal...")


        if iter > burn:
            beta_samples.append(current_beta)
            if iter % 100 == 0:
                if has_nans_or_infs(current_X):
                    raise RuntimeError("100: X has nans after proposal...")
                X_samples.append(current_X.copy())
                mu_X = np.mean(current_X, axis=0)
                Sigma_X = np.cov(current_X, rowvar=False)
                Sigma_X = nearestPD(Sigma_X)
                gc.collect()

        if last_iter >= beta_check:
            ar = accept_count / beta_check
            accept_count = 0
            last_iter = 0
            if scale_beta < 1e-3:
                scale_last_nan /= 10
                scale_x /= 10
            if ar > 0.9:
                scale_beta *= 1.01
                scale_last_nan *= 1.01
                scale_x *= 1.01
            if ar < 0.05:
                scale_beta /= 1.02
                scale_last_nan /= 1.02
                scale_x /= 1.02
            if ar< 1e-6:
                bad_set_count+=1
                if bad_set_count > 3:
                    bad_set_count=1
                    scale_beta, scale_x, scale_last_nan = scale_beta0, scale_x0, scale_last_nan0
            else:
                bad_set_count=0
            

        print(f"iter {iter} beta {current_beta} scale_beta {scale_beta} scale_last_nan {scale_last_nan} scale_x {scale_x} accept ratio {ar}")

    return np.array(beta_samples), np.array(X_samples)

def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def test_model(X_test, samples):
    beta_mean = samples.mean(axis=0)
    y_pred_prob = sigmoid(X_test @ beta_mean)
    y_pred = (y_pred_prob >= 0.5).astype(int)
    return y_pred, y_pred_prob

def main(file_path, burn, iter_tot, burn_nans, scale_beta, scale_x, scale_last_nan):
    data = load_data(file_path)
    X, y, col_names = data_to_numpy(data)
    print(f"before {X.shape}")
    #histograms_X(X=X, col_names=col_names)
    X = standard_normal_with_nans(X)

    ones_column = np.ones((X.shape[0], 1))
    X = np.hstack((ones_column, X))
    col_names = ["Intercept"] + col_names
    print(col_names)

    #histograms_X_normal(X=X, col_names=col_names)
    print(f"after {X.shape}")

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    smote = SMOTE()
    X_train = np.where(np.isnan(X_train), -1234567, X_train)
    X_train, y_train = smote.fit_resample(X_train, y_train)
    X_train = np.where(X_train == -1234567, np.nan, X_train)
    X_train, y_train = shuffle(X_train, y_train)
    X_train = standard_normal_with_nans_not_col0(X=X_train)
    print(f"col means X_train {np.nanmean(X_train, axis=0)}")

    mask_train = np.isnan(X_train)
    mask_test = np.isnan(X_test)

    samples_beta, samples_X = mh_sampler(X=X_train, y=y_train, burn=burn, burn_nans=burn_nans, iter_tot=iter_tot, scale_beta=scale_beta, scale_x=scale_x, scale_last_nan=scale_last_nan)
    print(f"NANS in X samples after training")
    print(f"samples_X, NANS: {has_nans_or_infs(samples_X)}")
    print(f"samples_beta: {samples_beta.shape}")

    traces(samples_beta=samples_beta, col_names=col_names)
    histograms(samples_beta=samples_beta, col_names=col_names)
    autocorrelations(samples_beta=samples_beta, col_names=col_names)

    print(f"after {samples_X.shape}")
    X_imputed_train = samples_X.reshape(-1, samples_X.shape[-1])
    histograms_X_normal(X=X_imputed_train, col_names=col_names)

    X_test = standard_normal_with_nans_not_col0(X=X_test)
    X_test = knn_impute_with_masks(X_train=np.mean(samples_X, axis=0), X_test=X_test, mask_train=mask_train, mask_test=mask_test)

    y_pred, y_pred_prob = test_model(X_test, samples_beta)
    accuracy = np.mean(y_pred == y_test)
    print(f"Accuracy: {accuracy}")

    report(true=y_test, pred=y_pred)

if __name__ == "__main__":
    from pathlib import Path
    main(burn=15000, iter_tot=20000, burn_nans=0, scale_beta=1e-2, 
         scale_x=1e0, scale_last_nan=1e-4, 
         file_path=Path("./data/2018_data_filtered_minfrac_0.9_method_noInpute_withBB.csv"))
    #main(burn=15000, iter_tot=20000, burn_nans=20000, scale_beta=1e-2, 
    #     scale_x=1e0, scale_last_nan=1e-4, 
    #     file_path=Path("./data/2018_data_filtered_minfrac_0.1_method_noInpute_withBB.csv"))
    
