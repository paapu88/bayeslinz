"""
python probit_on_bankruptcy.py --filename ./yaml/my_gibbsInputing_input.yaml
-> ./yaml/result_my_gibbsInputing_input.yaml

python probit_on_bankruptcy.py --filename ./yaml/statsmodel_median_input42.yaml
-> ./yaml/result_statsmodel_median_input42.yaml

python probit_on_bankruptcy.py --filename ./yaml/my_median_input42.yaml
-> ./yaml/result_my_median_input42.yaml

python probit_on_bankruptcy.py --filename ./yaml/my_gibbsInputing_input42.yaml
-> ./yaml/result_my_gibbsInputing_input42.yaml

"""

import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pandas as pd
from pathlib import Path
from utils.mynumpy import get_train_test
from utils.mypredictor import predict_probit
from utils.mysamplers import sample_beta, sample_z, impute_missing_values_train, impute_missing_values_test
from utils.myreports import report
from utils.utils import parse_arguments, read_yaml_file
from imblearn.over_sampling import SMOTE
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix, roc_auc_score, classification_report
import statsmodels.api as smf

args = parse_arguments()
data = read_yaml_file(args.filename)
reportfile = Path(args.filename)
print(f"reportfile {reportfile.name} {reportfile.parent}")
reportfile = reportfile.parent.joinpath("results_"+str(reportfile.name))
print(data)
df = pd.read_csv(
    Path.home().joinpath(data['infile']),
    index_col=0,
)
print(df.head())
print(df.describe())
y = df[data['ycol']]
X = df[data['xcols']]

if data["add_size_interaction"]:
    # Step 1: Create the dummy variable
    X['DUMMY_SIZE'] = (X['SIZE'] > data["size_threshold"]).astype(int)

    # Step 2: Create interaction terms
    #interaction_columns = [col for col in X.columns if col != 'SIZE' and col != 'DUMMY_SIZE']
    interaction_columns = ["SalesToTotalAssets"]
    for col in interaction_columns:
        X[f'{col}_INTERACT'] = X[col] * X['DUMMY_SIZE']
    print(f"added interaction with size")

print(f"data {data}")
if data["inpute"] =="median":
    print(f"median inputing")
    # Calculate the median of each column
    medians = X.median()
    # Replace NaN values with the median of the respective columns
    X = X.fillna(medians)
print(y)
print(X)
X = X.values
# ones for constant coefficiennt beta0
X = np.hstack([np.ones((X.shape[0], 1)), X])
y = y.values
mypreds=[]
mytrues=[]
for repeat in range(10):
    X_train, y_train, X_test, y_test = get_train_test(X=X, y=y)
    smote = SMOTE(random_state=42)
    X_train = np.where(np.isnan(X_train), -1234567, X_train)
    X_train, y_train = smote.fit_resample(X_train, y_train)
    # oversample because there is 1/10 bankruptcies
    X_train = np.where(X_train == -1234567, np.nan, X_train)
    if data["method"]=="statsmodel":
        probit_model=smf.Probit(y_train,X_train)
        result=probit_model.fit()
        preds = probit_model.predict(params=result.params, exog=X_test)
    elif data["method"] == "gibbs":
        # Initialize parameters
        n_iter = 200  # 1000
        burn = 100  # 500X = None
        beta_prior_mean = np.zeros(X_train.shape[1])
        beta_prior_cov = np.eye(X_train.shape[1])
        beta_samples = np.zeros((n_iter, X_train.shape[1]))
        z_samples = np.zeros((n_iter, X_train.shape[0]))

        # Initial values
        beta = np.zeros(X_train.shape[1])
        z = np.random.randn(X_train.shape[0])
        if data["inpute"] =="median":
            X_imputed=X_train
        else:
            X_imputed = impute_missing_values_train(X=X_train, beta=beta, y=y_train)


        # Gibbs sampling for training
        for i in range(n_iter):
            z = sample_z(X_imputed, y_train, beta)
            #beta_prior_mean, beta_prior_cov, 
            beta_prior_mean, beta_prior_cov, beta = sample_beta(
                X_imputed, z, beta_prior_mean, beta_prior_cov
            )
            if data["inpute"] !="median":
                X_imputed = impute_missing_values_train(X=X_train, beta=beta, y=y_train)
            beta_samples[i, :] = beta
            print(f"i {i} beta {beta}")
            z_samples[i, :] = z

        # Posterior mean of beta
        beta_mean = beta_samples[burn:, :].mean(axis=0)
        print("Posterior mean of beta:", beta_mean)

        # predictions
        last_goods = n_iter - burn
        if data["inpute"] =="median":
            X_imputed=X_test
        else:
            X_imputed = impute_missing_values_test(X=X_test, beta=beta_mean)
        # Gibbs sampling
        preds = predict_probit(beta_samples=beta_samples[-last_goods:, :],
                            X_test=X_imputed)
    else:
        raise NotImplementedError("only statsmodel or gibbs implemented")

    #for true, pred in zip(y_test.tolist(), preds):
    #    print(f"true {true} pred {pred}")
    mytrues = mytrues + y_test.tolist()
    preds = [1 if value >= 0.5 else 0 for value in preds]

    mypreds = mypreds + preds
    report(true=mytrues, pred=mypreds)
report(true=mytrues, pred=mypreds,outfile=reportfile)