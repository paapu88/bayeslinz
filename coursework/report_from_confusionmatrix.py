from sklearn.metrics import classification_report, confusion_matrix

# Given confusion matrix
confusion_mat = [[557, 114],
                 [52,  42]]

# Reconstruct true labels and predictions based on confusion matrix
y_true = [0] * (confusion_mat[0][0] + confusion_mat[0][1]) + [1] * (confusion_mat[1][0] + confusion_mat[1][1])
y_pred = [0] * confusion_mat[0][0] + [1] * confusion_mat[0][1] + [0] * confusion_mat[1][0] + [1] * confusion_mat[1][1]

# Generate classification report
report = classification_report(y_true, y_pred, target_names=["Class 0", "Class 1"])
print(f"12 variables, bayesian inputation for X (not y)")
confusion_mat = [[item * 2 for item in sublist] for sublist in confusion_mat]
print(f"confusion matrix:")
for row in confusion_mat:
    print(row)
print(report)

# Given confusion matrix
confusion_mat = [[41, 631],
                 [8,  85]]

# Reconstruct true labels and predictions based on confusion matrix
y_true = [0] * (confusion_mat[0][0] + confusion_mat[0][1]) + [1] * (confusion_mat[1][0] + confusion_mat[1][1])
y_pred = [0] * confusion_mat[0][0] + [1] * confusion_mat[0][1] + [0] * confusion_mat[1][0] + [1] * confusion_mat[1][1]

# Generate classification report
report = classification_report(y_true, y_pred, target_names=["Class 0", "Class 1"])
print(f"42 variables, bayesian inputation for X (not y)")
confusion_mat = [[item * 2 for item in sublist] for sublist in confusion_mat]
print(f"confusion matrix:")
for row in confusion_mat:
    print(row)
print(report)
