import statsmodels.api as smf
from imblearn.over_sampling import SMOTE
import pandas as pd
from pathlib import Path
from utils.mynumpy import get_train_test
import numpy as np

data = pd.read_csv(
    Path.home().joinpath(
        "Dropbox/Hanken/2024Linz/Bayes/coursework/data/2018_data_filtered_minfrac_0.1_method_median_withBB_altman.csv"
    ),
    index_col=0,
)
print(data.head())
print(data.describe())
cols = data.columns[2:].to_list()
X = data.iloc[:, 2:].values
# ones for constant coefficiennt beta0
X = np.hstack([np.ones((X.shape[0], 1)), X])
y = data.iloc[:, 1].values
print(X[:3, :])
print(f"y: {y[:3]}")
print(f"cols {cols}")
X_train, y_train, X_test, y_test = get_train_test(X=X, y=y)
smote = SMOTE(random_state=42)
X_train, y_train = smote.fit_resample(X_train, y_train)

probit_model=smf.Probit(y_train,X_train)
result=probit_model.fit()
print(result.summary2())
print(result.params)
preds = probit_model.predict(params=result.params, exog=X_test)
print(preds)
for true, pred in zip(y_test.tolist(), preds):
    print(f"true {true} pred {pred}")