import torch
import pyro
import pyro.distributions as dist
from pyro.infer import MCMC, NUTS

# Ensure PyTorch and Pyro are up to date
# pip install pyro-ppl torch --upgrade


# Define the model for a single study, ensuring inputs are tensors
def model_single_study(n_samples, positive_cases):
    # Ensure inputs are tensors
    n_samples = torch.tensor(n_samples, dtype=torch.float)
    positive_cases = torch.tensor(positive_cases, dtype=torch.float)

    # Prior distribution for the infection risk, using a uniform prior
    p = pyro.sample("p", dist.Uniform(0.0, 1.0))

    # Binomial likelihood for the observed data
    pyro.sample("obs", dist.Binomial(n_samples, p), obs=positive_cases)


# Given data for study A
n_samples_a = 1000
positive_a = 40

# Run MCMC for study A
nuts_kernel_a = NUTS(model_single_study)
mcmc_a = MCMC(nuts_kernel_a, num_samples=1000, warmup_steps=500)
mcmc_a.run(n_samples_a, positive_a)

# Extract samples for study A
samples_a = mcmc_a.get_samples()
p_a_samples = samples_a["p"].detach().cpu().numpy()

# Given data for study B
n_samples_b = 50
positive_b = 0

# Run MCMC for study B
nuts_kernel_b = NUTS(model_single_study)
mcmc_b = MCMC(nuts_kernel_b, num_samples=1000, warmup_steps=500)
mcmc_b.run(n_samples_b, positive_b)

# Extract samples for study B
samples_b = mcmc_b.get_samples()
p_b_samples = samples_b["p"].detach().cpu().numpy()

# Compute summary statistics
mean_a = p_a_samples.mean()
std_a = p_a_samples.std()
mean_b = p_b_samples.mean()
std_b = p_b_samples.std()

print(f"Study A - Mean: {mean_a:.4f}, Std: {std_a:.4f}")
print(f"Study B - Mean: {mean_b:.4f}, Std: {std_b:.4f}")
