import pyro
import pyro.distributions as dist
from pyro.infer import MCMC, NUTS, Predictive
import numpy as np
import torch
import pyreadr
import pandas as pd
import arviz as az
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression


# Define the model
def model(data, alpha_prior_1, beta_prior_1, alpha_prior_2, beta_prior_2):
    p_deaths_1 = pyro.sample("p_deaths_1", dist.Beta(alpha_prior_1, beta_prior_1))
    p_deaths_2 = pyro.sample("p_deaths_2", dist.Beta(alpha_prior_2, beta_prior_2))
    with pyro.plate("data_1", size=93):  # Adjusted to match the first part of the data
        pyro.sample("obs1", dist.Binomial(data[:93, 1], p_deaths_1), obs=data[:93, 0])
    with pyro.plate("data_2", size=data.shape[0] - 93):
        pyro.sample("obs2", dist.Binomial(data[93:, 1], p_deaths_2), obs=data[93:, 0])


# read data of 192 months
# Load the RData file
result = pyreadr.read_r("children.Rdata")

# `result` is a dictionary where keys are the names of the objects in the RData file
# and values are pandas DataFrames
dfs = []
for name, df in result.items():
    print(f"Dataset name: {name}")
    # print(df.head())  # Print the first few rows of each dataset

df = result["children"]
print(df.head())
df.to_csv("children.csv")

# run to get posteriori
# here positive cases are the dead children
# guess for beta prior from lecture notes
alpha_prior = 0.5
beta_prior = 5.0
nuts_kernel = NUTS(
    lambda data: model(data, alpha_prior, beta_prior, alpha_prior, beta_prior)
)
mcmc = MCMC(nuts_kernel, num_samples=1500, warmup_steps=500)
data = torch.tensor(df[["y", "exposed"]].to_numpy(), dtype=torch.float)
# data_orig = data.clone()
# print(data)
mcmc.run(data=data)

# extract samples from posteriori
print(f"samples {mcmc.get_samples()}")
samples = []
samples.append(mcmc.get_samples()["p_deaths_1"])
samples.append(mcmc.get_samples()["p_deaths_2"])

for tag, sample in zip(["old", "new"], samples):
    mean = sample.mean()
    std = sample.std()

    ci_lower = np.quantile(sample, 0.025)
    ci_upper = np.quantile(sample, 0.975)
    print(
        f"Study {tag} - Mean: {mean:.6e}, Std: {std:.6e}, 95% CI: [{ci_lower:.6e}, {ci_upper:.6e}]"
    )

# Study old - Mean: 2.460534e-04, Std: 1.806260e-05, 95% CI: [2.117414e-04, 2.849611e-04]
# Study new - Mean: 1.732789e-04, Std: 1.404913e-05, 95% CI: [1.472084e-04, 2.026489e-04]
for tag, sample in zip(["old", "new"], samples):
    p_deaths = sample.numpy()  # Ensure you convert to NumPy array for easier handling
    # Plotting the posterior distribution
    plt.hist(p_deaths, bins=30, density=True, alpha=0.6, color="g")
    plt.title(f"Posterior distribution of $p_{{{tag}}}$")
    plt.xlabel(f"$p_{{{tag}}}$")
    plt.ylabel("Density")
    plt.savefig(f"ex2-4-p-{tag}.png")
    plt.show()

# Study old - Mean: 2.460534e-04, Std: 1.806260e-05, 95% CI: [2.117414e-04, 2.849611e-04]
# Study new - Mean: 1.732789e-04, Std: 1.404913e-05, 95% CI: [1.472084e-04, 2.026489e-04]
