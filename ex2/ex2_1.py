import pyro
import pyro.distributions as dist
from pyro.infer import MCMC, NUTS
import numpy as np
import torch
import arviz as az
import matplotlib.pyplot as plt

# m = dist.Binomial(1000, torch.tensor([0, 0.01, 0.02, 0.03, 0.04, 0.05]))
# print(m.sample())


def model(n_samples: int, positive_cases: int) -> None:
    """update model, flat prior distribution, assume binomial distribution"""
    # Ensure inputs are tensors
    n_samples = torch.tensor(n_samples, dtype=torch.float)
    positive_cases = torch.tensor(positive_cases, dtype=torch.float)
    p = pyro.sample("p", dist.Uniform(0, 1))
    pyro.sample("obs", dist.Binomial(n_samples, p), obs=positive_cases)


# study A
n_samples_a = 1000
positive_a = 40
# run to get posteriori
nuts_kernel_a = NUTS(model)
mcmc_a = MCMC(nuts_kernel_a, num_samples=1500, warmup_steps=500)
# mcmc_a.run(n_samples=n_samples_a, positive_cases=positive_a)
mcmc_a.run(n_samples_a, positive_a)

# extract samples from posteriori
p_a_samples = mcmc_a.get_samples()["p"].numpy()
mean_a = p_a_samples.mean()
std_a = p_a_samples.std()

ci_lower_a = np.quantile(p_a_samples, 0.025)
ci_upper_a = np.quantile(p_a_samples, 0.975)
print(
    f"Study A - Mean: {mean_a:.4f}, Std: {std_a:.4f}, 95% CI: [{ci_lower_a:.4f}, {ci_upper_a:.4f}]"
)
# Convert Pyro MCMC samples to InferenceData
inference_data = az.from_pyro(mcmc_a)
# Plot the posterior for "p_positive"
az.plot_posterior(inference_data, var_names=["p"])
plt.show()
# study B
n_samples_b = 50
positive_b = 0
# run to get posteriori
nuts_kernel_b = NUTS(model)
mcmc_b = MCMC(nuts_kernel_b, num_samples=1500, warmup_steps=500)
# mcmc_a.run(n_samples=n_samples_a, positive_cases=positive_a)
mcmc_b.run(n_samples_b, positive_b)

# extract samples from posteriori
p_b_samples = mcmc_b.get_samples()["p"].numpy()
mean_b = p_b_samples.mean()
std_b = p_b_samples.std()

ci_lower_b = np.quantile(p_b_samples, 0.025)
ci_upper_b = np.quantile(p_b_samples, 0.975)
print(
    f"Study B - Mean: {mean_b:.4f}, Std: {std_b:.4f}, 95% CI: [{ci_lower_b:.4f}, {ci_upper_b:.4f}]"
)

# Study A - Mean: 0.0412, Std: 0.0061, 95% CI: [0.0301, 0.0533]
# Study B - Mean: 0.0194, Std: 0.0193, 95% CI: [0.0004, 0.0678]
