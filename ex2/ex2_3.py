import pyro
import pyro.distributions as dist
from pyro.infer import MCMC, NUTS, Predictive
import numpy as np
import torch
import pyreadr
import pandas as pd
import arviz as az
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression


# Define the model
def model(data, alpha_prior, beta_prior):
    # print(f"beta_prior in function{beta_prior}")

    # Use the passed alpha and beta as parameters for the Beta prior
    p_deaths = pyro.sample("p_deaths", dist.Beta(alpha_prior, beta_prior))
    # in binomial
    # np=mean, so p is about 1.9/9000 = 0.00021111
    # deaths = pyro.sample("deaths", dist.Binomial(9000, 0.00021111))
    # print(f"p_deaths: {p_deaths}")
    # print(data)
    # Assuming data is a tensor where data[:, 0] are successes and data[:, 1] are totals
    with pyro.plate("data", size=data.shape[0]):
        pyro.sample("obs", dist.Binomial(data[:, 1], p_deaths), obs=data[:, 0])


# read data of 192 months
# Load the RData file
result = pyreadr.read_r("children.Rdata")

# `result` is a dictionary where keys are the names of the objects in the RData file
# and values are pandas DataFrames
dfs = []
for name, df in result.items():
    print(f"Dataset name: {name}")
    # print(df.head())  # Print the first few rows of each dataset

df = result["children"]
print(df.head())

# run to get posteriori
# here positive cases are the dead children
# guess for beta prior from lecture notes
alpha_prior = 0.5
beta_prior = 5.0
nuts_kernel = NUTS(lambda data: model(data, alpha_prior, beta_prior))
mcmc = MCMC(nuts_kernel, num_samples=1500, warmup_steps=500)
data = torch.tensor(df[["y", "exposed"]].to_numpy(), dtype=torch.float)
# data_orig = data.clone()
# print(data)
mcmc.run(data=data)

# extract samples from posteriori
print(f"samples {mcmc.get_samples()}")
samples = mcmc.get_samples()["p_deaths"]
print(samples)
mean = samples.mean()
std = samples.std()

ci_lower = np.quantile(samples, 0.025)
ci_upper = np.quantile(samples, 0.975)
print(
    f"Study - Mean: {mean:.6e}, Std: {std:.6e}, 95% CI: [{ci_lower:.6e}, {ci_upper:.6e}]"
)
# Extract the parameter of interest, e.g., "p_positive"
p_deaths = samples.numpy()  # Ensure you convert to NumPy array for easier handling
# Plotting the posterior distribution
plt.hist(p_deaths, bins=30, density=True, alpha=0.6, color="g")
plt.title("Posterior distribution of $p_{p_deaths}$")
plt.xlabel("$p_{p_deaths}$ value")
plt.ylabel("Density")
plt.show()

# predict
time_series_of_total_cases_for_Jan = df[["exposed"]][::12].to_numpy().flatten()
print(time_series_of_total_cases_for_Jan)
XX = np.arange(len(time_series_of_total_cases_for_Jan)).reshape(
    -1, 1
)  # Time steps as features
yy = time_series_of_total_cases_for_Jan  # Values as targets
# Initialize and train the linear regression model
model_l = LinearRegression()
model_l.fit(XX, yy)
# Predict the next value
next_time_step = np.array(
    [[len(time_series_of_total_cases_for_Jan)]]
)  # Next time step (as a new observation)
predicted_next_Jan_exposed = model_l.predict(next_time_step)[0]

# Assuming `mcmc` is your MCMC object from Pyro after fitting the model
posterior_samples = mcmc.get_samples()

# Assuming you have `total_cases_next_month` as the total number of trials for the prediction month
total_cases_next_month = torch.tensor([predicted_next_Jan_exposed], dtype=int)


# Predictive model using the posterior samples
def predictive_model(total_cases):
    print(f"pred: {posterior_samples['p_deaths']}")
    print(f"total cases: {total_cases, total_cases.min(), total_cases.max()}")
    return pyro.sample(
        "predicted_cases", dist.Binomial(total_cases, posterior_samples["p_deaths"])
    )


# Create a Predictive object
predictive = Predictive(
    model=predictive_model,
    posterior_samples=posterior_samples,
    return_sites=["predicted_cases"],
)

# Generate predictive samples
predictive_samples = predictive(total_cases_next_month)

# Analyze predictive samples
predicted_cases_samples = predictive_samples["predicted_cases"]
mean_predicted_cases = predicted_cases_samples.mean()

print(f"predicted amount of children, next Jan {predicted_next_Jan_exposed}")
print(f"prediction, next Jan {mean_predicted_cases}")


# Convert Pyro MCMC samples to InferenceData
# inference_data = az.from_pyro(mcmc)
# Plot the posterior for "p_positive"
# az.plot_posterior(inference_data, var_names=["p_deaths"])
# plt.show()

result = {}
for alpha_prior in [0.1, 2, 5]:
    for beta_prior in [0.1, 2, 5]:
        nuts_kernel = NUTS(lambda data: model(data, alpha_prior, beta_prior))
        mcmc = MCMC(nuts_kernel, num_samples=1500, warmup_steps=500)
        data = torch.tensor(df[["y", "exposed"]].to_numpy(), dtype=torch.float)
        # print(data)
        mcmc.run(data)

        # extract samples from posteriori
        samples = mcmc.get_samples()["p_deaths"]
        # print(samples)
        mean = samples.mean()
        std = samples.std()
        ci_lower = np.quantile(samples, 0.025)
        ci_upper = np.quantile(samples, 0.975)
        print(
            f"BETA(alpha={alpha_prior}, beta={beta_prior}): Mean: {mean:.6e}, Std: {std:.6e}, 95% CI: [{ci_lower:.6e}, {ci_upper:.6e}]"
        )
        # Extract the parameter of interest, e.g., "p_positive"
        p_deaths = (
            samples.numpy()
        )  # Ensure you convert to NumPy array for easier handling
        # Plotting the posterior distribution
        plt.hist(p_deaths, bins=30, density=True, alpha=0.6, color="g")
        plt.title("Posterior distribution of $p_{p_deaths}$")
        plt.xlabel("$p_{p_deaths}$ value")
        plt.ylabel("Density")
        plt.show()

# predicted amount of children, next Jan 8990.062897499998
# prediction, next Jan 1.8667755126953125
# so we predict 2 deaths

# Study -                    Mean: 2.074514e-04, Std: 1.102748e-05, 95% CI: [1.862026e-04, 2.289302e-04]
# BETA(alpha=0.1, beta=0.1): Mean: 2.084659e-04, Std: 1.136139e-05, 95% CI: [1.871888e-04, 2.302064e-04]
# BETA(alpha=0.1, beta=2):   Mean: 2.074591e-04, Std: 1.102738e-05, 95% CI: [1.866492e-04, 2.282284e-04]
# BETA(alpha=0.1, beta=5):   Mean: 2.069850e-04, Std: 1.192964e-05, 95% CI: [1.840330e-04, 2.303854e-04]
# BETA(alpha=2, beta=0.1):   Mean: 2.088171e-04, Std: 1.121712e-05, 95% CI: [1.872131e-04, 2.319669e-04]
# BETA(alpha=2, beta=2):     Mean: 2.091210e-04, Std: 1.080745e-05, 95% CI: [1.889428e-04, 2.298499e-04]
# BETA(alpha=2, beta=5):     Mean: 2.086613e-04, Std: 1.112283e-05, 95% CI: [1.873656e-04, 2.319012e-04]
# BETA(alpha=5, beta=0.1):   Mean: 2.105158e-04, Std: 1.092717e-05, 95% CI: [1.889608e-04, 2.309957e-04]
# BETA(alpha=5, beta=2):     Mean: 2.104963e-04, Std: 1.100614e-05, 95% CI: [1.880624e-04, 2.325238e-04]
# BETA(alpha=5, beta=5):     Mean: 2.101252e-04, Std: 1.146809e-05, 95% CI: [1.871669e-04, 2.326038e-04]
