import torch
import pyro
import pyro.distributions as dist
from pyro.infer import MCMC, NUTS
import numpy as np

# guess for beta prior from lecture notes
alpha_prior = 0.5
beta_prior = 5.0
# Sample data (placeholder, replace with your actual data)
your_data = [[10, 9000], [20, 8000], [1000, 10000]]
data = torch.tensor(
    [[positive_cases, total_cases] for positive_cases, total_cases in your_data]
)


# Define the model
def model(data):
    # Probability of a positive case as a Beta distribution
    p_positive = pyro.sample("p_positive", dist.Beta(alpha_prior, beta_prior))

    # Binomial likelihood for observed data
    with pyro.plate("data", len(data)):
        # Observed data
        successes = data[:, 0]
        totals = data[:, 1]
        pyro.sample("obs", dist.Binomial(totals, p_positive), obs=successes)


# Run MCMC
nuts_kernel = NUTS(model)
mcmc = MCMC(nuts_kernel, num_samples=1000, warmup_steps=500)
mcmc.run(data)

# Extract samples
samples = mcmc.get_samples()

# Posterior mean and variance
p_mean = samples["p_positive"].mean().item()
p_variance = samples["p_positive"].var().item()

# Compute 95% credible interval
quantiles = np.quantile(samples["p_positive"], [0.025, 0.975])

print(f"Posterior mean: {p_mean}")
print(f"Posterior variance: {p_variance}")
print(f"95% credible interval for the probability of a positive case: {quantiles}")
