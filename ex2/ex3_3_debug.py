import pyro
import pyro.distributions as dist
from pyro.infer import MCMC, NUTS
import torch


def model(data, alpha_prior, beta_prior):
    # Use the passed alpha and beta as parameters for the Beta prior
    p_positive = pyro.sample("p_positive", dist.Beta(alpha_prior, beta_prior))
    # Assuming data is a tensor where data[:, 0] are successes and data[:, 1] are totals
    with pyro.plate("data", size=data.shape[0]):
        pyro.sample("obs", dist.Binomial(data[:, 1], p_positive), obs=data[:, 0])


# Ensure alpha_prior and beta_prior are valid positive numbers
alpha_prior = 2.0
beta_prior = 9000 - 2.0

# Define data appropriately
data = torch.tensor(
    [
        [number_of_positive_cases, total_cases]
        for number_of_positive_cases, total_cases in [[10, 10000], [5, 9000]]
    ]
)

# Modify MCMC or VI calls as necessary
nuts_kernel = NUTS(lambda data: model(data, alpha_prior, beta_prior))
mcmc = MCMC(nuts_kernel, num_samples=1000, warmup_steps=500)
mcmc.run(data)
